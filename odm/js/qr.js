var draw_qrcode = function(text, typeNumber, errorCorrectLevel) {
    document.write(create_qrcode(text, typeNumber, errorCorrectLevel) );
};

var create_qrcode = function(text, typeNumber, errorCorrectLevel, table) {

    var qr = qrcode(typeNumber || 4, errorCorrectLevel || 'M');
    qr.addData(text);
    qr.make();

//	return qr.createTableTag();
    return qr.createImgTag();
};

var update_qrcode = function() {
    var shop_id=document.upform.u_id.value;
    var brand_id=document.upform.i_brand_id.value;
    var text = '{"qr":{"Shop_id":"'+shop_id+'","Brand_id":"'+brand_id+'"}}'.replace(/^[\s\u3000]+|[\s\u3000]+$/g, '');
//    var text = '{"qr":{"Shop_id":"8","Brand_id":"2"}}'.replace(/^[\s\u3000]+|[\s\u3000]+$/g, '');
    var t = 4;
    var e = "M";
    document.getElementById('qr').innerHTML = create_qrcode(text, t, e);
};