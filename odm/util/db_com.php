<?php
session_start();
function quote_smart($value,$strFlg)
{
	// 数値以外をクオートする
	if (!is_numeric($value) || $strFlg) {
		$value = "'" . mysql_real_escape_string($value) . "'";
	}
	return $value;
}
function db_conn(){

	$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	if(!$db){
		die("connot connect:" . mysql_error());
	}

	$dns = mysql_select_db(DB_NAME,$db);

	if(!$dns){
		die("connot use db:" . mysql_error());
	}


	return $db;
}
function db_disConn($result,$db){

	//mysql_free_result($result);
	mysql_close($db);

}
function chkMem($u_id,$pwd){
	$link = db_conn();
	mysql_set_charset('utf8');

	$rowCnt = 0;

	$sql = sprintf("SELECT * from den_admin where lower(u_id)=%s and status=0",quote_smart($u_id,true));

	$result = mysql_query($sql,$link);
	if(!$result){
		$rowCnt = -1;
		db_disConn($result, $link);
		return $rowCnt;
	}

	$rowCnt=mysql_num_rows($result);

	if($rowCnt==0){
		db_disConn($result, $link);
		return $rowCnt;
	}else{
		$row=mysql_fetch_assoc($result);
		$dbPasswd=$row['pwd'];

		if(strcmp($dbPasswd, $pwd)==0){
			$rowCnt=$row;
		}else{
			$rowCnt=2;
		}
	}

	mysql_free_result($result);
	mysql_close($link);

	return $rowCnt;
}
function getRole($u_id){
    $link = db_conn();
    mysql_set_charset('utf8');

    $rowCnt = 0;

    $sql = sprintf("SELECT role from den_admin where lower(u_id)=%s and status=0",quote_smart($u_id,true));

    $result = mysql_query($sql,$link);
    if(!$result){
        $rowCnt = -1;
        db_disConn($result, $link);
        return $rowCnt;
    }

    $rowCnt=mysql_num_rows($result);

    if($rowCnt==0){
        db_disConn($result, $link);
        return $rowCnt;
    }else{
        $row=mysql_fetch_assoc($result);
        $rowCnt=$row['role'];
    }

    mysql_free_result($result);
    mysql_close($link);

    return $rowCnt;
}
//管理者情報存在チェック
function searchAdminInfo($u_id){
    $link = db_conn();
    mysql_set_charset('utf8');

    $rowCnt = 0;

    $sql = sprintf("SELECT * from den_admin where u_id=%s and status=0",quote_smart($u_id,true));

    $result = mysql_query($sql,$link);
    if(!$result){
        $rowCnt = -1;
        db_disConn($result, $link);
        return $rowCnt;
    }

    $rowCnt=mysql_num_rows($result);

    if($rowCnt==0){
        db_disConn($result, $link);
        return $rowCnt;
    }else{
        $rowCnt=1;
    }

    mysql_free_result($result);
    mysql_close($link);

    return $rowCnt;
}
//提携先情報存在チェック
function searchBrandInfo($brand_name){
    $link = db_conn();
    mysql_set_charset('utf8');

    $rowCnt = 0;

    $sql = sprintf("SELECT * from brand where brand_name=%s and del_flg=0",quote_smart(mysql_real_escape_string($brand_name),true));

    $result = mysql_query($sql,$link);
    if(!$result){
        $rowCnt = -1;
        db_disConn($result, $link);
        return $rowCnt;
    }

    $rowCnt=mysql_num_rows($result);

    if($rowCnt==0){
        db_disConn($result, $link);
        return $rowCnt;
    }else{
        $rowCnt=1;
    }

    mysql_free_result($result);
    mysql_close($link);

    return $rowCnt;
}
//提携先情報存在チェック（更新のとき）
function searchBrandInfoForUpdate($brand_name,$u_id){
    $link = db_conn();
    mysql_set_charset('utf8');

    $rowCnt = 0;

    $sql = sprintf("SELECT * from brand where brand_name=%s and brand_id<>%d and del_flg=0",quote_smart($brand_name,true),$u_id);

    $result = mysql_query($sql,$link);
    if(!$result){
        $rowCnt = -1;
        db_disConn($result, $link);
        return $rowCnt;
    }

    $rowCnt=mysql_num_rows($result);

    if($rowCnt==0){
        db_disConn($result, $link);
        return $rowCnt;
    }else{
        $rowCnt=1;
    }

    mysql_free_result($result);
    mysql_close($link);

    return $rowCnt;
}
//店舗情報存在チェック
function searchShopInfo($brand_id,$shop_name){
    $link = db_conn();
    mysql_set_charset('utf8');

    $rowCnt = 0;

    $sql = sprintf("SELECT * from shop where shop_name=%s and brand_id=%d and del_flg=0",quote_smart($shop_name,true),$brand_id);

    $result = mysql_query($sql,$link);
    if(!$result){
        $rowCnt = -1;
        db_disConn($result, $link);
        return $rowCnt;
    }

    $rowCnt=mysql_num_rows($result);

    if($rowCnt==0){
        db_disConn($result, $link);
        return $rowCnt;
    }else{
        $rowCnt=1;
    }

    mysql_free_result($result);
    mysql_close($link);

    return $rowCnt;
}
//店舗情報存在チェック（更新のとき）
function searchShopInfoForUpdate($brand_id,$shop_name,$u_id){
    $link = db_conn();
    mysql_set_charset('utf8');

    $rowCnt = 0;

    $sql = sprintf("SELECT * from shop where shop_name=%s and shop_id<>%d and brand_id=%d and del_flg=0",quote_smart($shop_name,true),$u_id,$brand_id);

    $result = mysql_query($sql,$link);
    if(!$result){
        $rowCnt = -1;
        db_disConn($result, $link);
        return $rowCnt;
    }

    $rowCnt=mysql_num_rows($result);

    if($rowCnt==0){
        db_disConn($result, $link);
        return $rowCnt;
    }else{
        $rowCnt=1;
    }

    mysql_free_result($result);
    mysql_close($link);

    return $rowCnt;
}
?>