<?php
	//include
	require '../util/include.php';
	//FORM情報取得（GET）
	$action = $_GET['action'];
	$u_id = $_GET['u_id'];

	//共通情報設定
	$sysdate=date('Y-m-d',time());
	$systime=date('Y-m-d H:i:s',time());
	$ip=get_real_ip();

	$role=$_SESSION['role'];
	$login_user=$_SESSION['login_user'];

	//プルダウンリスト取得
	$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	if(!$db){
		die("connot connect:" . mysql_error());
	}
	$dns = mysql_select_db(DB_NAME,$db);
	if(!$dns){
		die("connot use db:" . mysql_error());
	}
	mysql_set_charset('utf8');


	//提携先プルダウンリスト取得
	if($role=='2'){
		$sqlall = "select * from brand WHERE 1 and del_flg=0 and login_id='".$login_user."'  order by brand_id,sort_order";
	}
	elseif($role=='3'){
		$sqlall = "select bd.* from brand bd,shop sp WHERE 1 and bd.brand_id=sp.brand_id
                        and bd.del_flg=0 and sp.login_id='".$login_user."'  order by bd.brand_id,bd.sort_order";
	}
	else{
		$sqlall = "select * from brand WHERE 1";
	}
	$result_list = mysql_query($sqlall,$db);

	$sqlall = "select * from user_rank WHERE 1 and status=1";
	$result_list_prop = mysql_query($sqlall,$db);
	$sqlall = "select * from den_area WHERE 1 and parent_id=1 order by sort_order";
	$result_list_area1 = mysql_query($sqlall,$db);
	$sqlall = "select * from den_area WHERE 1 and parent_id=2 order by sort_order";
	$result_list_area2 = mysql_query($sqlall,$db);
	$sqlall = "select * from den_area WHERE 1 and parent_id=3 order by sort_order";
	$result_list_area3 = mysql_query($sqlall,$db);
	$sqlall = "select * from den_area WHERE 1 and parent_id=4 order by sort_order";
	$result_list_area4 = mysql_query($sqlall,$db);
	$sqlall = "select * from den_area WHERE 1 and parent_id=5 order by sort_order";
	$result_list_area5 = mysql_query($sqlall,$db);
	$sqlall = "select * from den_area WHERE 1 and parent_id=6 order by sort_order";
	$result_list_area6 = mysql_query($sqlall,$db);
	$sqlall = "select * from den_area WHERE 1 and parent_id=7 order by sort_order";
	$result_list_area7 = mysql_query($sqlall,$db);
	mysql_close($db);

	if ($action=='confirm'){
		$sub_title='新着情報管理　- 新規追加確認 -';
		$is_disabled="disabled='disabled'";
		$havePlaceholder='NO';

		//form項目
		$u_id = $_POST['u_id'];
		$update_flg=$_POST['update_flg'];
		//オンライン日付
		$i_date = $_POST['i_date'];
		//配信元ID
		$i_supply_id=$_POST['i_supply_id'];
		//ステータス
		$i_status = $_POST['i_status'];
		//タイトル
		$i_title = addslashes($_POST['i_title']);
		//詳細
		$i_content = $_POST['i_content'];
		$i_content=htmlspecialchars($i_content);
		//配信対象地域
		$i_area_id=$_POST['h_area_id'];
		$arr_area_id=explode(",",$i_area_id);
		//送信対象年齢FROM
		$i_target_age_from=$_POST['i_age_from'];
		//送信対象年齢FROM
		$i_target_age_to=$_POST['i_age_to'];
		//送信対象性別
		$i_sex=$_POST['h_sex'];
		$arr_sex=explode(",",$i_sex);
		//送信対象紹介元属性ID
		$i_prop_id=$_POST['h_prop_id'];
		$arr_prop_id=explode(",",$i_prop_id);
		//送信フラグ
		$i_push_flg=$_POST['i_push_flg'];

		$h_img_path1=$_POST['h_img_path1'];
		$h_img_path2=$_POST['h_img_path2'];
		$h_img_path3=$_POST['h_img_path3'];

	}
	elseif($action=='edit'){
		$sub_title='新着情報管理　- 新規追加訂正 -';
		$is_disabled="";
		$havePlaceholder='YES';
		//form項目
		$u_id = $_POST['u_id'];
		$update_flg=$_POST['update_flg'];
		//オンライン日付
		$i_date = $_POST['i_date'];
		//配信元ID
		$i_supply_id=$_POST['i_supply_id'];
		//ステータス
		$i_status = $_POST['i_status'];
		//タイトル
		$i_title = addslashes($_POST['i_title']);
		//詳細
		$i_content = $_POST['i_content'];
		$i_content=htmlspecialchars($i_content);
		//配信対象地域
		$i_area_id=$_POST['h_area_id'];
		$arr_area_id=explode(",",$i_area_id);
		//送信対象年齢FROM
		$i_target_age_from=$_POST['i_age_from'];
		//送信対象年齢FROM
		$i_target_age_to=$_POST['i_age_to'];
		//送信対象性別
		$i_sex=$_POST['h_sex'];
		$arr_sex=explode(",",$i_sex);
		//送信対象紹介元属性ID
		$i_prop_id=$_POST['h_prop_id'];
		$arr_prop_id=explode(",",$i_prop_id);
		//送信フラグ
		$i_push_flg=$_POST['i_push_flg'];
		$h_img_path1=$_POST['h_img_path1'];
		$h_img_path2=$_POST['h_img_path2'];
		$h_img_path3=$_POST['h_img_path3'];
	}
	elseif ($action=='update'||$action=='delete'){
		if($action=='update'){
			$sub_title='新着情報管理　- 編集 -';
			$is_disabled="";
			$havePlaceholder='NO';
		}
		if($action=='delete'){
			$sub_title='新着情報管理　- 削除 -';
			$is_disabled="disabled='disabled'";
			$havePlaceholder='NO';
		}

		$u_id = $_GET['u_id'];

		$update_flg='1';

		$logstr = "$systime $ip INFO：▼新着情報取得開始：id = $u_id \r\n";
		error_log($logstr,3,'../log/gen.log');

		if($u_id==''){

			db_disConn($result, $link);
			$logstr = "$systime ERR：id取得エラー！ \r\n";
			$logstr .= "$systime $ip INFO：▲新着情報取得異常終了 \r\n";
			error_log($logstr,3,'../log/gen.log');

			$err_cd_list[]="02";
			$_SESSION['err_cd_list']=$err_cd_list;
			$url= URL_PATH . "err.php";
			redirect($url);
		}

		//情報取得
		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}
		$dns = mysql_select_db(DB_NAME,$db);
		if(!$dns){
			die("connot use db:" . mysql_error());
		}
		mysql_set_charset('utf8');
		$sqlall = "select * from den_new_info WHERE 1";
		$sqlall .= " and id = $u_id";

		$result = mysql_query($sqlall,$db);
		$rs=mysql_fetch_object($result);

		//DB情報取得
		//オンライン日付
		$i_date = date('Y-m-d',$rs->online_date);
		//配信元ID
		$i_supply_id=$rs->supply_id;
		//ステータス
		$i_status = $rs->status;
		//タイトル
		$i_title = addslashes($rs->title);
		//詳細
		$i_content = $rs->contents;
		$i_content=htmlspecialchars($i_content);
		//画像１
		$h_img_path1=$rs->img_path1;
		$h_img_path1=IMG_URL_PATH.$h_img_path1;
		//画像２
		$h_img_path2=$rs->img_path2;
		$h_img_path2=IMG_URL_PATH.$h_img_path2;
		//画像３
		$h_img_path3=$rs->img_path3;
		$h_img_path3=IMG_URL_PATH.$h_img_path3;
		//配信対象地域
		$i_area_id=$rs->target_area_id;
		$arr_area_id=explode(",",$i_area_id);
		//送信対象年齢From
		$i_target_age_from=$rs->target_age_from;
		if($i_target_age_from=='0'){$i_target_age_from='';}
		//送信対象年齢To
		$i_target_age_to=$rs->target_age_to;
		if($i_target_age_to=='0'){$i_target_age_to='';}
		//送信対象性別
		$i_sex=$rs->target_sex;
		$arr_sex=explode(",",$i_sex);
		//送信対象紹介元属性ID
		$i_prop_id=$rs->target_rank_id;
		$arr_prop_id=explode(",",$i_prop_id);
		//送信フラグ
		$i_push_flg=$rs->push_flg;


		mysql_close($db);

		$logstr = "$systime $ip INFO：▲新着情報取得正常終了：id = $u_id \r\n";
		error_log($logstr,3,'../log/gen.log');

	}
	else{
		$sub_title='新着情報管理　- 新規追加 -';
		$is_disabled="";
		$havePlaceholder='YES';
		//form項目

		//オンライン日付
		$i_date = $sysdate;
		//配信元ID
		$i_supply_id='';
		//ステータス
		$i_status='';
		//タイトル
		$i_title='';
		//詳細
		$i_content='';
		//画像１
		$h_img_path1='';
		//画像２
		$h_img_path2='';
		//画像３
		$h_img_path3='';
		//配信対象地域
		$i_target_area_id='';
		//送信対象年齢From
		$i_target_age_from='';
		//送信対象年齢To
		$i_target_age_to='';
		//送信対象性別
		$i_target_sex='';
		//送信対象紹介元属性ID
		$i_target_rank_id='';
		//送信フラグ
		$i_push_flg='';
	}

	if ($action=='confirm'){

		$db_file_name_moto=time();
		if($_FILES["upfile1"]["tmp_name"]!=''){
			$file_tmp_name=$_FILES["upfile1"]["tmp_name"];
			$file = $_FILES["upfile1"];
			$pinfo=pathinfo($file["name"]);
			$ftype=$pinfo['extension'];
			$db_file_name=$db_file_name_moto.'_1';
			$destination1 = NEWS_UPLOAD_FOLDER.$db_file_name.".".$ftype;
			$show_dest1=NEWS_UPLOAD_FOLDER_ROOT.$db_file_name.".".$ftype;
			$ret=imgUpload($file_tmp_name,$file,$db_file_name,NEWS_UPLOAD_FOLDER_ROOT);

			if($ret!='0'){
				$err_cd_list[]="01";
				$_SESSION['err_cd_list']=$err_cd_list;
				$url= URL_PATH . "err.php";
				redirect($url);
			}
		}
		if($_FILES["upfile2"]["tmp_name"]!=''){
			$file_tmp_name=$_FILES["upfile2"]["tmp_name"];
			$file = $_FILES["upfile2"];
			$pinfo=pathinfo($file["name"]);
			$ftype=$pinfo['extension'];
			$db_file_name=$db_file_name_moto.'_2';
			$destination2 = NEWS_UPLOAD_FOLDER.$db_file_name.".".$ftype;
			$show_dest2=NEWS_UPLOAD_FOLDER_ROOT.$db_file_name.".".$ftype;
			$ret=imgUpload($file_tmp_name,$file,$db_file_name,NEWS_UPLOAD_FOLDER_ROOT);

			if($ret!='0'){
				$err_cd_list[]="01";
				$_SESSION['err_cd_list']=$err_cd_list;
				$url= URL_PATH . "err.php";
				redirect($url);
			}
		}
		if($_FILES["upfile3"]["tmp_name"]!=''){
			$file_tmp_name=$_FILES["upfile3"]["tmp_name"];
			$file = $_FILES["upfile3"];
			$pinfo=pathinfo($file["name"]);
			$ftype=$pinfo['extension'];
			$db_file_name=$db_file_name_moto.'_3';
			$destination3 = NEWS_UPLOAD_FOLDER.$db_file_name.".".$ftype;
			$show_dest3=NEWS_UPLOAD_FOLDER_ROOT.$db_file_name.".".$ftype;
			$ret=imgUpload($file_tmp_name,$file,$db_file_name,NEWS_UPLOAD_FOLDER_ROOT);

			if($ret!='0'){
				$err_cd_list[]="01";
				$_SESSION['err_cd_list']=$err_cd_list;
				$url= URL_PATH . "err.php";
				redirect($url);
			}
		}
	}

	//insert
	if ($action=='insert'){
		$logstr = "$systime $ip INFO：▼ニュース情報登録開始 \r\n";
		error_log($logstr,3,'../log/gen.log');

		//オンライン日付
		$i_date = $_POST['i_date'];
		//配信元ID
		$i_supply_id=$_POST['i_supply_id'];
		//ステータス
		$i_status = $_POST['i_status'];
		//タイトル
		$i_title = addslashes($_POST['i_title']);
		//詳細
		$i_content = $_POST['i_content'];
		$i_content=htmlspecialchars($i_content);
		//配信対象地域
		$i_area_id=$_POST['h_area_id'];
		$arr_area_id=explode(",",$i_area_id);
		//送信対象年齢FROM
		$i_target_age_from=$_POST['i_age_from'];
		//送信対象年齢FROM
		$i_target_age_to=$_POST['i_age_to'];
		//送信対象性別
		$i_sex=$_POST['h_sex'];
		$arr_sex=explode(",",$i_sex);
		//送信対象紹介元属性ID
		$i_prop_id=$_POST['h_prop_id'];
		$arr_prop_id=explode(",",$i_prop_id);
		//送信フラグ
		$i_push_flg=$_POST['i_push_flg'];
		$img_path1=$_POST['h_destination1'];
		$img_path2=$_POST['h_destination2'];
		$img_path3=$_POST['h_destination3'];
		if($img_path1==''){
			$img_path1=NEWS_UPLOAD_FOLDER.'news_default_img.png';
		}
		if($img_path2==''){
			$img_path2=NEWS_UPLOAD_FOLDER.'news_default_img.png';
		}
		if($img_path3==''){
			$img_path3=NEWS_UPLOAD_FOLDER.'news_default_img.png';
		}

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("insert into den_new_info (
						title,
						status,
						supply_id,
						contents,
						img_path1,
						img_path2,
						img_path3,
						target_area_id,
						target_age_from,
						target_age_to,
						target_sex,
						target_rank_id,
						push_flg,
						online_date,
						insert_time) values (%s,%d,%d,%s,%s,%s,%s,'%s',%d,%d,'%s','%s',%d,%d,%d)",
						quote_smart($i_title,true),
						$i_status,
						$i_supply_id,
						quote_smart($i_content,true),
						quote_smart($img_path1,true),
						quote_smart($img_path2,true),
						quote_smart($img_path3,true),
						$i_area_id,
						$i_target_age_from,
						$i_target_age_to,
						$i_sex,
						$i_prop_id,
						$i_push_flg,
						strtotime($i_date),
						strtotime($systime));
		$logstr = "$systime $ip INFO：ニュース情報登録 INSERT SQL文： ".$sql."\r\n";
		error_log($logstr,3,'../log/gen.log');

		$result = mysql_query($sql,$db);
		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
			$logstr = "$systime ERR：ニュース情報DB登録異常！ \r\n";
        	$logstr .= "$systime $ip INFO：▲ニュース情報登録異常終了 \r\n";
			error_log($logstr,3,'../log/gen.log');

			$err_cd_list[]="01";
			$_SESSION['err_cd_list']=$err_cd_list;
			$url= URL_PATH . "err.php";
			redirect($url);
		}

		mysql_close($db);
		$logstr = "$systime $ip INFO：▲ニュース情報登録正常終了！！ \r\n";
		error_log($logstr,3,'../log/gen.log');

		if ($i_push_flg=='1'&&$i_status=='1') {
			$logstr = "$systime $ip INFO：▼プッシュ通知開始 \r\n";
			error_log($logstr,3,'../log/gen.log');

			if($i_target_age_from!='') {
				$age_from=strtotime(date("Y-m-d",strtotime("-".$i_target_age_from." year")));
			}
			if($i_target_age_to!='') {
				$age_to=strtotime(date("Y-m-d",strtotime("-".$i_target_age_to." year")));
			}

			//プルダウンリスト取得
			$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
			if(!$db){
				die("connot connect:" . mysql_error());
			}
			$dns = mysql_select_db(DB_NAME,$db);
			if(!$dns){
				die("connot use db:" . mysql_error());
			}
			mysql_set_charset('utf8');

			$sql = "SELECT DISTINCT device_id, device_type from users where 1 and push_flg=0 and device_id <> '0' AND device_id <> '' AND device_id IS NOT NULL";

			if($i_area_id!='') {
				$i_area_id=str_replace(",","','",$i_area_id);
				$i_area_id="'".$i_area_id."'";
				$sql .= " and address in ($i_area_id)";
			}
			if($age_from!='') {
				$sql .= " and UNIX_TIMESTAMP(birthday) <= $age_from";
			}
			if($age_to!='') {
				$sql .= " and UNIX_TIMESTAMP(birthday) >= $age_to";
			}
			if($i_sex!='') {
				$sql .= " and sex in ($i_sex)";
			}
			if($i_prop_id!='') {
				$i_prop_id=str_replace(",","','",$i_prop_id);
				$i_prop_id="'".$i_prop_id."'";
				$sql .= " and rank_id in ($i_prop_id)";
			}

			$logstr = "$systime $ip INFO：プッシュ通知情報取得 SQL文： ".$sql."\r\n";
			error_log($logstr,3,'../log/gen.log');

			$result = mysql_query($sql,$db);

			if(!$result){
				db_disConn($result, $link);
				$logstr = "$systime ERR：プッシュ通知情報取得異常！ \r\n";
				$logstr .= "$systime $ip INFO：▲プッシュ通知異常終了 \r\n";
				error_log($logstr,3,'../log/gen.log');

				$err_cd_list[]="01";
				$_SESSION['err_cd_list']=$err_cd_list;
				$url= URL_PATH . "err.php";
				redirect($url);
			}
			while($rs=mysql_fetch_object($result)){
				if($rs->device_type=='1'){
					if($rs->device_id!=''){
						$deviceToken=$rs->device_id;
						$message=$i_title;
						$logstr = "$systime $ip INFO：▼プッシュ通知送信開始：device_id＝$deviceToken \r\n";
						error_log($logstr,3,'../log/gen.log');
						$ret = sendPush($message,$deviceToken);
						if($ret!='0'){
							$logstr = "$systime ERR：プッシュ通知送信異常！ \r\n";
							$logstr .= "$systime $ip INFO：▲プッシュ通知異常終了 \r\n";
							error_log($logstr,3,'../log/gen.log');

							$err_cd_list[]="01";
							$_SESSION['err_cd_list']=$err_cd_list;
							$url= URL_PATH . "err.php";
							redirect($url);
						}
						$logstr = "$systime $ip INFO：▲プッシュ通知送信正常終了！！ \r\n";
						error_log($logstr,3,'../log/gen.log');
					}
				}
				elseif($rs->device_type=='2'){
					if($rs->device_id!=''){
						$deviceToken=$rs->device_id;
						$message=$i_title;
						$logstr = "$systime $ip INFO：▼プッシュ通知送信開始：device_id＝$deviceToken \r\n";
						error_log($logstr,3,'../log/gen.log');
						$ret = send_notification($message,$deviceToken);
						if($ret!='0'){
							$logstr = "$systime ERR：プッシュ通知送信異常！ \r\n";
							$logstr .= "$systime $ip INFO：▲プッシュ通知異常終了 \r\n";
							error_log($logstr,3,'../log/gen.log');

							$err_cd_list[]="01";
							$_SESSION['err_cd_list']=$err_cd_list;
							$url= URL_PATH . "err.php";
							redirect($url);
						}
						$logstr = "$systime $ip INFO：▲プッシュ通知送信正常終了！！ \r\n";
						error_log($logstr,3,'../log/gen.log');
					}
				}
			}

			mysql_close($db);
			$logstr = "$systime $ip INFO：▲プッシュ通知正常終了！！ \r\n";
			error_log($logstr,3,'../log/gen.log');
		}



		$url= URL_PATH . "m_news.php?action=search";
		redirect($url);
	}

	//更新実行
	if ($action=='updatesubmit'){

		$u_id=$_POST['u_id'];
		//オンライン日付
		$i_date = $_POST['i_date'];
		//配信元ID
		$i_supply_id=$_POST['i_supply_id'];
		//ステータス
		$i_status = $_POST['i_status'];
		//タイトル
		$i_title = addslashes($_POST['i_title']);
		//詳細
		$i_content = $_POST['i_content'];
		$i_content=htmlspecialchars($i_content);
		//配信対象地域
		$i_area_id=$_POST['h_area_id'];
		$arr_area_id=explode(",",$i_area_id);
		//送信対象年齢FROM
		$i_target_age_from=$_POST['i_age_from'];
		//送信対象年齢FROM
		$i_target_age_to=$_POST['i_age_to'];
		//送信対象性別
		$i_sex=$_POST['h_sex'];
		$arr_sex=explode(",",$i_sex);
		//送信対象紹介元属性ID
		$i_prop_id=$_POST['h_prop_id'];
		$arr_prop_id=explode(",",$i_prop_id);
		//送信フラグ
		$i_push_flg=$_POST['i_push_flg'];
		$img_path1=$_POST['h_destination1'];
		$img_path2=$_POST['h_destination2'];
		$img_path3=$_POST['h_destination3'];
		$img_path1=$_POST['h_destination1'];
		if ($img_path1=='') {
			$img_path1=$_POST['h_img_path1'];
			$img_path1=str_replace(IMG_URL_PATH,'',$img_path1);
		}
		$img_path2=$_POST['h_destination2'];
		if ($img_path2=='') {
			$img_path2=$_POST['h_img_path2'];
			$img_path2=str_replace(IMG_URL_PATH,'',$img_path2);
		}
		$img_path3=$_POST['h_destination3'];
		if ($img_path3=='') {
			$img_path3=$_POST['h_img_path3'];
			$img_path3=str_replace(IMG_URL_PATH,'',$img_path3);
		}
		if($img_path1==''){
			$img_path1=NEWS_UPLOAD_FOLDER.'news_default_img.png';
		}
		if($img_path2==''){
			$img_path2=NEWS_UPLOAD_FOLDER.'news_default_img.png';
		}
		if($img_path3==''){
			$img_path3=NEWS_UPLOAD_FOLDER.'news_default_img.png';
		}

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$logstr = "$systime $ip INFO：▼新着情報更新開始 \r\n";
		error_log($logstr,3,'../log/gen.log');
		$sql = sprintf("update den_new_info set
                            title='%s',
                            status=%d,
                            supply_id=%d,
                            contents='%s',
                            img_path1='%s',
                            img_path2='%s',
                            img_path3='%s',
                            target_area_id='%s',
                            target_age_from=%d,
                            target_age_to=%d,
                            target_sex='%s',
                            target_rank_id='%s',
                            push_flg=%d,
                            online_date=%d,
                            update_time=%d
                            where id=%d",
			$i_title,
			$i_status,
			$i_supply_id,
			$i_content,
			$img_path1,
			$img_path2,
			$img_path3,
			$i_area_id,
			$i_target_age_from,
			$i_target_age_to,
			$i_sex,
			$i_prop_id,
			$i_push_flg,
			strtotime($i_date),
			strtotime($systime),
			$u_id
		);
		$logstr = "$systime $ip INFO：新着情報更新 UPDATE SQL文： ".$sql."\r\n";
		error_log($logstr,3,'../log/gen.log');

		$result = mysql_query($sql,$db);
		if(!$result){
			db_disConn($result, $link);
			$logstr = "$systime ERR：新着情報DB更新異常！ \r\n";
			$logstr .= "$systime $ip INFO：▲新着情報更新異常終了 \r\n";
			error_log($logstr,3,'../log/gen.log');

			$err_cd_list[]="01";
			$_SESSION['err_cd_list']=$err_cd_list;
			$url= URL_PATH . "err.php";
			redirect($url);
		}

		mysql_close($db);
		$logstr = "$systime $ip INFO：▲新着情報更新正常終了！！ \r\n";
		error_log($logstr,3,'../log/gen.log');

		if ($i_push_flg=='1'&&$i_status=='1') {
			$logstr = "$systime $ip INFO：▼プッシュ通知開始 \r\n";
			error_log($logstr,3,'../log/gen.log');

			if($i_target_age_from!='') {
				$age_from=strtotime(date("Y-m-d",strtotime("-".$i_target_age_from." year")));
			}
			if($i_target_age_to!='') {
				$age_to=strtotime(date("Y-m-d",strtotime("-".$i_target_age_to." year")));
			}

			//プルダウンリスト取得
			$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
			if(!$db){
				die("connot connect:" . mysql_error());
			}
			$dns = mysql_select_db(DB_NAME,$db);
			if(!$dns){
				die("connot use db:" . mysql_error());
			}
			mysql_set_charset('utf8');

			$sql = "SELECT DISTINCT device_id, device_type from users where 1 and push_flg=0 and device_id <> '0' AND device_id <> '' AND device_id IS NOT NULL";

			if($i_area_id!='') {
				$i_area_id=str_replace(",","','",$i_area_id);
				$i_area_id="'".$i_area_id."'";
				$sql .= " and address in ($i_area_id)";
			}
			if($age_from!='') {
				$sql .= " and UNIX_TIMESTAMP(birthday) <= $age_from";
			}
			if($age_to!='') {
				$sql .= " and UNIX_TIMESTAMP(birthday) >= $age_to";
			}
			if($i_sex!='') {
				$sql .= " and sex in ($i_sex)";
			}
			if($i_prop_id!='') {
				$i_prop_id=str_replace(",","','",$i_prop_id);
				$i_prop_id="'".$i_prop_id."'";
				$sql .= " and rank_id in ($i_prop_id)";
			}

			$logstr = "$systime $ip INFO：プッシュ通知情報取得 SQL文： ".$sql."\r\n";
			error_log($logstr,3,'../log/gen.log');

			$result = mysql_query($sql,$db);

			if(!$result){
				db_disConn($result, $link);
				$logstr = "$systime ERR：プッシュ通知情報取得異常！ \r\n";
				$logstr .= "$systime $ip INFO：▲プッシュ通知異常終了 \r\n";
				error_log($logstr,3,'../log/gen.log');

				$err_cd_list[]="01";
				$_SESSION['err_cd_list']=$err_cd_list;
				$url= URL_PATH . "err.php";
				redirect($url);
			}
			while($rs=mysql_fetch_object($result)){
				if($rs->device_type=='1'){
					if($rs->device_id!=''){
						$deviceToken=$rs->device_id;
						$message=$i_title;
						$logstr = "$systime $ip INFO：▼プッシュ通知送信開始：device_id＝$deviceToken \r\n";
						error_log($logstr,3,'../log/gen.log');
						$ret = sendPush($message,$deviceToken);
						if($ret!='0'){
							$logstr = "$systime ERR：プッシュ通知送信異常！ \r\n";
							$logstr .= "$systime $ip INFO：▲プッシュ通知異常終了 \r\n";
							error_log($logstr,3,'../log/gen.log');

							$err_cd_list[]="01";
							$_SESSION['err_cd_list']=$err_cd_list;
							$url= URL_PATH . "err.php";
							redirect($url);
						}
						$logstr = "$systime $ip INFO：▲プッシュ通知送信正常終了！！ \r\n";
						error_log($logstr,3,'../log/gen.log');
					}
				}
				elseif($rs->device_type=='2'){
					if($rs->device_id!=''){
						$registatoin_ids=array($rs->device_id);
						$message=array("msg" =>$i_title);
						$logstr = "$systime $ip INFO：▼プッシュ通知送信開始：device_id＝$registatoin_ids \r\n";
						error_log($logstr,3,'../log/gen.log');
						$ret = send_notification($message,$registatoin_ids);
						if($ret!='0'){
							$logstr = "$systime ERR：プッシュ通知送信異常！ \r\n";
							$logstr .= "$systime $ip INFO：▲プッシュ通知異常終了 \r\n";
							error_log($logstr,3,'../log/gen.log');

							$err_cd_list[]="01";
							$_SESSION['err_cd_list']=$err_cd_list;
							$url= URL_PATH . "err.php";
							redirect($url);
						}
						$logstr = "$systime $ip INFO：▲プッシュ通知送信正常終了！！ \r\n";
						error_log($logstr,3,'../log/gen.log');
					}
				}
			}

			mysql_close($db);
			$logstr = "$systime $ip INFO：▲プッシュ通知正常終了！！ \r\n";
			error_log($logstr,3,'../log/gen.log');
		}

		$url= URL_PATH . "m_news.php?action=search&role=".$role."&l_id=".$login_user;
		redirect($url);
	}

	//削除実行
	if ($action=='deletesubmit'){

		$i_login_id=$_POST['h_login_id'];
		$u_id=$_POST['u_id'];

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$logstr = "$systime $ip INFO：▼新着情報削除更新開始 \r\n";
		error_log($logstr,3,'../log/gen.log');
		$sql = sprintf("update den_new_info set del_flg=1 where id=%d",$u_id);
		$logstr = "$systime $ip INFO：新着情報削除更新 UPDATE SQL文： ".$sql."\r\n";
		error_log($logstr,3,'../log/gen.log');

		$result = mysql_query($sql,$db);
		if(!$result){
			db_disConn($result, $link);
			$logstr = "$systime ERR：新着情報DB削除更新異常！ \r\n";
			$logstr .= "$systime $ip INFO：▲新着情報削除更新異常終了 \r\n";
			error_log($logstr,3,'../log/gen.log');

			$err_cd_list[]="01";
			$_SESSION['err_cd_list']=$err_cd_list;
			$url= URL_PATH . "err.php";
			redirect($url);
		}

		mysql_close($db);
		$logstr = "$systime $ip INFO：▲新着情報削除更新正常終了！！ \r\n";
		error_log($logstr,3,'../log/gen.log');


		$url= URL_PATH . "m_news.php?action=search&role=".$role."&l_id=".$login_user;
		redirect($url);
	}
?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $sub_title; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../js/multiselectSrc/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="../js/assets/style.css" />
<link rel="stylesheet" type="text/css" href="../js/assets/prettify.css" />
<link rel="stylesheet" type="text/css" href="../js/ui/jquery-ui.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>

<script type="text/javascript" src="../js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/assets/prettify.js"></script>
<script type="text/javascript" src="../js/multiselectSrc/jquery.multiselect.js"></script>
<script type="text/javascript">
$(function(){
	$("#i_prop_id").multiselect({
		noneSelectedText: "選択してください",
		checkAllText: "全て選択",
		uncheckAllText: '全て選択解除',
		selectedList:3,
		selectedText: '# 個選択',
		height:'auto',
		minWidth:500
	});
	$("#i_area_id").multiselect({
		noneSelectedText: "選択してください",
		checkAllText: "全て選択",
		uncheckAllText: '全て選択解除',
		selectedList:8,
		selectedText: '# 個選択',
		height:'auto',
		minWidth:500
	});
	$("#i_sex").multiselect({
		noneSelectedText: "選択してください",
		checkAllText: "全て選択",
		uncheckAllText: '全て選択解除',
		selectedList:2,
		selectedText: '# 個選択',
		height:'auto'
	});
});
</script>
</head>
<?php
	if ($is_disabled=="disabled='disabled'"){
		echo"
	<body onload='setDisableMultiselect();'>
	";
	}else{
		echo"
	<body>
	";
	}
?>
<div class='main'>
<div class='subtitle'><?php echo $sub_title;?></div>
<form enctype='multipart/form-data' method='post' name='upform'>
<div class='input-area'>
	<label class='w100'>日付</label>
		<input type='text' name='i_date' id='i_date' class='w200' placeholder='<?php if($havePlaceholder=='YES') echo$sysdate;?>' value='<?php echo $i_date; ?>' <?php echo $is_disabled;?>/>
	<div style='clear:both;'></div>
	<label class='w100'>提携会社</label>
		<select name='i_supply_id' id='i_supply_id' class='re_select w300' <?php echo $is_disabled;?>>
			<option value=''></option>
				<?php
		        while($arr_list_row=mysql_fetch_array($result_list)){
		        	if($arr_list_row[brand_id]==$i_supply_id){
		        		echo"<option value=".$arr_list_row[brand_id]." selected >".$arr_list_row[brand_name]. "</option>";
		        	}else{
		        		echo"<option value=".$arr_list_row[brand_id]." >".$arr_list_row[brand_name]. "</option>";
		        	}
		        }
        		?>
		</select>
	<div style='clear:both;'></div>
	<label class='w100'>タイトル</label>
		<input type='text' name='i_title' id='i_title' class='w500' placeholder='<?php if($havePlaceholder=='YES') echo'';?>' value='<?php echo $i_title;?>' <?php echo $is_disabled;?>/>
	<div style='clear:both;'></div>
	<label class='w100'>ステータス</label>
		<select name='i_status' id='i_status' class='re_select fl mt3' <?php echo $is_disabled;?>>
			<option value='0'<?php if ($i_status=='0') {echo 'selected';}?>>承認待</option>
			<option value='1'<?php if ($i_status=='1') {echo 'selected';}?>>公開中</option>
			<option value='2'<?php if ($i_status=='2') {echo 'selected';}?>>非公開</option>
		</select>
	<input type='checkbox' name='i_push_flg' id='i_push_flg' value='1' class='ml240 fl'<?php echo $is_disabled;if ($i_push_flg=='1') echo 'checked ' ?>/><span class='input-span mr20'>プッシュ通知</span>
	<div style='clear:both;'></div>
	<label class='w100'>対象会員種別</label>
		<select name='i_prop_id' id='i_prop_id' multiple="multiple" size="5">
			<?php
				$i=0;
				while($arr_list_row=mysql_fetch_array($result_list_prop)){
					if($arr_list_row[rank_id]==$arr_prop_id[$i]){
						echo"<option value=".$arr_list_row[rank_id]." selected >".$arr_list_row[rank_name]. "</option>";
						$i++;
					}else{
						echo"<option value=".$arr_list_row[rank_id]." >".$arr_list_row[rank_name]. "</option>";
					}
				}
			?>
		</select>
	<div style='clear:both;'></div>
	<label class='w100'>対象都道府県</label>
		<select id ="i_area_id" name="i_area_id" multiple="multiple" size="8">
			<optgroup label="北海道・東北">
				<?php
				$i=0;
				$j=0;
				while($arr_list_row=mysql_fetch_array($result_list_area1)){
					if($arr_list_row[name]==$arr_area_id[$j]){
						echo"<option value=".$arr_list_row[name]." selected >".$arr_list_row[name]. "</option>";
						$i++;
						$j++;
					}else{
						echo"<option value=".$arr_list_row[name]." >".$arr_list_row[name]. "</option>";
					}
				}
				?>
			</optgroup>
			<optgroup label="関東">
				<?php
				$i=0;
					while($arr_list_row=mysql_fetch_array($result_list_area2)){
						if($arr_list_row[name]==$arr_area_id[$j]){
							echo"<option value=".$arr_list_row[name]." selected >".$arr_list_row[name]. "</option>";
							$i++;
							$j++;
						}else{
							echo"<option value=".$arr_list_row[name]." >".$arr_list_row[name]. "</option>";
						}
					}
				?>
			</optgroup>
			<optgroup label="中部">
				<?php
				$i=0;
					while($arr_list_row=mysql_fetch_array($result_list_area3)){
						if($arr_list_row[name]==$arr_area_id[$j]){
							echo"<option value=".$arr_list_row[name]." selected >".$arr_list_row[name]. "</option>";
							$i++;
							$j++;
						}else{
							echo"<option value=".$arr_list_row[name]." >".$arr_list_row[name]. "</option>";
						}
					}
				?>
			</optgroup>
			<optgroup label="関西">
				<?php
				$i=0;
					while($arr_list_row=mysql_fetch_array($result_list_area4)){
						if($arr_list_row[name]==$arr_area_id[$j]){
							echo"<option value=".$arr_list_row[name]." selected >".$arr_list_row[name]. "</option>";
							$i++;
							$j++;
						}else{
							echo"<option value=".$arr_list_row[name]." >".$arr_list_row[name]. "</option>";
						}
					}
				?>
			</optgroup>
			<optgroup label="中国">
				<?php
				$i=0;
					while($arr_list_row=mysql_fetch_array($result_list_area5)){
						if($arr_list_row[name]==$arr_area_id[$j]){
							echo"<option value=".$arr_list_row[name]." selected >".$arr_list_row[name]. "</option>";
							$i++;
							$j++;
						}else{
							echo"<option value=".$arr_list_row[name]." >".$arr_list_row[name]. "</option>";
						}
					}
				?>
			</optgroup>
			<optgroup label="四国">
				<?php
				$i=0;
					while($arr_list_row=mysql_fetch_array($result_list_area6)){
						if($arr_list_row[name]==$arr_area_id[$j]){
							echo"<option value=".$arr_list_row[name]." selected >".$arr_list_row[name]. "</option>";
							$i++;
							$j++;
						}else{
							echo"<option value=".$arr_list_row[name]." >".$arr_list_row[name]. "</option>";
						}
					}
				?>
			</optgroup>
			<optgroup label="九州">
				<?php
				$i=0;
					while($arr_list_row=mysql_fetch_array($result_list_area7)){
						if($arr_list_row[name]==$arr_area_id[$j]){
							echo"<option value=".$arr_list_row[name]." selected >".$arr_list_row[name]. "</option>";
							$i++;
							$j++;
						}else{
							echo"<option value=".$arr_list_row[name]." >".$arr_list_row[name]. "</option>";
						}
					}
				?>
			</optgroup>
		</select>
	<div style='clear:both;'></div>
	<label class='w100'>対象年齢</label>
		<input type='text' class='w50' name='i_age_from' id='i_age_from' <?php echo $is_disabled;?> value='<?php echo $i_target_age_from;?>'/>
		<label class='tcenter mg0'>〜</label>
		<input type='text' class='w50' name='i_age_to' id='i_age_to' <?php echo $is_disabled;?> value='<?php echo $i_target_age_to;?>'/>
	<div style='clear:both;'></div>
	<label class='w100'>対象性別</label>
		<select id ="i_sex" multiple="multiple" name="i_sex" size="2">
			<option value="1" <?php if ($arr_sex[0]=='1') echo 'selected';?>>男性</option>
			<option value="0" <?php if ($arr_sex[1]=='0') echo 'selected';?>>女性</option>
		</select>
	<div style='clear:both;'></div>
	<label class='w100'>詳細</label>
	<textarea name='i_content' id='i_content' placeholder='<?php if($havePlaceholder=='YES') echo'詳細内容を入力してください';?>' <?php echo $is_disabled;?>><?php echo htmlspecialchars_decode($i_content);?></textarea>
	<div style='clear:both;'></div>
	<label class='w100'>画像</label>
	<?php
	if($action=='confirm'){
		if($update_flg=='1'){
			if($_FILES["upfile1"]["tmp_name"]!=''){
				echo "<img src=\"".$show_dest1."\" width='160px' height='113px' class='mr20'>";
			}else{
				echo "<img src=\"".$h_img_path1."\" width='160px' height='113px' class='mr20'>";
			}
			if($_FILES["upfile2"]["tmp_name"]!=''){
				echo "<img src=\"".$show_dest2."\" width='160px' height='113px' class='mr20'>";
			}else{
				echo "<img src=\"".$h_img_path2."\" width='160px' height='113px' class='mr20'>";
			}
			if($_FILES["upfile3"]["tmp_name"]!=''){
				echo "<img src=\"".$show_dest3."\" width='160px' height='113px' class='mr20'>";
			}else{
				echo "<img src=\"".$h_img_path3."\" width='160px' height='113px' class='mr20'>";
			}
		}else{
			if($_FILES["upfile1"]["tmp_name"]!=''){
				echo "<img src=\"".$show_dest1."\" width='160px' height='113px' class='mr20'>";
			}
			if($_FILES["upfile2"]["tmp_name"]!=''){
				echo "<img src=\"".$show_dest2."\" width='160px' height='113px' class='mr20'>";
			}
			if($_FILES["upfile3"]["tmp_name"]!=''){
				echo "<img src=\"".$show_dest3."\" width='160px' height='113px' class='mr20'>";
			}
			if(($_FILES["upfile1"]["tmp_name"]=='')&&($_FILES["upfile2"]["tmp_name"]=='')&&($_FILES["upfile3"]["tmp_name"]=='')){
				echo "<span class='input-span'>添付画像なし</span>";
			}
		}
	}
	elseif($action=='update'||$action=='delete'||$action=='edit'){
		echo "<img src=\"".$h_img_path1."\" width='160px' height='113px' class='mr20'>
              <input name='upfile1' id='upfile1' type='file'></br>
        ";
		echo "<img src=\"".$h_img_path2."\" width='160px' height='113px' class='mr20 ml143'>
              <input name='upfile2' id='upfile2' type='file'></br>
        ";
		echo "<img src=\"".$h_img_path3."\" width='160px' height='113px' class='mr20 ml143'>
              <input name='upfile3' id='upfile3' type='file'></br>
        ";
	}
	else{
		echo"
			<input name='upfile1' id='upfile1' type='file'></br>
			<input name='upfile2' id='upfile2' type='file'></br>
			<input name='upfile3' id='upfile3' type='file'>
		";
	}
	?>
	<div style='clear:both;'></div>
	<input type="hidden" name="h_destination1" value="<?php echo $destination1;?>"/>
	<input type="hidden" name="h_destination2" value="<?php echo $destination2;?>"/>
	<input type="hidden" name="h_destination3" value="<?php echo $destination3;?>"/>
	<input type="hidden" name="h_prop_id" id="h_prop_id" value="<?php echo $i_prop_id;?>"/>
	<input type="hidden" name="h_area_id" id="h_area_id" value="<?php echo $i_area_id;?>"/>
	<input type="hidden" name="h_sex" id="h_sex" value="<?php echo $i_sex;?>"/>
	<input type="hidden" name="h_img_path1" value="<?php echo $h_img_path1;?>"/>
    <input type="hidden" name="h_img_path2" value="<?php echo $h_img_path2;?>"/>
    <input type="hidden" name="h_img_path3" value="<?php echo $h_img_path3;?>"/>
    <input type="hidden" name="update_flg" value="<?php echo $update_flg;?>"/>
    <input type="hidden" name="u_id" value="<?php echo $u_id;?>" />
	<?php
	if($action=='confirm'){
		if($update_flg=='1'){
			echo "
                <input type='button' class='buttonS bGreen ml143 w200 mt40' value='訂正' onclick='editMode();'/>
                <input type='button' class='buttonS bGreen ml100 w200 mt40' value='更新' onclick='updateSubmit();'/>
            ";
		}else{
			echo "
                <input type='button' class='buttonS bGreen ml143 w200 mt40' value='訂正' onclick='editMode();'/>
                <input type='button' class='buttonS bGreen ml100 w200 mt40' value='送信' onclick='confirmSubmit();'/>
            ";
		}
	}else{
		echo "<input type='button' class='buttonS bGreen ml143 w200 mt40' value='確認画面へ' onclick='moveConfirm();'/>";
	}
	?>
</div>

<script type="text/javascript" language="javascript">
	function moveConfirm() {
		//日付
		 if(document.upform.i_date.value == ""){
		  alert("日付を入力してください。");
		  document.upform.i_date.focus();
		  return false;
		 }
		 //配信元，提携会社
		 if(document.upform.i_supply_id.value == ""){
		  alert("提携会社を選択してください。");
		  document.upform.i_supply_id.focus();
		  return false;
		 }
		 //タイトル
		 if(document.upform.i_title.value == ""){
		  alert("タイトルを入力してください。");
		  document.upform.i_title.focus();
		  return false;
		 }
		 //詳細
		 if(document.upform.i_content.value == ""){
		  alert("詳細内容を入力してください。");
		  document.upform.i_content.focus();
		  return false;
		 }

		var val_prop = $("#i_prop_id").multiselect("getChecked").map(function(){return this.value;}).get();
		var val_area_id = $("#i_area_id").multiselect("getChecked").map(function(){return this.value;}).get();
		var val_sex = $("#i_sex").multiselect("getChecked").map(function(){return this.value;}).get();

		document.upform.h_prop_id.value=val_prop;
		document.upform.h_area_id.value=val_area_id;
		document.upform.h_sex.value=val_sex;
		//submit
		document.upform.action="?action=confirm";
		document.upform.submit();
	}
	function confirmSubmit() {
		document.getElementById('i_date').disabled=false;
		document.getElementById('i_supply_id').disabled=false;
		document.getElementById('i_title').disabled=false;
		document.getElementById('i_status').disabled=false;
		document.getElementById('i_content').disabled=false;
//		document.getElementById('i_area_id').disabled=false;
		document.getElementById('i_age_from').disabled=false;
		document.getElementById('i_age_to').disabled=false;
//		document.getElementById('i_sex').disabled=false;
//		document.getElementById('i_prop_id').disabled=false;
		document.getElementById('i_push_flg').disabled=false;

		var $widget = $("select").multiselect();
		$widget.multiselect('enable');

		document.upform.action="?action=insert";
		document.upform.submit();
	}
	function updateSubmit() {
		document.getElementById('i_date').disabled=false;
		document.getElementById('i_supply_id').disabled=false;
		document.getElementById('i_title').disabled=false;
		document.getElementById('i_status').disabled=false;
		document.getElementById('i_content').disabled=false;
//		document.getElementById('i_area_id').disabled=false;
		document.getElementById('i_age_from').disabled=false;
		document.getElementById('i_age_to').disabled=false;
//		document.getElementById('i_sex').disabled=false;
//		document.getElementById('i_prop_id').disabled=false;
		document.getElementById('i_push_flg').disabled=false;

		var $widget = $("select").multiselect();
		$widget.multiselect('enable');

		document.upform.action="?action=updatesubmit";
		document.upform.submit();
	}
	function editMode(){

		document.getElementById('i_date').disabled=false;
		document.getElementById('i_supply_id').disabled=false;
		document.getElementById('i_title').disabled=false;
		document.getElementById('i_status').disabled=false;
		document.getElementById('i_content').disabled=false;
//		document.getElementById('i_area_id').disabled=false;
		document.getElementById('i_age_from').disabled=false;
		document.getElementById('i_age_to').disabled=false;
//		document.getElementById('i_sex').disabled=false;
//		document.getElementById('i_prop_id').disabled=false;
		document.getElementById('i_push_flg').disabled=false;

		var $widget = $("select").multiselect();
		$widget.multiselect('enable');

		document.upform.action="?action=edit";
		document.upform.submit();
	}
	function setDisableMultiselect(){
		var state = false;
		var $widget_prop = $("#i_prop_id").multiselect();
		var $widget_area = $("#i_area_id").multiselect();
		var $widget_sex = $("#i_sex").multiselect();
		state = !state;
		$widget_prop.multiselect(state ? 'disable' : 'enable');
		$widget_area.multiselect(state ? 'disable' : 'enable');
		$widget_sex.multiselect(state ? 'disable' : 'enable');
	}
</script>
</form>
</div>
</body>
</html>