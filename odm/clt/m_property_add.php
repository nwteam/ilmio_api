<?php
	//include
	require '../util/include.php';

	$action = $_GET['action'];
	$sysdate=date('Y-m-d',time());
	$systime=date('Y-m-d H:i:s',time());
	$ip=get_real_ip();

	if ($action=='confirm'){
		$sub_title='紹介元属性管理　- 新規追加確認 -';
		$is_disabled="disabled='disabled'";
		$havePlaceholder='NO';

		//hidden項目
		$u_id = $_POST['u_id'];
		$update_flg=$_POST['update_flg'];

		//form項目
		//ユーザログインコード
		$i_rank_id = $_POST['i_rank_id'];
		//ユーザログインコード（更新前）
		$u_rank_id = $_POST['u_rank_id'];
		//紹介元属性名
		$i_rank_name =$_POST['i_rank_name'];
		//ステータス
		$i_status = $_POST['i_status'];
		//有効期間From
		$i_expired_date_from= $_POST['i_expired_date_from'];
		if($i_expired_date_from==''){
			$i_expired_date_from=$sysdate;
		}
		//有効期間To
		$i_expired_date_to= $_POST['i_expired_date_to'];
		if($i_expired_date_to==''){
			$i_expired_date_to='2030-12-31';
		}
		//詳細
		$i_memo = $_POST['i_memo'];
		$i_memo=htmlspecialchars($i_memo);
	}
	elseif ($action=='update'){

		$sub_title='紹介元情報管理　- 編集 -';
		$is_disabled="";
		$havePlaceholder='NO';

		$u_id = $_GET['u_id'];

		$update_flg='1';

		$logstr = "$systime $ip INFO：▼紹介元情報取得開始：id = $u_id \r\n";
		error_log($logstr,3,'../log/gen.log');

		if($u_id==''){

			db_disConn($result, $link);
			$logstr = "$systime ERR：id取得エラー！ \r\n";
			$logstr .= "$systime $ip INFO：▲紹介元情報取得異常終了 \r\n";
			error_log($logstr,3,'../log/gen.log');

			$err_cd_list[]="99";
			$_SESSION['err_cd_list']=$err_cd_list;
			$url= URL_PATH . "err.php";
			redirect($url);
		}

		//情報取得
		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}
		$dns = mysql_select_db(DB_NAME,$db);
		if(!$dns){
			die("connot use db:" . mysql_error());
		}
		mysql_set_charset('utf8');
		$sqlall = "select * from user_rank WHERE 1";
		$sqlall .= " and id = $u_id";

		$result = mysql_query($sqlall,$db);
		$rs=mysql_fetch_object($result);

		//ユーザログインコード
		$i_rank_id = $rs->rank_id;
		//紹介元属性名
		$i_rank_name = $rs->rank_name;
		//ステータス
		$i_status =$rs->status;
		//有効期間From
		$i_expired_date_from=$rs->expired_date_from;
		$i_expired_date_from=date('Y-m-d',$i_expired_date_from);
		//有効期間To
		$i_expired_date_to=$rs->expired_date_to;
		$i_expired_date_to=date('Y-m-d',$i_expired_date_to);
		//詳細
		$i_memo =$rs->memo;
		$i_memo = htmlspecialchars($i_memo);

		mysql_close($db);

		$logstr = "$systime $ip INFO：▲紹介元情報取得正常終了：id = $u_id \r\n";
		error_log($logstr,3,'../log/gen.log');

	}
	elseif($action=='edit'){
		$sub_title='紹介元属性管理　- 新規追加訂正 -';
		$is_disabled="";
		$havePlaceholder='YES';

		//form項目
		//ユーザログインコード
		$i_rank_id = $_POST['i_rank_id'];
		//紹介元属性名
		$i_rank_name =$_POST['i_rank_name'];
		//ステータス
		$i_status = $_POST['i_status'];
		//有効期間From
		$i_expired_date_from= $_POST['i_expired_date_from'];
		if($i_expired_date_from==''){
			$i_expired_date_from=$sysdate;
		}
		//有効期間To
		$i_expired_date_to= $_POST['i_expired_date_to'];
		if($i_expired_date_to==''){
			$i_expired_date_to='2030-12-31';
		}
		//詳細
		$i_memo = $_POST['i_memo'];
		$i_memo=htmlspecialchars($i_memo);
	}
	else{
		$sub_title='紹介元属性管理　- 新規追加 -';
		$is_disabled="";
		$havePlaceholder='YES';
		//form項目
		//ユーザログインコード
		$i_rank_id = '';
		//紹介元属性名
		$i_rank_name ='';
		//ステータス
		$i_status = '';
		//有効期間From
		$i_expired_date_from='';
		//有効期間To
		$i_expired_date_to='';
		$i_memo = '';
	}

	//insert
	if ($action=='insert'){
		$logstr = "$systime $ip INFO：▼紹介元属性情報登録開始 \r\n";
		error_log($logstr,3,'../log/gen.log');

		//form項目
		//ユーザログインコード
		$i_rank_id = $_POST['i_rank_id'];
		//紹介元属性名
		$i_rank_name =$_POST['i_rank_name'];
		//ステータス
		$i_status = $_POST['i_status'];
		//有効期間From
		$i_expired_date_from= $_POST['i_expired_date_from'];
		if($i_expired_date_from==''){
			$i_expired_date_from=$sysdate;
		}
		//有効期間To
		$i_expired_date_to= $_POST['i_expired_date_to'];
		if($i_expired_date_to==''){
			$i_expired_date_to='2030-12-31';
		}
		//詳細
		$i_memo = $_POST['i_memo'];
		$i_memo=htmlspecialchars($i_memo);

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("insert into user_rank (rank_id,rank_name,status,expired_date_from,expired_date_to,memo,insert_time)
						values (%s,%s,%d,%d,%d,%s,%d)",
						quote_smart($i_rank_id,true),
						quote_smart($i_rank_name,true),
						$i_status,
						strtotime($i_expired_date_from),
						strtotime($i_expired_date_to),
						quote_smart($i_memo,true),
						strtotime($systime));

		$logstr = "$systime $ip INFO：紹介元属性情報登録 INSERT SQL文： ".$sql."\r\n";
		error_log($logstr,3,'../log/gen.log');

		$result = mysql_query($sql,$db);
		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
			$logstr = "$systime ERR：紹介元属性情報DB登録異常！ \r\n";
        	$logstr .= "$systime $ip INFO：▲紹介元属性情報登録異常終了 \r\n";
			error_log($logstr,3,'../log/gen.log');

			$err_cd_list[]="01";
			$_SESSION['err_cd_list']=$err_cd_list;
			$url= URL_PATH . "err.php";
			redirect($url);
		}

		mysql_close($db);
		$logstr = "$systime $ip INFO：▲紹介元属性情報登録正常終了！！ \r\n";
		error_log($logstr,3,'../log/gen.log');
		$url= URL_PATH . "m_property.php?action=search";
		redirect($url);
	}
	//更新実行
	if ($action=='updatesubmit'){

		$u_id=$_POST['u_id'];

		//form項目
		//ユーザログインコード
		$i_rank_id = $_POST['i_rank_id'];
		//ユーザログインコード(更新前)
		$u_rank_id = $_POST['u_rank_id'];
		//紹介元属性名
		$i_rank_name =$_POST['i_rank_name'];
		//ステータス
		$i_status = $_POST['i_status'];
		//有効期間From
		$i_expired_date_from= $_POST['i_expired_date_from'];
		if($i_expired_date_from==''){
			$i_expired_date_from=$sysdate;
		}
		//有効期間To
		$i_expired_date_to= $_POST['i_expired_date_to'];
		if($i_expired_date_to==''){
			$i_expired_date_to='2030-12-31';
		}
		//詳細
		$i_memo = $_POST['i_memo'];
		$i_memo=htmlspecialchars($i_memo);

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$logstr = "$systime $ip INFO：▼紹介元属性情報更新開始 \r\n";
		error_log($logstr,3,'../log/gen.log');
		$sql = sprintf("update user_rank set
                                rank_id='%s',
                                rank_name='%s',
                                status=%d,
                                expired_date_from=%d,
                                expired_date_to=%d,
                                memo='%s',
                                update_time=%d
                                where id=%d",
			$i_rank_id,
			$i_rank_name,
			$i_status,
			strtotime($i_expired_date_from),
			strtotime($i_expired_date_to),
			$i_memo,
			strtotime($systime),
			$u_id
		);
		$logstr = "$systime $ip INFO：紹介元属性情報更新 UPDATE SQL文： ".$sql."\r\n";
		error_log($logstr,3,'../log/gen.log');

		$result = mysql_query($sql,$db);
		if(!$result){
			db_disConn($result, $link);
			$logstr = "$systime ERR：紹介元属性情報DB更新異常！ \r\n";
			$logstr .= "$systime $ip INFO：▲紹介元属性情報更新異常終了 \r\n";
			error_log($logstr,3,'../log/gen.log');

			$err_cd_list[]="01";
			$_SESSION['err_cd_list']=$err_cd_list;
			$url= URL_PATH . "err.php";
			redirect($url);
		}

		$logstr = "$systime $ip INFO：▲紹介元属性情報更新正常終了！！ \r\n";
		error_log($logstr,3,'../log/gen.log');

		$logstr = "$systime $ip INFO：▼紹介元属性情報更新、ユーザー関連更新開始 \r\n";
		error_log($logstr,3,'../log/gen.log');
		$sql = sprintf("update users set rank_id='%s' where rank_id='%s'",$i_rank_id,$u_rank_id);
		$logstr = "$systime $ip INFO：紹介元属性情報更新、ユーザー関連更新 UPDATE SQL文： ".$sql."\r\n";
		error_log($logstr,3,'../log/gen.log');

		$result = mysql_query($sql,$db);
		if(!$result){
			db_disConn($result, $link);
			$logstr = "$systime ERR：紹介元属性情報更新、ユーザー関連更新DB更新異常！ \r\n";
			$logstr .= "$systime $ip INFO：▲紹介元属性情報更新、ユーザー関連更新異常終了 \r\n";
			error_log($logstr,3,'../log/gen.log');

			$err_cd_list[]="01";
			$_SESSION['err_cd_list']=$err_cd_list;
			$url= URL_PATH . "err.php";
			redirect($url);
		}
		$logstr = "$systime $ip INFO：▲紹介元属性情報更新、ユーザー関連更新正常終了！！ \r\n";
		error_log($logstr,3,'../log/gen.log');


		$logstr = "$systime $ip INFO：▼紹介元属性情報更新、KP関連更新開始 \r\n";
		error_log($logstr,3,'../log/gen.log');
		$sql = sprintf("update den_kp_code set rank_id='%s' where rank_id='%s'",$i_rank_id,$u_rank_id);
		$logstr = "$systime $ip INFO：紹介元属性情報更新、KP関連更新 UPDATE SQL文： ".$sql."\r\n";
		error_log($logstr,3,'../log/gen.log');

		$result = mysql_query($sql,$db);
		if(!$result){
			db_disConn($result, $link);
			$logstr = "$systime ERR：紹介元属性情報更新、KP関連更新DB更新異常！ \r\n";
			$logstr .= "$systime $ip INFO：▲紹介元属性情報更新、KP関連更新異常終了 \r\n";
			error_log($logstr,3,'../log/gen.log');

			$err_cd_list[]="01";
			$_SESSION['err_cd_list']=$err_cd_list;
			$url= URL_PATH . "err.php";
			redirect($url);
		}


		$logstr = "$systime $ip INFO：▲紹介元属性情報更新、Sort関連更新正常終了！！ \r\n";
		error_log($logstr,3,'../log/gen.log');

		$logstr = "$systime $ip INFO：▼紹介元属性情報更新、Sort関連更新開始 \r\n";
		error_log($logstr,3,'../log/gen.log');
		$sql = sprintf("update den_rank_sort set rank_id='%s' where rank_id='%s'",$i_rank_id,$u_rank_id);
		$logstr = "$systime $ip INFO：紹介元属性情報更新、Sort関連更新 UPDATE SQL文： ".$sql."\r\n";
		error_log($logstr,3,'../log/gen.log');

		$result = mysql_query($sql,$db);
		if(!$result){
			db_disConn($result, $link);
			$logstr = "$systime ERR：紹介元属性情報更新、Sort関連更新DB更新異常！ \r\n";
			$logstr .= "$systime $ip INFO：▲紹介元属性情報更新、Sort関連更新異常終了 \r\n";
			error_log($logstr,3,'../log/gen.log');

			$err_cd_list[]="01";
			$_SESSION['err_cd_list']=$err_cd_list;
			$url= URL_PATH . "err.php";
			redirect($url);
		}
		$logstr = "$systime $ip INFO：▲紹介元属性情報更新、Sort関連更新正常終了！！ \r\n";
		error_log($logstr,3,'../log/gen.log');

		mysql_close($db);

		$url= URL_PATH . "m_property.php?action=search";

		redirect($url);
	}
?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $sub_title; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
</head>
<body>
<div class='main'>
<div class='subtitle'><?php echo $sub_title; ?></div>
<form enctype='multipart/form-data' method='post' name='upform'>
<div class='input-area'>
	<label class='w200'>ユーザログインコード</label>
		<input type='text' name='i_rank_id' id='i_rank_id' class='w500' value='<?php echo $i_rank_id;?>' <?php echo $is_disabled;?>/>
	<div style='clear:both;'></div>
	<label class='w200'>紹介元属性名</label>
	<input type='text' name='i_rank_name' id='i_rank_name' class='w500' value='<?php echo $i_rank_name;?>' <?php echo $is_disabled;?>/>
	<div style='clear:both;'></div>

	<label class='w200'>ステータス</label>
		<select name='i_status' id='i_status' class='re_select' <?php echo $is_disabled;?>>
			<option value='1'<?php if ($i_status=='1') {echo 'selected';}?>>アクティブ</option>
			<option value='0'<?php if ($i_status=='0') {echo 'selected';}?>>非アクティブ</option>
		</select>
	<div style='clear:both;'></div>
	<label class='w200'>有効期間</label>
        <input type='text' name='i_expired_date_from' id='i_expired_date_from' class='w100' value='<?php echo $i_expired_date_from;?>' <?php echo $is_disabled;?>/>
        <label class='tcenter mg0'>〜</label>
        <input type='text' name='i_expired_date_to' id='i_expired_date_to' class='w100' value='<?php echo $i_expired_date_to?>' <?php echo $is_disabled;?>/>
        <span class='input-span ml20 fl'>例：<?php echo $sysdate.' 〜 2024-12-31';?></span>
    <div style='clear:both;'></div>
	<label class='w200'>詳細</label>
	<textarea name='i_memo' id='i_memo'<?php echo $is_disabled;?>><?php echo htmlspecialchars_decode($i_memo);?></textarea>
	<div style='clear:both;'></div>
	<input type="hidden" name="u_id" value="<?php echo $u_id;?>" />
	<input type="hidden" name="u_rank_id" value="<?php if($action=='confirm'){echo $u_rank_id;}elseif($action=='update'){echo $i_rank_id;}?>" />
    <input type="hidden" name="update_flg" value="<?php echo $update_flg;?>"/>
	<?php
	if($action=='confirm'){
		if($update_flg=='1'){
			echo "
			<input type='button' class='input-button buttonS bGreen ml240 w200 mt40' value='訂正' onclick='editMode();'/>
			<input type='button' class='input-button buttonS bGreen ml100 w200 mt40' value='更新' onclick='updateSubmit();'/>
		";
		}else{
			echo "
			<input type='button' class='input-button buttonS bGreen ml240 w200 mt40' value='訂正' onclick='editMode();'/>
			<input type='button' class='input-button buttonS bGreen ml100 w200 mt40' value='送信' onclick='confirmSubmit();'/>
		";
		}
	}else{
		echo "<input type='button' class='input-button buttonS bGreen ml243 w200 mt40' value='確認画面へ' onclick='moveConfirm();'/>";
	}
	?>
</div>

<script type="text/javascript" language="javascript">
	function moveConfirm() {
		//ユーザログインコード
		if(document.upform.i_rank_id.value == ""){
		alert("ユーザログインコードを入力してください。");
		document.upform.i_rank_id.focus();
		return false;
		}
		//紹介元属性名
		if(document.upform.i_rank_name.value == ""){
		alert("紹介元属性名を入力してください。");
		document.upform.i_rank_name.focus();
		return false;
		}

		//submit
		document.upform.action="?action=confirm";
		document.upform.submit();
	}
	function confirmSubmit() {
		document.getElementById('i_rank_id').disabled=false;
		document.getElementById('i_rank_name').disabled=false;
		document.getElementById('i_status').disabled=false;
		document.getElementById('i_expired_date_from').disabled=false;
		document.getElementById('i_expired_date_to').disabled=false;
		document.getElementById('i_memo').disabled=false;

		document.upform.action="?action=insert";
		document.upform.submit();
	}
	function updateSubmit() {
		document.getElementById('i_rank_id').disabled=false;
		document.getElementById('i_rank_name').disabled=false;
		document.getElementById('i_status').disabled=false;
		document.getElementById('i_expired_date_from').disabled=false;
		document.getElementById('i_expired_date_to').disabled=false;
		document.getElementById('i_memo').disabled=false;

		document.upform.action="?action=updatesubmit";
		document.upform.submit();
	}

	function editMode(){
		document.getElementById('i_rank_id').disabled=false;
		document.getElementById('i_rank_name').disabled=false;
		document.getElementById('i_status').disabled=false;
		document.getElementById('i_expired_date_from').disabled=false;
		document.getElementById('i_expired_date_to').disabled=false;
		document.getElementById('i_memo').disabled=false;

		document.upform.action="?action=edit";
		document.upform.submit();
	}
</script>
</form>
</div>
</body>
</html>