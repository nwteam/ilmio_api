<?php
	//include
	require '../util/include.php';

    $sub_title='購入履歴データ結果取込画面';
	$systime=date('Y-m-d H:i:s',time());

	$role=$_SESSION['role'];
	$login_user=$_SESSION['login_user'];

    $db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
    if(!$db){
        die("connot connect:" . mysql_error());
    }
    $dns = mysql_select_db(DB_NAME,$db);
    if(!$dns){
        die("connot use db:" . mysql_error());
    }
    mysql_set_charset('utf8');

	$action = $_GET['action'];

	//提携先ID取得
	if($role=='2'){
		$sqlall = "select brand_id from brand WHERE 1 and del_flg=0 and login_id='".$login_user."'";
		$brand_list = mysql_query($sqlall,$db);
		$rs=mysql_fetch_object($brand_list);
		$brand_id=$rs->brand_id;
		$i_sts=2;
	}
	elseif($role=='3'){
		$sqlall = "select brand_id,shop_id from shop WHERE 1 and login_id='".$login_user."'";
		$brand_list = mysql_query($sqlall,$db);
		$rs=mysql_fetch_object($brand_list);
		$brand_id=$rs->brand_id;
		$shop_id=$rs->shop_id;
		$i_sts=1;
	}
	elseif($role=='1'){
		$i_sts=2;
	}
	else{
		$brand_id='';
		$shop_id='';
	}

	$msg_flg = 1;
	$msg="";
	if ($action == 'import') {
		$filename = $_FILES['file']['tmp_name'];
		if (empty ($filename)) {
			$msg_flg = 1;
			$msg = 'エラー：購入履歴のCSVファイルを選択してください。';
		}
		else{
			$handle = fopen($filename, 'r');
			$result = input_csv($handle);
			$len_result = count($result);
			if($len_result==0){
				$msg_flg = 1;
				$msg = 'エラー：インポート対象レコードがありません。';
			}
			else{
				for ($i = 1; $i < $len_result; $i++) {
					$date        = strtotime($result[$i][0]);
					$time_slot   = $result[$i][1];
					$amount      = $result[$i][2];
					$commission  = $result[$i][3];
					$property_id = $result[$i][4];
					if($role=='2'||$role=='3'){
						if($brand_id==''){
							$brand_id = $result[$i][5];
						}
					}else{
						$brand_id = $result[$i][5];
					}
					if($role=='3'){
						if($shop_id==''){
							$shop_id  = $result[$i][6];
						}
					}else{
						$shop_id  = $result[$i][6];
					}
					$period      = $result[$i][7];
					$sex         = $result[$i][8];
					$area_id     = $result[$i][9];
					$note        = $result[$i][10];
					$rep_month=date('Y年m月',strtotime($result[$i][0]));


//					$sqlall = "select id from den_history_info WHERE 1 and status<>3 
//								and date=".$date."
//								and time_slot='".$time_slot."'
//								and rank_id='".$property_id."'
//								and brand_id=".$brand_id."
//								and amount=".$amount."
//								and commission=".$commission."
//								and shop_id=".$shop_id;
//echo $sqlall."<br>";
//					$result_id = mysql_query($sqlall,$db);
//					while($rs=mysql_fetch_object($result_id)){
//						$sql_update = sprintf("UPDATE den_history_info SET status=3 WHERE id = %d",$rs->id);
//						$sql_update_result = mysql_query($sql_update,$db);
//echo $sql_update."<br>";
//					}

					$sql_insert = sprintf("insert into den_history_info (
                            date,
							time_slot,
							amount,
							commission,
							property_id,
							brand_id,
							shop_id,
							period,
							sex,
							area_id,
							medium,
							note,
							rank_id,
							address,
							status,
							rep_month,
                            insert_time
                            ) values
                            (%d,'%s',%d,%d,'%s',%d,%d,%d,%d,'%s','CSV','%s','%s','%s',%d,'%s',%d)",
						$date,
						$time_slot,
						$amount,
						$commission,
						$property_id,
						$brand_id,
						$shop_id,
						$period,
						$sex,
						$area_id,
						$note,
						$property_id,
						$area_id,
						$i_sts,
						$rep_month,
						strtotime($systime)
					);
//echo $sql_insert."<br>";
					$query = mysql_query($sql_insert,$db);

					if (!$query) {
						$msg_flg = 1;
						$msg = 'エラー：第'.($i).'行目以降の購入履歴データ取込は失敗。';
						break;
					}else{
						$msg_flg = 0;
						$msg = '購入履歴データを取込みました。';
					}
				}
			}
			fclose($handle);
		}
	}
	mysql_close($db);
	//die;
?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $sub_title; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
</head>
<body>
<div class='main'>
<div class='subtitle'><?php echo $sub_title; ?></div>
<div class='input-area'>
	<form action='?action=import' method='post' enctype='multipart/form-data'>
		<input type="text" readonly="readonly" name="file_name" id="file_name" class="w300" placeholder="ファイルを選択してください"/>
		<input type="file" name="file" style="display:none" onchange="file_change(this.value)"/>
		<input type="button" class="buttonS bGreen ml20 btn-width" value="選  択" onclick="file.click();" name="get_file"/>
		<input type="submit" class="buttonS bGreen ml20 btn-width" value="取  込"/>
	</form>
</div>
<div class='input-area'>
	<label>取込結果：
	<?php
		if ($msg_flg==0){
			echo "<font color='#33CC00'>$msg</font>";
		}
		else{
			echo "<font color='#FF0000'>$msg</font>";
		}
	?>
	</label>
</div>
</div>
</div>
<script type="text/javascript">
	function file_change(e)
    {
        document.getElementById("file_name").value = e;
    }
</script>
</body>
</html>
