<?php
	//include
	require '../util/include.php';
	session_start();
	$sub_title='新着情報管理　- 新着情報一覧 -';
	$systime=date('Y-m-d H:i:s',time());

	$role=$_SESSION['role'];
	$login_user=$_SESSION['login_user'];

	$action = $_GET['action'];

	//プルダウンリスト取得
	$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	if(!$db){
		die("connot connect:" . mysql_error());
	}
	$dns = mysql_select_db(DB_NAME,$db);
	if(!$dns){
		die("connot use db:" . mysql_error());
	}
	mysql_set_charset('utf8');

	if($role=='') {
		$role=$_GET['role'];
	}
	if($login_user=='') {
		$login_user=$_GET['l_id'];
	}
	if($role=='') {
		$role=$_POST['h_role'];
	}
	if($login_user=='') {
		$login_user=$_POST['h_login_user'];
	}

	//提携先プルダウンリスト取得
	if($role=='2'){
		$sqlall = "select * from brand WHERE 1 and del_flg=0 and login_id='".$login_user."'  order by brand_id,sort_order";
		$brand_list = mysql_query($sqlall,$db);
		$arr_list_row=mysql_fetch_array($brand_list);
		$brand_id=$arr_list_row[brand_id];
	}
	elseif($role=='3'){
		$sqlall = "select bd.* from brand bd,shop sp WHERE 1 and bd.brand_id=sp.brand_id
                        and bd.del_flg=0 and sp.login_id='".$login_user."'  order by bd.brand_id,bd.sort_order";
		$brand_list = mysql_query($sqlall,$db);
		$arr_list_row=mysql_fetch_array($brand_list);
		$brand_id=$arr_list_row[brand_id];
	}
	else{
		$brand_id='';
	}
	mysql_close($db);
	//Delete
	if ($action=='delete'){
		$u_id = $_GET['u_id'];

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("delete from den_new_info WHERE id = %d",$u_id);
		$result = mysql_query($sql,$db);

		mysql_close($db);

	}
	//Active
	if ($action=='active'){
		$u_id = $_GET['u_id'];

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("UPDATE den_new_info SET status =1,update_time=%d WHERE id = %d",strtotime($systime),$u_id);
		$result = mysql_query($sql,$db);

		mysql_close($db);

	}
	//unActive
	if ($action=='unactive'){
		$u_id = $_GET['u_id'];

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("UPDATE den_new_info SET status =2,update_time=%d WHERE id = %d",strtotime($systime),$u_id);
		$result = mysql_query($sql,$db);

		mysql_close($db);

	}
	//Search
	if ($action=='search'||$action=='active'||$action=='unactive'||$action=='delete'){

		$link = db_conn();
		mysql_set_charset('utf8');

		$page_size=10;

		if( isset($_GET['page']) ){
		   $page = intval( $_GET['page'] );
		}
		else{
		   $page = 1;
		}
		$rowCnt = 0;

		$s_status=$_POST['s_status'];
		if($_GET['s_sts']!='') {
			$s_status=$_GET['s_sts'];
		}

		//All
		$sqlall = "select dni.*,
					(select brand_name from brand dcdi where dcdi.brand_id=dni.supply_id) credit_name
					from den_new_info dni,brand b WHERE 1 and dni.del_flg=0 and b.brand_id=dni.supply_id ";

		if($s_status!='') {
			$sqlall .= " and dni.status = $s_status";
		}
		if($role=='2' && $brand_id!=''){
			$sqlall .= " and dni.supply_id=$brand_id";
		}
		if($role=='3' && $brand_id!=''){
			$sqlall .= " and dni.supply_id=$brand_id";
		}

		$result = mysql_query($sqlall,$link) or die(mysql_error());

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}
		$rowCntall=mysql_num_rows($result);

		//Select current all
		$sql = sprintf("%s order by dni.id desc  limit %d,%d",$sqlall,($page-1)*$page_size,$page_size);

		$result = mysql_query($sql,$link);

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}

		$rowCnt=mysql_num_rows($result);

		//paging
		if($rowCnt==0){
			$page_count = 0;
			db_disConn($result, $link);
		}
		else{
			if( $rowCntall<$page_size ){ $page_count = 1; }
			if( $rowCntall%$page_size ){
				$page_count = (int)($rowCntall / $page_size) + 1;
			}else{
				$page_count = $rowCntall / $page_size;
			}
		}
		$page_string = '';
		if (($page == 1)||($page_count == 1)){
		   $page_string .= 'トップページ|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		else{
		   $page_string .= '<a href=?action=search&page=1&s_sts='.$s_status.'>トップページ</a>|<a href=?action=search&page='.($page-1).'&s_sts='.$s_status.'>前頁</a>|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		if( ($page == $page_count) || ($page_count == 0) ){
		   $page_string .= '次頁|最終ページ';
		}
		else{
		   $page_string .= '<a href=?action=search&page='.($page+1).'&s_sts='.$s_status.'>次頁</a>|<a href=?action=search&page='.$page_count.'&s_sts='.$s_status.'>最終ページ</a>';
		}
	}



?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $sub_title; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../js/common.js"></script>
</head>
<body>
<div class='main'>
<div class='subtitle'><?php echo $sub_title; ?></div>
<form action='?action=search' method='post' name='form1'>
<div class='input-area'>
	<label class='search_label'>ステータス</label>
	<select name='s_status' id='s_status' class='re_select'>
		<option value='' <?php if ($s_status=='') echo 'selected';?>>&nbsp;</option>
		<option value='0' <?php if ($s_status=='0') echo 'selected';?>>承認待</option>
		<option value='1' <?php if ($s_status=='1') echo 'selected';?>>公開中</option>
		<option value='2' <?php if ($s_status=='2') echo 'selected';?>>非公開</option>
	</select>
	<input type='submit' class='buttonS bGreen ml20' value='絞り込み'/>
	<input type='button' class='buttonS bGreen ml20' value='新規登録' onclick='addInfo();'/>
</div>
<?php
if ($rowCnt>0){
	echo "
		<table width='98%' cellspacing='1' cellpadding='2'>
			<tr bgcolor='#DBE6F5'>
			  <td><span style='float:left; text-align:center;font-size:16px;'><font color=#666666>$page_string</font></span></td>
			</tr>
		</table>";
	echo "
		<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
			<tr bgcolor='#DBE6F5'>
              <th width='80px'>日付</th>
              <th width='240px'>タイトル</th>
              <th width='180px'>配信元</th>
              <th width='100px'>ステータス</th>
              <th width='124px'>操作</th>
              <th width='90px'>配信日時</th>
              <th width='50px'>編集</th>
              <th width='50px'>削除</th>
			</tr>
		</table>
	";
	$i=1;
	while($rs=mysql_fetch_object($result))
	{
	  echo "
			<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
				<tr align='left' bgcolor='#EEF2F4'>
					<td width='80px'align='center'>".date("Y-m-d",$rs->online_date)."</td>
					<td width='240px'>".$rs->title."</td>
					<td width='180px' align='center'>".$rs->credit_name."</td>
		";
		if($rs->status=='0'){
			echo"
				<td width='100px'align='center'>承認待</td>
				<td width='124px'align='center'>
					<input type='button' class='buttonS bGreen' value='公開する' onclick='changeToActive(".$rs->id.");'/>
				</td>
				";
		}
		elseif($rs->status=='1'){
			echo"
				<td width='100px'align='center'>公開中</td>
				<td width='124px'align='center'>
					<input type='button' class='buttonS bRed' value='非公開する' onclick='changeToUnActive(".$rs->id.");'/>
				</td>
				";
		}
		elseif($rs->status=='2'){
			echo"
				<td width='100px'align='center'>非公開</td>
				<td width='124px'align='center'>
					<input type='button' class='buttonS bRed' value='公開する' onclick='changeToActive(".$rs->id.");'/>
				</td>
				";
		}
		else{
			echo"
				<td width='100px'align='center'></td>
				<td width='124px'align='center'></td>
				";
		}
		if($rs->update_time==''){
			echo "<td width='90px'align='center'>".date("Y-m-d H:i:s",$rs->update_time)."</td>";
		}
		else{
			echo "<td width='90px'align='center'>".date("Y-m-d H:i:s",$rs->insert_time)."</td>";
		}
		echo"
					<td width='50px'align='center'><a href='#' onclick=\"editInfo('".$rs->id."')\">編集</a></td>
					<td width='50px'align='center'><a href='#' onclick=\"var ret=confirm('新着情報を削除します。よろしいですか？');if(ret)deleteInfo('".$rs->id."');\">削除</a></td>
				</tr>
			</table>
		";
		$i++;
	}
	echo "
		<table width='98%' cellspacing='1' cellpadding='2'>
			<tr bgcolor='#DBE6F5'>
			  <td><span style='float:left; text-align:center;font-size:16px;'><font color=#666666>$page_string</font></span></td>
			</tr>
		</table>";
	mysql_close($link);
}else{
	if ($action=='search'){
		echo "検索結果がありません。";
	}
}
?>
</form>
<script language="javascript" type="text/javascript">
	function addInfo() {
		var pageurl="m_news_add.php";
		window.location.href=pageurl;
	}
	function editInfo(u_id) {
		var pageurl="m_news_add.php?action=update&u_id="+u_id;
		window.location.href=pageurl;
	}
	function deleteInfo(u_id) {
		var pageurl="?action=delete&u_id="+u_id;
		window.location.href=pageurl;
	}
	function updateChange(u_id) {
		var pageurl="?action=update";
		window.location.href=pageurl;
	}
	function changeToActive(u_id) {
		var pageurl="?action=active&u_id="+u_id;
		window.location.href=pageurl;
	}
	function changeToUnActive(u_id) {
		var pageurl="?action=unactive&u_id="+u_id;
		window.location.href=pageurl;
	}
</script>
</div>
</body>
</html>