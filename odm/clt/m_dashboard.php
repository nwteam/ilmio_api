<?php
	//include
	require '../util/include.php';
	session_start();

	if(empty($_SESSION['login_user']))
	{
		header('Location: index.php');
	}else{
		$login_user=$_SESSION['login_user'];
	}

	$sysdate=date('Y-m-d',time());
	$systime_from=strtotime($sysdate.' 00:00:00');
	$systime_to=strtotime($sysdate.' 23:59:59');

	//新着情報件数取得
	$rowCnt_news_total=0;
	$rowCnt_news_today=0;
	$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	if(!$db){
		die("connot connect:" . mysql_error());
	}
	$dns = mysql_select_db(DB_NAME,$db);
	if(!$dns){
		die("connot use db:" . mysql_error());
	}
	mysql_set_charset('utf8');
	$sqlall = "select * from den_new_info WHERE 1";
	$sql_today = sprintf("select * from den_new_info WHERE insert_time>%d and insert_time<%d",$systime_from,$systime_to);
	$sql_role = "select * from den_admin WHERE 1";
	if($login_user!='') {
			$sql_role .= " and u_id = '$login_user'";
	}
	$result_all = mysql_query($sqlall,$db);
	$result_today = mysql_query($sql_today,$db);
	$result_role = mysql_query($sql_role,$db);
	$rowCnt_news_total=mysql_num_rows($result_all);
	$rowCnt_news_today=mysql_num_rows($result_today);
	mysql_close($db);

?>
<!DOCTYPE HTML>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<meta http-equiv='content-type' content='text/html; charset=utf-8' >
<meta http-equiv='content-style-type' content='text/css'>
<meta http-equiv='content-script-type' content='text/javascript'>
<link href='../css/common.css' type='text/css' rel='stylesheet'>
</head>
<body>
<?php
	$rs=mysql_fetch_object($result_role);
	if($rs->role=='0'){
		echo"
			<div class='main'>
				<div class='mainContent clearfix'>
					<div id='dashboard'>
						<h2 class='header'><span class='icon'></span>ウィジェット</h2>
						<div class='monitor'>
							<h4>サマリー情報</h4>
							<div class='clearfix'>
								<ul class='content'>
									<li>今日</li>
									<li class='news'>新着情報：<a href='m_news.php'><span class='count'><?php echo $rowCnt_news_today;?></span></a></li>
									<li class='introduce'>紹介元：<a href='m_property.php'><span class='count'>0</span></a></li>
									<li class='credit'>提携先：<a href='m_brand.php'><span class='count'>0</span></a></li>
									<li class='shop'>店舗：<a href='m_shop.php'><span class='count'>0</span></a></li>
									<li class='history'>最新購入：<a href='m_history.php'><span class='count'>0</span></a></li>
								</ul>
								<ul class='discussions'>
									<li>全体</li>
									<li class='news'>新着情報：<a href='m_news.php'><span class='count'><?php echo $rowCnt_news_total;?></span></a></li>
									<li class='introduce'>紹介元：<a href='m_property.php'><span class='count'>0</span></a></li>
									<li class='credit'>提携先：<a href='m_brand.php'><span class='count'>0</span></a></li>
									<li class='shop'>店舗：<a href='m_shop.php'><span class='count'>0</span></a></li>
									<li class='history'>最新購入：<a href='m_history.php'><span class='count'>0</span></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
	";
	}
	else{
		echo"
		<div class='main'>
			<div class='mainContent clearfix'>
			</div>
		</div>
		";
	}
?>
</body>
</html>