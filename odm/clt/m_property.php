<?php
	//include
	require '../util/include.php';

	$sub_title='紹介元属性管理　- 紹介元属性一覧 -';

	$role=$_SESSION['role'];
	$login_user=$_SESSION['login_user'];

	$action = $_GET['action'];

	//Delete
	if ($action=='delete'){
		$u_id = $_GET['u_id'];

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("delete from user_rank WHERE id = %d",$u_id);
		$result = mysql_query($sql,$db);

		mysql_close($db);

	}

	//Active
	if ($action=='active'){
		$u_id = $_GET['u_id'];

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("UPDATE user_rank SET status =1,update_time=%d WHERE id = %d",strtotime($systime),$u_id);
		$result = mysql_query($sql,$db);

		mysql_close($db);

	}
	//unActive
	if ($action=='unactive'){
		$u_id = $_GET['u_id'];

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("UPDATE user_rank SET status =0,update_time=%d WHERE id = %d",strtotime($systime),$u_id);
		$result = mysql_query($sql,$db);

		mysql_close($db);

	}

	//Search
	if ($action=='search'||$action=='active'||$action=='unactive'||$action=='delete'){

		$link = db_conn();
		mysql_set_charset('utf8');

		$page_size=50;

		if( isset($_GET['page']) ){
		   $page = intval( $_GET['page'] );
		}
		else{
		   $page = 1;
		}
		$rowCnt = 0;

		$s_status=$_POST['s_status'];

		//All
		$sqlall = "select * from user_rank WHERE 1";

		$result = mysql_query($sqlall,$link) or die(mysql_error());

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}
		$rowCntall=mysql_num_rows($result);

		//Select current all
		$sql = sprintf("%s order by id limit %d,%d",$sqlall,($page-1)*$page_size,$page_size);

		$result = mysql_query($sql,$link);

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}

		$rowCnt=mysql_num_rows($result);

		//paging
		if($rowCnt==0){
			$page_count = 0;
			db_disConn($result, $link);
		}
		else{
			if( $rowCntall<$page_size ){ $page_count = 1; }
			if( $rowCntall%$page_size ){
				$page_count = (int)($rowCntall / $page_size) + 1;
			}else{
				$page_count = $rowCntall / $page_size;
			}
		}
		$page_string = '';
		if (($page == 1)||($page_count == 1)){
		   $page_string .= 'トップページ|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		else{
		   $page_string .= '<a href=?action=search&page=1>トップページ</a>|<a href=?action=search&page='.($page-1).'>前頁</a>|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		if( ($page == $page_count) || ($page_count == 0) ){
		   $page_string .= '次頁|最終ページ';
		}
		else{
		   $page_string .= '<a href=?action=search&page='.($page+1).'>次頁</a>|<a href=?action=search&page='.$page_count.'>最終ページ</a>';
		}
	}



?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $sub_title; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../js/common.js"></script>
</head>
<body>
<div class='main'>
<div class='subtitle'><?php echo $sub_title; ?></div>
<form action='?action=search' method='post' name='form1'>
<div class='input-area'>
	<input type='button' class='input-button buttonS bGreen ml20' value='新規登録' onclick="move_to_add_page('m_property_add.php');"/>
</div>
<?php
if ($rowCnt>0){
	echo "
		<table width='98%' cellspacing='1' cellpadding='2'>
			<tr bgcolor='#DBE6F5'>
			  <td><span style='float:left; text-align:center;font-size:16px;'><font color=#666666>$page_string</font></span></td>
			</tr>
		</table>";
	echo "
		<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
			<tr bgcolor='#DBE6F5'>
              <th width='80px'>ID</th>
              <th width='120px'>ユーザーログインコード</th>
              <th width='240px'>紹介元属性</th>
              <th width='124px'>ステータス</th>
              <th width='124px'>操作</th>
              <th width='100px'>表示順編集</th>
              <th width='50px'>編集</th>
              <th width='60px'>削除</th>
			</tr>
		</table>
	";
	$i=1;
	while($rs=mysql_fetch_object($result))
	{
	  echo "
			<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
				<tr align='left' bgcolor='#EEF2F4'>
					<input type='hidden' name='u_id".$i."' value=".$rs->id."/>
					<input type='hidden' name='u_status_old".$i."' value=".$rs->status."/>
					<td width='80px'align='center'>".$rs->id."</td>
					<td width='120px'align='center'>".$rs->rank_id."</td>
					<td width='240px'>".$rs->rank_name."</td>
		";
		if($rs->status=='0'){
			echo"
				<td width='124px'align='center'>非アクティブ</td>
				<td width='124px'align='center'>
					<input type='button' class='buttonS bGreen' value='アクティブ' onclick='changeToActive(".$rs->id.");'/>
				</td>
				";
		}
		elseif($rs->status=='1'){
			echo"
				<td width='124px'align='center'>アクティブ</td>
				<td width='124px'align='center'>
					<input type='button' class='buttonS bRed' value='非アクティブ' onclick='changeToUnActive(".$rs->id.");'/>
				</td>
				";
		}
		else{
			echo"
				<td width='124px'align='center'></td>
				<td width='124px'align='center'></td>
				";
		}
		if($role=='1'){
			echo"
						<td width='100px'align='center'><a href='#' onclick=\"editSortInfo('".$rs->rank_id."','".$rs->id."')\">表示順編集</a></td>
                        <td width='50px'align='center'><a href='#' onclick=\"editInfo('".$rs->id."')\">編集</a></td>
                        <td width='60px'align='center'>
							<input type='button' class='buttonS bBlue' value='削除' onclick=\"var ret=confirm('紹介元属性情報を削除します。よろしいですか？');if(ret)deleteInfo('".$rs->id."');\">
						</td>
                    </tr>
                </table>
		    ";
		}
		else{
			echo"
						<td width='100px'align='center'>表示順編集</td>
                        <td width='50px'align='center'>編集</td>
                        <td width='60px'align='center'>削除</td>
                    </tr>
                </table>
		    ";
		}
		$i++;
	}
	echo "
		<table width='98%' cellspacing='1' cellpadding='2'>
			<tr bgcolor='#DBE6F5'>
			  <td><span style='float:left; text-align:center;font-size:16px;'><font color=#666666>$page_string</font></span></td>
			</tr>
		</table>";
	mysql_close($link);
}else{
	if ($action=='search'){
		echo "検索結果がありません。";
	}
}
?>
</form>
<script language="javascript" type="text/javascript">
	function addInfo() {
		var pageurl="m_property_add.php";
		window.location.href=pageurl;
	}
	function deleteInfo(u_id) {
		var pageurl="?action=delete&u_id="+u_id;
		window.location.href=pageurl;
	}
	function editInfo(u_id) {
		var pageurl="m_property_add.php?action=update&u_id="+u_id;
		window.location.href=pageurl;
	}
	function editSortInfo(u_id,id) {
		var pageurl="m_property_sort.php?action=search&u_id="+u_id+"&id="+id;
		window.location.href=pageurl;
	}
	function changeToActive(u_id) {
		var pageurl="?action=active&u_id="+u_id;
		window.location.href=pageurl;
	}
	function changeToUnActive(u_id) {
		var pageurl="?action=unactive&u_id="+u_id;
		window.location.href=pageurl;
	}
</script>
</div>
</body>
</html>