<?php
session_start();
	//include
	require '../util/include.php';

	$sub_title='店舗情報管理　- 店舗情報一覧 -';
	$systime=date('Y-m-d H:i:s',time());

    $role=$_SESSION['role'];
    $login_user=$_SESSION['login_user'];

	$action = $_GET['action'];


	$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	if(!$db){
		die("connot connect:" . mysql_error());
	}
	$dns = mysql_select_db(DB_NAME,$db);
	if(!$dns){
		die("connot use db:" . mysql_error());
	}

	mysql_set_charset('utf8');

    //カテゴリプルダウンリスト取得
	$sqlall = "select * from category WHERE 1 and is_show=1";
	$result_list_cate = mysql_query($sqlall,$db);

	if($role=='') {
		$role=$_GET['role'];
	}
	if($login_user=='') {
		$login_user=$_GET['l_id'];
	}
	if($role=='') {
		$role=$_POST['h_role'];
	}
	if($login_user=='') {
		$login_user=$_POST['h_login_user'];
	}

    //提携先プルダウンリスト取得
    if($role=='2'){
        $sqlall = "select * from brand WHERE 1 and del_flg=0 and login_id='".$login_user."'  order by brand_id,sort_order";
        $result_list_brand = mysql_query($sqlall,$db);
        $brand_is_disabled="disabled='disabled'";
    }
    elseif($role=='3'){
        $sqlall = "select bd.* from brand bd,shop sp WHERE 1 and bd.brand_id=sp.brand_id
                        and bd.del_flg=0 and sp.login_id='".$login_user."'  order by bd.brand_id,bd.sort_order";
        $result_list_brand = mysql_query($sqlall,$db);

        $brand_is_disabled="disabled='disabled'";
    }
    else{
        $sqlall = "select * from brand WHERE 1 and del_flg=0 order by brand_id,sort_order";
        $result_list_brand = mysql_query($sqlall,$db);
        $brand_is_disabled='';
    }

	//都道府県プルダウンリスト取得
	$sqlall = "select * from den_area WHERE 1";
	$result_list_area = mysql_query($sqlall,$db);
	mysql_close($db);

	//Active
	if ($action=='active'){
		$u_id = $_GET['u_id'];

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("UPDATE shop SET status =1,update_time=%d WHERE shop_id = %d",strtotime($systime),$u_id);
		$result = mysql_query($sql,$db);

		mysql_close($db);

	}
	//unActive
	if ($action=='unactive'){
		$u_id = $_GET['u_id'];

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("UPDATE shop SET status =0,update_time=%d WHERE shop_id = %d",strtotime($systime),$u_id);
		$result = mysql_query($sql,$db);

		mysql_close($db);

	}

	//Search
	if ($action=='search'||$action=='active'||$action=='unactive'){

		$link = db_conn();
		mysql_set_charset('utf8');

		$page_size=50;

		if( isset($_GET['page']) ){
		   $page = intval( $_GET['page'] );
		}
		else{
		   $page = 1;
		}
		$rowCnt = 0;

		$s_area_id=$_POST['s_area_id'];
		$s_brand_id=$_POST['s_brand_id'];

        if($role!='1') {
            $h_brand_id=$_POST['h_brand_id'];
            if($h_brand_id!=''){
				$_SESSION['h_brand_id']=$h_brand_id;
			}
            $s_brand_id=$_SESSION['h_brand_id'];
        }
        if($_GET['b_id']!='') {
			$s_brand_id=$_GET['b_id'];
		}
		if($_GET['a_id']!='') {
			$s_area_id=$_GET['a_id'];
		}

		//All
		$sqlall = "select dsi.*,
					(select brand_name from brand dcdi where dcdi.brand_id=dsi.brand_id) brand_name,
					(select name from den_area da where da.id=dsi.area_id) area_name
					from shop dsi WHERE 1 and dsi.del_flg=0";

		if($s_brand_id!='') {
			$sqlall .= " and dsi.brand_id = $s_brand_id";
		}
		if($s_area_id!='') {
			$sqlall .= " and dsi.area_id = $s_area_id";
		}
        if($role=='3') {
            $sqlall .= " and dsi.login_id = '$login_user'";
        }

		$result = mysql_query($sqlall,$link) or die(mysql_error());

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}
		$rowCntall=mysql_num_rows($result);

		//Select current all
		$sql = sprintf("%s order by dsi.shop_id desc  limit %d,%d",$sqlall,($page-1)*$page_size,$page_size);

		$result = mysql_query($sql,$link);

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}

		$rowCnt=mysql_num_rows($result);

		//paging
		if($rowCnt==0){
			$page_count = 0;
			db_disConn($result, $link);
		}
		else{
			if( $rowCntall<$page_size ){ $page_count = 1; }
			if( $rowCntall%$page_size ){
				$page_count = (int)($rowCntall / $page_size) + 1;
			}else{
				$page_count = $rowCntall / $page_size;
			}
		}
		$page_string = '';
		if (($page == 1)||($page_count == 1)){
		   $page_string .= 'トップページ|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		else{
		   $page_string .= '<a href=?action=search&page=1&role='.$role.'&l_id='.$login_user.'&b_id='.$s_brand_id.'&a_id='.$s_area_id.'>トップページ</a>|<a href=?action=search&page='.($page-1).'&role='.$role.'&l_id='.$login_user.'&b_id='.$s_brand_id.'&a_id='.$s_area_id.'>前頁</a>|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		if( ($page == $page_count) || ($page_count == 0) ){
		   $page_string .= '次頁|最終ページ';
		}
		else{
		   $page_string .= '<a href=?action=search&page='.($page+1).'&role='.$role.'&l_id='.$login_user.'&b_id='.$s_brand_id.'&a_id='.$s_area_id.'>次頁</a>|<a href=?action=search&page='.$page_count.'&role='.$role.'&l_id='.$login_user.'&b_id='.$s_brand_id.'&a_id='.$s_area_id.'>最終ページ</a>';
		}
	}



?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $sub_title; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
</head>
<body>
<div class='main'>
<div class='subtitle'><?php echo $sub_title; ?></div>
<form action='?action=search' method='post' name='form1'>
<div class='input-area'>
    <?php
    if($role!='1'){
        $rs=mysql_fetch_object($result_list_brand);
		if($rs->brand_id!=''&&$role!=''&&$login_user!=''){
			echo"
				<input type='hidden' name='h_brand_id' value='".$rs->brand_id."'/>
				<input type='hidden' name='h_role' value='".$role."'/>
				<input type='hidden' name='h_login_user' value='".$login_user."'/>
			";
		}
    }
    ?>
	<label class='search_label w100'>提携先</label>
		<select name='s_brand_id' id='s_brand_id' class='re_select w200 fl' <?php echo $brand_is_disabled;?>>
				<?php
                if($role!='1'){
                    echo"<option value=".$rs->brand_id." selected >".$rs->brand_name. "</option>";
                }else{
                    echo "<option value=''></option>";
                    while($arr_list_row=mysql_fetch_array($result_list_brand)){
                        if($arr_list_row[brand_id]==$s_brand_id){
                            echo"<option value=".$arr_list_row[brand_id]." selected >".$arr_list_row[brand_name]. "</option>";
                        }else{
                            echo"<option value=".$arr_list_row[brand_id]." >".$arr_list_row[brand_name]. "</option>";
                        }
                    }
                }
        		?>
		</select>
	<div style='clear:both;'></div>
	<label class='search_label w100'>都道府県</label>
		<select name='s_area_id' id='s_area_id' class='re_select w100 fl' <?php echo $is_disabled;?>>
			<option value=''></option>
				<?php
		        while($arr_list_row=mysql_fetch_array($result_list_area)){
		        	if($arr_list_row[id]==$s_area_id){
		        		echo"<option value=".$arr_list_row[id]." selected >".$arr_list_row[name]. "</option>";
		        	}else{
		        		echo"<option value=".$arr_list_row[id]." >".$arr_list_row[name]. "</option>";
		        	}
		        }
        		?>
		</select>
	<input type='submit' class='buttonS bGreen ml100' value='絞り込み'/>
    <?php
    if($role=='1'||$role=='2'){
        echo"
            <input type='button' class='input-button buttonS bGreen ml20' value='新規登録' onclick='addInfo();'/>
        ";
    }
    ?>
</div>
<?php

if ($rowCnt>0){
	echo "
		<table width='98%' cellspacing='1' cellpadding='2'>
			<tr bgcolor='#DBE6F5'>
			  <td><span style='float:left; text-align:center;font-size:16px;'><font color=#666666>$page_string</font></span></td>
			</tr>
		</table>";
	echo "
		<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
			<tr bgcolor='#DBE6F5'>
              <th width='120px'>ログインID</th>
              <th width='180px'>提携先</th>
              <th width='240px'>店舗名</th>
			  <th width='100px'>都道府県</th>
              <th width='124px'>ステータス</th>
              <th width='124px'>ステータス</th>
              <th width='50px'>編集</th>
              <th width='50px'>削除</th>
			</tr>
		</table>
	";
	$i=1;
	while($rs=mysql_fetch_object($result))
	{
	  echo "
			<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
				<tr align='left' bgcolor='#EEF2F4'>
					<input type='hidden' name='u_id".$i."' value=".$rs->shop_id."/>
					<input type='hidden' name='u_status_old".$i."' value=".$rs->status."/>
					<td width='120px'align='center'>".$rs->login_id."</td>
					<td width='180px'>".htmlspecialchars_decode($rs->brand_name)."</td>
					<td width='240px'>".htmlspecialchars_decode($rs->shop_name)."</td>
					<td width='100px'align='center'>".$rs->area_name."</td>
		";
		if($rs->status=='0'){
			echo"
				<td width='124px'align='center'>非アクティブ</td>
				<td width='124px'align='center'>
					<input type='button' class='buttonS bGreen' value='アクティブ' onclick='changeToActive(".$rs->shop_id.");'/>
				</td>
				";
		}
		elseif($rs->status=='1'){
			echo"
				<td width='124px'align='center'>アクティブ</td>
				<td width='124px'align='center'>
					<input type='button' class='buttonS bRed' value='非アクティブ' onclick='changeToUnActive(".$rs->shop_id.");'/>
				</td>
				";
		}
		else{
			echo"
				<td width='124px'align='center'></td>
				<td width='124px'align='center'></td>
				";
		}
        if($role=='1'){
            echo"
                        <td width='50px'align='center'><a href='#' onclick=\"editInfo('".$rs->shop_id."')\">編集</a></td>
                        <td width='50px'align='center'><a href='#' onclick=\"deleteInfo('".$rs->shop_id."')\">削除</a></td>
                    </tr>
                </table>
		    ";
        }
        elseif($role=='2'){
            echo"
                        <td width='50px'align='center'><a href='#' onclick=\"editInfo('".$rs->shop_id."')\">編集</a></td>
                        <td width='50px'align='center'><a href='#' onclick=\"deleteInfo('".$rs->shop_id."')\">削除</a></td>
                    </tr>
                </table>
		    ";
        }
        elseif($role=='3'){
            echo"
                        <td width='50px'align='center'><a href='#' onclick=\"editInfo('".$rs->shop_id."')\">編集</a></td>
                        <td width='50px'align='center'>削除</td>
                    </tr>
                </table>
		    ";
        }
        else{
            echo"
                        <td width='50px'align='center'>編集</td>
                        <td width='50px'align='center'>削除</td>
                    </tr>
                </table>
		    ";
        }
		$i++;
	}
	echo "
		<table width='98%' cellspacing='1' cellpadding='2'>
			<tr bgcolor='#DBE6F5'>
			  <td><span style='float:left; text-align:center;font-size:16px;'><font color=#666666>$page_string</font></span></td>
			</tr>
		</table>";
	mysql_close($link);
}else{
	if ($action=='search'){
		echo "検索結果がありません。";
	}
}
?>
</form>
<script language="javascript" type="text/javascript">
	function addInfo() {
		var pageurl="m_shop_add.php";
		window.location.href=pageurl;
	}
    function deleteInfo(u_id) {
        var pageurl="m_shop_add.php?action=delete&u_id="+u_id;
        window.location.href=pageurl;
    }
	function editInfo(u_id) {
		var pageurl="m_shop_add.php?action=update&u_id="+u_id;
		window.location.href=pageurl;
	}
	function changeToActive(u_id) {
		var pageurl="?action=active&u_id="+u_id;
		window.location.href=pageurl;
	}
	function changeToUnActive(u_id) {
		var pageurl="?action=unactive&u_id="+u_id;
		window.location.href=pageurl;
	}
</script>
</div>
</body>
</html>