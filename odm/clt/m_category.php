<?php
	//include
	require '../util/include.php';

	$sub_title='カテゴリ管理　- 新着情報一覧 -';
	$systime=date('Y-m-d H:i:s',time());

	$action = $_GET['action'];

	//Delete
	if ($action=='delete'){
		$u_id = $_GET['u_id'];

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("delete from category WHERE cat_id = %d",$u_id);
		$result = mysql_query($sql,$db);

		mysql_close($db);

	}
	//Update
	if ($action=='update'){
		$u_id = $_GET['u_id'];
		$up_name = $_GET['name'];

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("UPDATE category SET cat_name ='%s',update_time=%d WHERE cat_id = %d",$up_name,strtotime($systime),$u_id);
		$result = mysql_query($sql,$db);

		mysql_close($db);

	}
	//Active
	if ($action=='active'){
		$u_id = $_GET['u_id'];

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("UPDATE category SET is_show =1,update_time=%d WHERE cat_id = %d",strtotime($systime),$u_id);
		$result = mysql_query($sql,$db);

		mysql_close($db);

	}
	//unActive
	if ($action=='unactive'){
		$u_id = $_GET['u_id'];

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("UPDATE category SET is_show =0,update_time=%d WHERE cat_id = %d",strtotime($systime),$u_id);
		$result = mysql_query($sql,$db);

		mysql_close($db);

	}
	//Search
	if ($action=='search'||$action=='active'||$action=='unactive'||$action=='update'||$action=='delete'){

		$link = db_conn();
		mysql_set_charset('utf8');

		$page_size=50;

		if( isset($_GET['page']) ){
		   $page = intval( $_GET['page'] );
		}
		else{
		   $page = 1;
		}
		$rowCnt = 0;

		//All
		$sqlall = "select * from category WHERE 1";

		$result = mysql_query($sqlall,$link) or die(mysql_error());

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}
		$rowCntall=mysql_num_rows($result);

		//Select current all
		$sql = sprintf("%s order by cat_id limit %d,%d",$sqlall,($page-1)*$page_size,$page_size);

		$result = mysql_query($sql,$link);

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}

		$rowCnt=mysql_num_rows($result);

		//paging
		if($rowCnt==0){
			$page_count = 0;
			db_disConn($result, $link);
		}
		else{
			if( $rowCntall<$page_size ){ $page_count = 1; }
			if( $rowCntall%$page_size ){
				$page_count = (int)($rowCntall / $page_size) + 1;
			}else{
				$page_count = $rowCntall / $page_size;
			}
		}
		$page_string = '';
		if (($page == 1)||($page_count == 1)){
		   $page_string .= 'トップページ|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		else{
		   $page_string .= '<a href=?action=search&page=1>トップページ</a>|<a href=?action=search&page='.($page-1).'>前頁</a>|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		if( ($page == $page_count) || ($page_count == 0) ){
		   $page_string .= '次頁|最終ページ';
		}
		else{
		   $page_string .= '<a href=?action=search&page='.($page+1).'>次頁</a>|<a href=?action=search&page='.$page_count.'>最終ページ</a>';
		}
	}

?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $sub_title; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
</head>
<body>
<div class='main'>
<div class='subtitle'><?php echo $sub_title; ?></div>
<form action='?action=search' method='post' name='form1'>
<div class='input-area'>
	<input type='button' class='input-button buttonS bGreen ml20' value='新規登録' onclick='addInfo();'/>
</div>
<?php
if ($rowCnt>0){
	echo "
		<table width='100%' cellspacing='1' cellpadding='2'>
			<tr bgcolor='#DBE6F5'>
			  <td><span style='float:left; text-align:center;font-size:16px;'><font color=#666666>$page_string</font></span></td>
			</tr>
		</table>";
	echo "
		<table width='100%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
			<tr bgcolor='#DBE6F5'>
              <th width='60px'>ID</th>
              <th width='320px'>カテゴリ名</th>
              <th width='124px'>ステータス</th>
              <th width='124px'>操作</th>
              <th width='100px'>名称更新</th>
              <th width='60px'>削除</th>
              <th width='170px'>登録日時</th>
              <th width='170px'>更新日時</th>
			</tr>
		</table>
	";
	$i=1;
	while($rs=mysql_fetch_object($result))
	{
	  echo "
		<table width='100%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
			<tr align='left' bgcolor='#EEF2F4'>
				<td width='60px'align='center'>".$rs->cat_id."</td>
				<td width='320px'>
					<input type='text' name='up_name".$i."' id='up_name".$i."'value=".htmlspecialchars_decode($rs->cat_name)." style='width:298px;font-size:15px;' />
				</td>
		";
		if($rs->is_show=='0'){
			echo"
				<td width='124px'align='center'>非アクティブ</td>
				<td width='124px'align='center'>
					<input type='button' class='buttonS bGreen' value='アクティブ' onclick='changeToActive(".$rs->cat_id.");'/>
				</td>
				";
		}
		elseif($rs->is_show=='1'){
			echo"
				<td width='124px'align='center'>アクティブ</td>
				<td width='124px'align='center'>
					<input type='button' class='buttonS bRed' value='非アクティブ' onclick='changeToUnActive(".$rs->cat_id.");'/>
				</td>
				";
		}
		else{
			echo"
				<td width='124px'align='center'></td>
				<td width='124px'align='center'></td>
				";
		}
		echo"
			<td width='100px'align='center'>
				<input type='button' class='buttonS bBlue' value='名称更新' onclick=\"updateChange("."'up_name".$i."',".$rs->cat_id.")\">
			</td>
			<td width='60px'align='center'>
				<input type='button' class='buttonS bBlue' value='削除' onclick=\"var ret=confirm('カテゴリ情報を削除します。よろしいですか？');if(ret)deleteInfo('".$rs->cat_id."');\">
			</td>
		";
		echo"
			<td width='170px'align='center'>".date("Y-m-d H:i:s",$rs->insert_time)."</td>
		";
		if($rs->update_time!='0'){
			echo"<td width='170px'align='center'>".date("Y-m-d H:i:s",$rs->update_time)."</td>";
		}
		else{
			echo"<td width='170px'align='center'></td>";
		}
		echo"
			</tr>
		</table>
		";
		$i++;
	}
	echo "
		<table width='100%' cellspacing='1' cellpadding='2'>
			<tr bgcolor='#DBE6F5'>
			  <td><span style='float:left; text-align:center;font-size:16px;'><font color=#666666>$page_string</font></span></td>
			</tr>
		</table>";
	mysql_close($link);
}else{
	if ($action=='search'){
		echo "検索結果がありません。";
	}
}
?>
</form>
<script language="javascript" type="text/javascript">
	function addInfo() {
		var pageurl="m_category_add.php";
		window.location.href=pageurl;
	}
	function editInfo(u_id) {
		var pageurl="m_category_add.php?action=update&u_id="+u_id;
		window.location.href=pageurl;
	}
	function changeToActive(u_id) {
		var pageurl="?action=active&u_id="+u_id;
		window.location.href=pageurl;
	}
	function changeToUnActive(u_id) {
		var pageurl="?action=unactive&u_id="+u_id;
		window.location.href=pageurl;
	}
	function deleteInfo(u_id) {
		var pageurl="?action=delete&u_id="+u_id;
		window.location.href=pageurl;
	}
	function updateChange(up_name,u_id) {
		var up_name=document.getElementById(up_name).value;
		var pageurl="?action=update&u_id="+u_id+"&name="+up_name;
		window.location.href=pageurl;
	}
</script>
</div>
</body>
</html>