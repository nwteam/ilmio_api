<?php
	//include
	require '../util/include.php';

	$sub_title='新着情報管理　- 新着情報一覧 -';

	$action = $_GET['action'];

	//Search
	if ($action=='search'){

		$link = db_conn();
		mysql_set_charset('utf8');

		$page_size=10;

		if( isset($_GET['page']) ){
		   $page = intval( $_GET['page'] );
		}
		else{
		   $page = 1;
		}
		$rowCnt = 0;

		$s_status=$_POST['s_status'];

		//All
		$sqlall = "select dni.*,
					(select name from den_credit_dest_info dcdi where dcdi.id=dni.supply_id) credit_name
					from den_new_info dni WHERE 1";

		if($s_status!='') {
			$sqlall .= " and dni.status = $s_status";
		}

		$result = mysql_query($sqlall,$link) or die(mysql_error());

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}
		$rowCntall=mysql_num_rows($result);

		//Select current all
		$sql = sprintf("%s order by dni.id desc  limit %d,%d",$sqlall,($page-1)*$page_size,$page_size);

		$result = mysql_query($sql,$link);

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}

		$rowCnt=mysql_num_rows($result);

		//paging
		if($rowCnt==0){
			$page_count = 0;
			db_disConn($result, $link);
		}
		else{
			if( $rowCntall<$page_size ){ $page_count = 1; }
			if( $rowCntall%$page_size ){
				$page_count = (int)($rowCntall / $page_size) + 1;
			}else{
				$page_count = $rowCntall / $page_size;
			}
		}
		$page_string = '';
		if (($page == 1)||($page_count == 1)){
		   $page_string .= 'トップページ|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		else{
		   $page_string .= '<a href=?action=search&page=1>トップページ</a>|<a href=?action=search&page='.($page-1).'>前頁</a>|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		if( ($page == $page_count) || ($page_count == 0) ){
		   $page_string .= '次頁|最終ページ';
		}
		else{
		   $page_string .= '<a href=?action=search&page='.($page+1).'>次頁</a>|<a href=?action=search&page='.$page_count.'>最終ページ</a>';
		}
	}



?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $sub_title; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
</head>
<body>
<div class='main'>
<div class='subtitle'><?php echo $sub_title; ?></div>
<form action='?action=search' method='post' name='form1'>
<div class='input-area'>
	<label class='search_label'>ステータス</label>
	<select name='s_status' id='s_status' class='re_select'>
		<option value=''>&nbsp;</option>
		<option value='0'>承認待</option>
		<option value='1'>公開中</option>
		<option value='2'>非公開</option>
	</select>
	<input type='submit' class='buttonS bGreen ml20' value='絞り込み'/>
	<input type='button' class='input-button buttonS bGreen ml20' value='新規登録' onclick='addNews();'/>
	<input type='button' class='input-button buttonH bGreen ml20' value='保　存'/>
</div>
<?php
if ($rowCnt>0){
	echo "
		<table width='100%' cellspacing='1' cellpadding='2'>
			<tr bgcolor='#DBE6F5'>
			  <td><span style='float:left; text-align:center'><font color=#666666>$page_string</font></span></td>
			</tr>
		</table>";
	echo "
		<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
			<tr bgcolor='#DBE6F5'>
              <th width='80px'>日付</th>
              <th width='240px'>タイトル</th>
              <th width='180px'>配信元</th>
              <th width='124px'>ステータス</th>
              <th width='90px'>配信日時</th>
              <th width='50px'>編集</th>
              <th width='50px'>削除</th>
			</tr>
		</table>
	";
	$i=1;
	while($rs=mysql_fetch_object($result))
	{
	  echo "
			<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
				<tr align='left' bgcolor='#EEF2F4'>
					<input type='hidden' name='u_id".$i."' value=".$rs->id."/>
					<input type='hidden' name='u_status_old".$i."' value=".$rs->status."/>
					<td width='80px'align='center'>".date("Y-m-d",$rs->online_date)."</td>
					<td width='240px'>".$rs->title."</td>
					<td width='180px'>".$rs->credit_name."</td>
		";
		if($rs->status=='0'){
			echo"
				<td width='124px'align='center'>
					<select name='u_status".$i."' id='u_status".$i."' class='re_select'>
						<option value='0'selected='selected'>承認待</option>
						<option value='1'>公開中</option>
						<option value='2'>非公開</option>
					</select>
				</td>
				";
		}
		elseif($rs->status=='1'){
			echo"
				<td width='124px'align='center'>
					<select name='u_status".$i."' id='u_status".$i."' class='re_select'>
						<option value='0'>承認待</option>
						<option value='1'selected='selected'>公開中</option>
						<option value='2'>非公開</option>
					</select>
				</td>
				";
		}
		elseif($rs->status=='2'){
			echo"
				<td width='124px'align='center'>
					<select name='u_status".$i."' id='u_status".$i."' class='re_select'>
						<option value='0'>承認待</option>
						<option value='1'>公開中</option>
						<option value='2'selected='selected'>非公開</option>
					</select>
				</td>
				";
		}
		else{
			echo"
				<td width='124px'align='center'>
					<select name='u_status".$i."' id='u_status".$i."' class='re_select'>
						<option value='0'selected='selected'>承認待</option>
						<option value='1'>公開中</option>
						<option value='2'>非公開</option>
					</select>
				</td>
				";
		}
		echo"
					<td width='90px'align='center'>".date("Y-m-d H:i:s",$rs->supply_time_from)."</td>
					<td width='50px'align='center'>編集</td>
					<td width='50px'align='center'>削除</td>
				</tr>
			</table>
		";
		$i++;
	}
	echo "
		<table width='100%' cellspacing='1' cellpadding='2'>
			<tr bgcolor='#DBE6F5'>
			  <td><span style='float:left; text-align:center'><font color=#666666>$page_string</font></span></td>
			</tr>
		</table>";
	mysql_close($link);
}else{
	if ($action=='search'){
		echo "検索結果がありません。";
	}
}
?>
</form>
<script language="javascript" type="text/javascript">
	function addNews() {
		var pageurl="m_news_add.php";
		window.location.href=pageurl;
	}
	function updateNews(u_id) {
		var pageurl="m_news_add.php?action=update&u_id="+u_id;
		window.location.href=pageurl;
	}
	function updateChange(u_id) {
		var pageurl="?action=update";
		window.location.href=pageurl;
	}
</script>
</div>
</body>
</html>