<?php
	//include
	require '../util/include.php';

	$sub_title='紹介元属性管理　- 会員表示順編集 -';

	$role=$_SESSION['role'];
	$login_user=$_SESSION['login_user'];

	$action = $_GET['action'];
	$s_rank_id = $_GET['u_id'];
	$s_id = $_GET['id'];

	//UpdateChecked
	if ($action=='updateChecked'){

		$a_brand_id = $_POST['a_brand_id'];
		$s_rank_id=$_POST['s_rank_id'];
		$a_sort_code = $_POST['a_sort_code'];
		$a_insert_flg = $_POST['a_insert_flg'];

		$i=1;

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');
		
		foreach($a_brand_id as $v){
			if($v!=''){
				if ($a_sort_code[$i]!=''&&$s_rank_id!=''){
					if($a_insert_flg[$i]=='1'){
						$sql = sprintf("insert into den_rank_sort (brand_id,rank_id,sort_code) values (%d,'%s',%d);",$v,$s_rank_id,$a_sort_code[$i]);
						$logstr = "$systime $ip INFO：会員表示順情報登録 INSERT SQL文： ".$sql."\r\n";
						error_log($logstr,3,'../log/gen_sort_info.log');
						$result = mysql_query($sql,$db);
						
					}else{
						$sql = sprintf("UPDATE den_rank_sort SET sort_code=%d WHERE brand_id = %d and rank_id='%s';",$a_sort_code[$i],$v,$s_rank_id);
						$logstr = "$systime $ip INFO：会員表示順情報一括更新 UPDATE SQL文： ".$sql."\r\n";
						error_log($logstr,3,'../log/gen_sort_info.log');
						$result = mysql_query($sql,$db);
					}
				}
			}
			$i++;
		}
		mysql_close($db);
	}

	//Search
	if ($action=='search'||$action=='updateChecked'){

		$link = db_conn();
		mysql_set_charset('utf8');

		$page_size=100;

		if( isset($_GET['page']) ){
		   $page = intval( $_GET['page'] );
		}
		else{
		   $page = 1;
		}
		$rowCnt = 0;

		$s_brand_name=$_POST['s_brand_name'];
		
		if($s_rank_id=='') {
			$s_rank_id=$_POST['s_rank_id'];
		}
		if($s_id=='') {
			$s_id=$_POST['s_id'];
		}

		if($_GET['b_name']!='') {
			$s_brand_name=$_GET['b_name'];
		}

		//All
		$sqlall = "select bd.*,
					(select sort_code from den_rank_sort drs where drs.brand_id=bd.brand_id and drs.rank_id='".$s_rank_id."') sort_code
		            from brand bd WHERE 1 and bd.del_flg=0";
		$sqlall .= " and find_in_set($s_id,bd.property_id)";

        if($s_brand_name!='') {
            $sqlall .= " and bd.brand_name like '%".mysql_real_escape_string($s_brand_name)."%'";
        }

		$result = mysql_query($sqlall,$link) or die(mysql_error());

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}
		$rowCntall=mysql_num_rows($result);
		
		if($rowCntall>$page_size){
			$all_row=$page_size;
		}else{
			$all_row=$rowCntall;
		}

		//Select current all
		$sql = sprintf("%s order by sort_code limit %d,%d",$sqlall,($page-1)*$page_size,$page_size);

		$result = mysql_query($sql,$link);

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}

		$rowCnt=mysql_num_rows($result);

		//paging
		if($rowCnt==0){
			$page_count = 0;
			db_disConn($result, $link);
		}
		else{
			if( $rowCntall<$page_size ){ $page_count = 1; }
			if( $rowCntall%$page_size ){
				$page_count = (int)($rowCntall / $page_size) + 1;
			}else{
				$page_count = $rowCntall / $page_size;
			}
		}
		$page_string = '';
		if (($page == 1)||($page_count == 1)){
		   $page_string .= 'トップページ|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		else{
		   $page_string .= '<a href=?action=search&page=1&b_name='.$s_brand_name.'>トップページ</a>|<a href=?action=search&page='.($page-1).'&b_name='.$s_brand_name.'>前頁</a>|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		if( ($page == $page_count) || ($page_count == 0) ){
		   $page_string .= '次頁|最終ページ';
		}
		else{
		   $page_string .= '<a href=?action=search&page='.($page+1).'&b_name='.$s_brand_name.'>次頁</a>|<a href=?action=search&page='.$page_count.'&b_name='.$s_brand_name.'>最終ページ</a>';
		}
	}



?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $sub_title; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../js/common.js"></script>
</head>
<body>
<div class='main'>
<div class='subtitle'><?php echo $sub_title; ?></div>
<form action='?action=search' method='post' name='form1'>
<div class='input-area'>
    <label class='search_label w100'>会社名称</label>
	<input type='text' name='s_brand_name' id='s_brand_name' class='w500' value='<?php echo $s_brand_name;?>'/>
	<input type='hidden' name='s_rank_id' id='s_rank_id' class='w500' value='<?php echo $s_rank_id;?>'/>
	<input type='hidden' name='s_id' id='s_id' class='w500' value='<?php echo $s_id;?>'/>
	<input type='submit' class='buttonS bGreen ml100' value='絞り込み'/>
	<input type='button' class='buttonS bGreen ml20' value='一括更新' onclick='updateCheckedSubmit(<?php echo $all_row;?>);'/>
	<input type='button' class='buttonS bGreen ml20 w100' value='戻る' onclick='back();'/>
</div>
<?php
if ($rowCnt>0){
	echo "
		<table width='98%' cellspacing='1' cellpadding='2'>
			<tr bgcolor='#DBE6F5'>
			  <td><span style='float:left; text-align:center;font-size:16px;'><font color=#666666>$page_string</font></span></td>
			</tr>
		</table>";
	echo "
		<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
			<tr bgcolor='#DBE6F5'>
              <td width='15px'><input type='checkbox' name='update_all' id='update_all' onclick=\"checkAll(".$all_row.")\"></input></td>
              <th width='120px'>会員種別コード</th>
              <th width='120px'>ログインID</th>
              <th width='240px'>会社名称</th>
              <th width='120px'>表示順</th>
			</tr>
		</table>
	";
	$i=1;
	while($rs=mysql_fetch_object($result))
	{
	  echo "
			<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
				<tr align='left' bgcolor='#EEF2F4'>
					<td width='15px'><input type='checkbox' name='update_flg_".$i."' id='update_flg_".$i."'></input></td>
					<td width='120px'align='center'>".$s_rank_id."</td>
					<td width='120px'align='center'>".$rs->login_id."</td>
					<td width='240px'>".$rs->brand_name."</td>
					<td width='120px'align='center'>
						<input type='text' name='sort_code".$i."' id='sort_code".$i."' value='".$rs->sort_code."' style='width:95%;font-size:15px;' onchange=\"checkIt(".$i.");\"/>
					</td>
					<input type='hidden' name='brand_id".$i."' id='brand_id".$i."' value='".$rs->brand_id."'>
					<input type='hidden' name='a_brand_id[".$i."]' id='a_brand_id[".$i."]' value=''>
					<input type='hidden' name='a_rank_id[".$i."]' id='a_rank_id[".$i."]' value=''>
					<input type='hidden' name='a_sort_code[".$i."]' id='a_sort_code[".$i."]' value=''>
					<input type='hidden' name='a_insert_flg[".$i."]' id='a_insert_flg[".$i."]' value=''>
		";
		if($rs->sort_code==''){
			echo"<input type='hidden' name='insert_flg".$i."' id='insert_flg".$i."' value='1'>";
		}else{
			echo"<input type='hidden' name='insert_flg".$i."' id='insert_flg".$i."' value='0'>";
		}
		$i++;
	}
	echo "
		<table width='98%' cellspacing='1' cellpadding='2'>
			<tr bgcolor='#DBE6F5'>
			  <td><span style='float:left; text-align:center;font-size:16px;'><font color=#666666>$page_string</font></span></td>
			</tr>
		</table>";
	mysql_close($link);
}else{
	if ($action=='search'){
		echo "検索結果がありません。";
	}
}
?>
</form>
<script language="javascript" type="text/javascript">
    function updateCheckedSubmit(rowcnt) {
        	var a_brand_id=new Array();
        	var a_sort_code=new Array();
        	var a_insert_flg=new Array();
        	
        for(var i=1;i<=rowcnt;i++){
        		var update_flg=document.getElementById("update_flg_"+i).checked;
        		if(update_flg==true){
        			a_brand_id[i]=document.getElementById('brand_id'+i).value;
        			a_sort_code[i]=document.getElementById('sort_code'+i).value;
        			a_insert_flg[i]=document.getElementById('insert_flg'+i).value;
        			document.getElementById('a_brand_id['+i+']').value=a_brand_id[i];
        			document.getElementById('a_sort_code['+i+']').value=a_sort_code[i];
        			document.getElementById('a_insert_flg['+i+']').value=a_insert_flg[i];
        		}
        		else{
        			a_brand_id[i]='';
        			a_sort_code[i]='';
        			a_insert_flg[i]='';
        			document.getElementById('a_brand_id['+i+']').value=a_brand_id[i];
        			document.getElementById('a_sort_code['+i+']').value=a_sort_code[i];
        			document.getElementById('a_insert_flg['+i+']').value=a_insert_flg[i];
        		}
        }
        document.form1.action="?action=updateChecked";
        document.form1.submit();
    }
	function checkAll(rowcnt) {
		if (document.getElementById('update_all').checked==true){
			for(var i=1;i<=rowcnt;i++){
		 		document.getElementById('update_flg_'+i).checked=true;
			}			
		}else{
			for(var i=1;i<=rowcnt;i++){
		 		document.getElementById('update_flg_'+i).checked=false;
			}			
		}
	}
	function checkIt(id) {
		document.getElementById('update_flg_'+id).checked='checked';
	}
	function back() {
		var pageurl="m_property.php?action=search";
		window.location.href=pageurl;
	}
</script>
</div>
</body>
</html>