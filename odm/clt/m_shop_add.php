<?php
session_start();
	//include
	require '../util/include.php';
	include('../util/phpqrcode/phpqrcode.php');

    $role=$_SESSION['role'];
    $login_user=$_SESSION['login_user'];

	$action = $_GET['action'];
	$sysdate=date('Y-m-d',time());
	$systime=date('Y-m-d H:i:s',time());
	$ip=get_real_ip();

	$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	if(!$db){
		die("connot connect:" . mysql_error());
	}
	$dns = mysql_select_db(DB_NAME,$db);
	if(!$dns){
		die("connot use db:" . mysql_error());
	}
	mysql_set_charset('utf8');

    //カテゴリプルダウンリスト取得
	$sqlall = "select * from category WHERE 1 and is_show=1";
	$result_list_cate = mysql_query($sqlall,$db);
    //提携先プルダウンリスト取得
    if($role=='2'){
        $sqlall = "select * from brand WHERE 1 and del_flg=0 and login_id='".$login_user."'  order by brand_id,sort_order";
        $result_list_brand = mysql_query($sqlall,$db);
        $brand_is_disabled="disabled='disabled'";
    }
    elseif($role=='3'){
        $sqlall = "select bd.* from brand bd,shop sp WHERE 1 and bd.brand_id=sp.brand_id
                    and bd.del_flg=0 and sp.login_id='".$login_user."'  order by bd.brand_id,bd.sort_order";
        $result_list_brand = mysql_query($sqlall,$db);
        $brand_is_disabled="disabled='disabled'";
    }
    else{
        $sqlall = "select * from brand WHERE 1 and del_flg=0 order by brand_id,sort_order";
        $result_list_brand = mysql_query($sqlall,$db);
    }
    //都道府県プルダウンリスト取得
    $sqlall = "select * from den_area WHERE 1";
    $result_list_area = mysql_query($sqlall,$db);

	mysql_close($db);

	if ($action=='confirm'){
		$sub_title='店舗情報管理　- 新規追加確認 -';
		$is_disabled="disabled='disabled'";
		$havePlaceholder='NO';

		//form項目
        //hidden項目
        $u_id = $_POST['u_id'];
        $update_flg=$_POST['update_flg'];


        //ログインID
        if($update_flg=='1'){
            $i_login_id=$_POST['h_login_id'];
        }else{
            $i_login_id=$_POST['i_login_id'];
        }
        //パスワード
        $i_password=$_POST['i_pwd'];
        //ステータス
        $i_status = $_POST['i_status'];
        //有効期間From
        $i_expired_date_from= $_POST['i_expired_date_from'];
        if($i_expired_date_from==''){
            $i_expired_date_from=$sysdate;
        }
        //有効期間To
        $i_expired_date_to= $_POST['i_expired_date_to'];
        if($i_expired_date_to==''){
            $i_expired_date_to='2030-12-31';
        }
        //提携先
        $i_brand_id=$_POST['i_brand_id'];
        //店舗名
        $i_shop_name = $_POST['i_shop_name'];
		$i_shop_name = addslashes($i_shop_name);
        //都道府県
        $i_area_id = $_POST['i_area_id'];
        //住所
        $i_address = $_POST['i_address'];
		$i_address = addslashes($i_address);
        //緯度
        $i_latitude = $_POST['i_latitude'];
        //経度
        $i_longitude = $_POST['i_longitude'];
        //電話番号
        $i_tel = $_POST['i_tel'];
        //営業時間From
        $i_open_time = $_POST['i_open_time'];
        //定休日
        $i_rest_day = $_POST['i_rest_day'];
		//barcode
		$i_barcode = $_POST['i_barcode'];
	}
	elseif($action=='edit'){
		$sub_title='店舗情報管理　- 新規追加訂正 -';
		$is_disabled="";
		$havePlaceholder='YES';
		//form項目
        //form項目
        //hidden項目
        $u_id = $_POST['u_id'];
        $update_flg=$_POST['update_flg'];


        //ログインID
        if($update_flg=='1'){
            $i_login_id=$_POST['h_login_id'];
        }else{
            $i_login_id=$_POST['i_login_id'];
        }
        //パスワード
        $i_password=$_POST['i_pwd'];
        //ステータス
        $i_status = $_POST['i_status'];
        //有効期間From
        $i_expired_date_from= $_POST['i_expired_date_from'];
        if($i_expired_date_from==''){
            $i_expired_date_from=$sysdate;
        }
        //有効期間To
        $i_expired_date_to= $_POST['i_expired_date_to'];
        if($i_expired_date_to==''){
            $i_expired_date_to='2030-12-31';
        }
        //提携先
        $i_brand_id=$_POST['i_brand_id'];
        //店舗名
        $i_shop_name = $_POST['i_shop_name'];
		$i_shop_name = addslashes($i_shop_name);
        //都道府県
        $i_area_id = $_POST['i_area_id'];
        //住所
        $i_address = $_POST['i_address'];
		$i_address = addslashes($i_address);
        //緯度
        $i_latitude = $_POST['i_latitude'];
        //経度
        $i_longitude = $_POST['i_longitude'];
        //電話番号
        $i_tel = $_POST['i_tel'];
        //営業時間From
        $i_open_time = $_POST['i_open_time'];

        //定休日
        $i_rest_day = $_POST['i_rest_day'];
		//barcode
		$i_barcode = $_POST['i_barcode'];
	}
    elseif ($action=='update'||$action=='delete'){
        if($action=='update'){
            $sub_title='店舗情報管理　- 編集 -';
            $is_disabled="";
            $havePlaceholder='NO';
        }
        if($action=='delete'){
            $sub_title='店舗情報管理　- 削除 -';
            $is_disabled="disabled='disabled'";
            $havePlaceholder='NO';
        }

        $u_id = $_GET['u_id'];

        $update_flg='1';

        $logstr = "$systime $ip INFO：▼店舗情報取得開始：shop_id = $u_id \r\n";
        error_log($logstr,3,'../log/gen.log');

        if($u_id==''){

            db_disConn($result, $link);
            $logstr = "$systime ERR：shop_id取得エラー！ \r\n";
            $logstr .= "$systime $ip INFO：▲店舗情報取得異常終了 \r\n";
            error_log($logstr,3,'../log/gen.log');

            $err_cd_list[]="02";
            $_SESSION['err_cd_list']=$err_cd_list;
            $url= URL_PATH . "err.php";
            redirect($url);
        }

        //情報取得
        $db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        if(!$db){
            die("connot connect:" . mysql_error());
        }
        $dns = mysql_select_db(DB_NAME,$db);
        if(!$dns){
            die("connot use db:" . mysql_error());
        }
        mysql_set_charset('utf8');
        $sqlall = "select * from shop WHERE 1";
        $sqlall .= " and shop_id = $u_id";

        $result = mysql_query($sqlall,$db);
        $rs=mysql_fetch_object($result);

        //ログインID
        $i_login_id=$rs->login_id;
        //パスワード
        $i_password=$rs->password;
        //ステータス
        $i_status =$rs->status;
        //有効期間From
        $i_expired_date_from=$rs->expired_date_from;
        $i_expired_date_from=date('Y-m-d',$i_expired_date_from);
        //有効期間To
        $i_expired_date_to=$rs->expired_date_to;
        $i_expired_date_to=date('Y-m-d',$i_expired_date_to);
        //提携先
        $i_brand_id=$rs->brand_id;
        //店舗名
        $i_shop_name =$rs->shop_name;
        //都道府県
        $i_area_id =$rs->area_id;
        //住所
        $i_address =$rs->address;
		$i_address = addslashes($i_address);
        //緯度
        $i_latitude =$rs->latitude;
        //経度
        $i_longitude =$rs->longitude;
        //電話番号
        $i_tel =$rs->tel;
        //営業時間From
        $i_open_time =$rs->open_time;

        //定休日
        $i_rest_day =$rs->rest_day;
		//barcode
		$i_barcode = $rs->barcode;

        mysql_close($db);

        $logstr = "$systime $ip INFO：▲店舗情報取得正常終了：shop_id = $u_id \r\n";
        error_log($logstr,3,'../log/gen.log');

    }
	else{
		$sub_title='店舗情報管理　- 新規追加 -';
		$is_disabled="";
		$havePlaceholder='YES';
		//form項目
        //ログインID
        if($role=='3') {
            $i_login_id= $login_user;
        }
        else{
            $i_login_id= '';
        }
        //パスワード
        $i_password='';
        //ステータス
        $i_status ='';
        //有効期間From
        $i_expired_date_from='';
        //有効期間To
        $i_expired_date_to='';
        //提携先
        $i_brand_id='';
        //店舗名
        $i_shop_name ='';
        //都道府県
        $i_area_id ='';
        //住所
        $i_address ='';
        //緯度
        $i_latitude ='';
        //経度
        $i_longitude ='';
        //電話番号
        $i_tel ='';
        //営業時間From
        $i_open_time ='';

        //定休日
        $i_rest_day ='';
		//barcode
		$i_barcode = '';
	}

	//insert
	if ($action=='insert'){
		$logstr = "$systime $ip INFO：▼店舗情報登録開始 \r\n";
		error_log($logstr,3,'../log/gen.log');

        //ログインID
        $i_login_id=$_POST['i_login_id'];
        //パスワード
        $i_password=$_POST['i_pwd'];
        //ステータス
        $i_status = $_POST['i_status'];
        //有効期間From
        $i_expired_date_from= $_POST['i_expired_date_from'];
        if($i_expired_date_from==''){
            $i_expired_date_from=$sysdate;
        }
        //有効期間To
        $i_expired_date_to= $_POST['i_expired_date_to'];
        if($i_expired_date_to==''){
            $i_expired_date_to='2030-12-31';
        }
        //提携先
        $i_brand_id=$_POST['i_brand_id'];
        //店舗名
        $i_shop_name = $_POST['i_shop_name'];
		$i_shop_name = addslashes($i_shop_name);
        //都道府県
        $i_area_id = $_POST['i_area_id'];
        //住所
        $i_address = $_POST['i_address'];
		$i_address = addslashes($i_address);
        //緯度
        $i_latitude = $_POST['i_latitude'];
        //経度
        $i_longitude = $_POST['i_longitude'];
        //電話番号
        $i_tel = $_POST['i_tel'];
        //営業時間From
        $i_open_time = $_POST['i_open_time'];

        //定休日
        $i_rest_day = $_POST['i_rest_day'];
		//barcode
		$i_barcode = $_POST['i_barcode'];

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

        $logstr = "$systime $ip INFO：▼店舗名存在チェック開始 \r\n";
        error_log($logstr,3,'../log/gen.log');
        //店舗名存在チェック
        $ret=searchShopInfo($i_brand_id,$i_shop_name);
        if($ret=='1'){

            db_disConn($result, $link);
            $logstr = "$systime ERR：店舗名存在チェックエラー！ \r\n";
            $logstr .= "$systime $ip INFO：▲店舗名存在チェック異常終了 \r\n";
            error_log($logstr,3,'../log/gen.log');

            $err_cd_list[]="02";
            $_SESSION['err_cd_list']=$err_cd_list;
            $url= URL_PATH . "err.php";
            redirect($url);
        }

        $logstr = "$systime $ip INFO：▲店舗名存在チェック正常終了！！ \r\n";
        error_log($logstr,3,'../log/gen.log');
        if($role!='3'){
            $logstr = "$systime $ip INFO：▼店舗管理者情報登録開始 \r\n";
            error_log($logstr,3,'../log/gen.log');

            //管理者情報存在チェック
            $ret=searchAdminInfo($i_login_id);
            if($ret=='1'){

                db_disConn($result, $link);
                $logstr = "$systime ERR：店舗管理者情報DB登録存在チェックエラー！ \r\n";
                $logstr .= "$systime $ip INFO：▲店舗管理者情報登録異常終了 \r\n";
                error_log($logstr,3,'../log/gen.log');

                $err_cd_list[]="02";
                $_SESSION['err_cd_list']=$err_cd_list;
                $url= URL_PATH . "err.php";
                redirect($url);
            }

            $sql = sprintf("insert into den_admin (u_id,pwd,role,insert_time) values ('%s','%s',%d,%d)",
                $i_login_id,$i_password,3,strtotime($systime)
            );

            $logstr = "$systime $ip INFO：店舗管理者情報登録 INSERT SQL文： ".$sql."\r\n";
            error_log($logstr,3,'../log/gen.log');

            $result = mysql_query($sql,$db);
            if(!$result){

                db_disConn($result, $link);
                $logstr = "$systime ERR：店舗管理者情報DB登録異常！ \r\n";
                $logstr .= "$systime $ip INFO：▲店舗管理者情報登録異常終了 \r\n";
                error_log($logstr,3,'../log/gen.log');

                $err_cd_list[]="01";
                $_SESSION['err_cd_list']=$err_cd_list;
                $url= URL_PATH . "err.php";
                redirect($url);
            }
            $logstr = "$systime $ip INFO：▲店舗管理者情報登録正常終了！！ \r\n";
            error_log($logstr,3,'../log/gen.log');
        }
        $logstr = "$systime $ip INFO：▼店舗情報登録開始 \r\n";
        error_log($logstr,3,'../log/gen.log');
        $sql = sprintf("
                        insert into shop (
                            brand_id,
                            shop_name,
                            status,
                            expired_date_from,
                            expired_date_to,
                            login_id,
                            password,
                            area_id,
                            address,
                            latitude,
                            longitude,
                            tel,
                            open_time,
                            rest_day,
                            barcode,
                            insert_time
                            ) values
                            (%d,'%s',%d,%d,%d,'%s','%s',%d,'%s','%s','%s','%s','%s','%s','%s',%d)",
            $i_brand_id,
            $i_shop_name,
            $i_status,
            strtotime($i_expired_date_from),
            strtotime($i_expired_date_to),
            $i_login_id,
            $i_password,
            $i_area_id,
            $i_address,
            $i_latitude,
            $i_longitude,
            $i_tel,
            $i_open_time,
            $i_rest_day,
			$i_barcode,
            strtotime($systime)
        );
        $logstr = "$systime $ip INFO：店舗情報登録 INSERT SQL文： ".$sql."\r\n";
        error_log($logstr,3,'../log/gen.log');

        $result = mysql_query($sql,$db);
        if(!$result){
            db_disConn($result, $link);
            $logstr = "$systime ERR：店舗情報DB登録異常！ \r\n";
            $logstr .= "$systime $ip INFO：▲店舗情報登録異常終了 \r\n";
            error_log($logstr,3,'../log/gen.log');

            $err_cd_list[]="01";
            $_SESSION['err_cd_list']=$err_cd_list;
            $url= URL_PATH . "err.php";
            redirect($url);
        }

        mysql_close($db);
        $logstr = "$systime $ip INFO：▲店舗情報登録正常終了！！ \r\n";
        error_log($logstr,3,'../log/gen.log');


		if($role!='1'){
			$url= URL_PATH . "m_shop.php?action=search&b_id=".$i_brand_id."&role=".$role."&l_id=".$login_user;
		}else{
			$url= URL_PATH . "m_shop.php?action=search&role=".$role."&l_id=".$login_user;
		}
        redirect($url);
	}
    //更新実行
    if ($action=='updatesubmit'){

        $u_id=$_POST['u_id'];

        //ログインID
        $i_login_id=$_POST['h_login_id'];
        //パスワード
        $i_password=$_POST['i_pwd'];
        //ステータス
        $i_status = $_POST['i_status'];
        //有効期間From
        $i_expired_date_from= $_POST['i_expired_date_from'];
        if($i_expired_date_from==''){
            $i_expired_date_from=$sysdate;
        }
        //有効期間To
        $i_expired_date_to= $_POST['i_expired_date_to'];
        if($i_expired_date_to==''){
            $i_expired_date_to='2030-12-31';
        }
        //提携先
        $i_brand_id=$_POST['i_brand_id'];
        //店舗名
        $i_shop_name = $_POST['i_shop_name'];
		$i_shop_name = addslashes($i_shop_name);
        //都道府県
        $i_area_id = $_POST['i_area_id'];
        //住所
        $i_address = $_POST['i_address'];
		$i_address = addslashes($i_address);
        //緯度
        $i_latitude = $_POST['i_latitude'];
        //経度
        $i_longitude = $_POST['i_longitude'];
        //電話番号
        $i_tel = $_POST['i_tel'];
        //営業時間From
        $i_open_time = $_POST['i_open_time'];
        //定休日
        $i_rest_day = $_POST['i_rest_day'];
		//barcode
		$i_barcode = $_POST['i_barcode'];

        $db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        if(!$db){
            die("connot connect:" . mysql_error());
        }

        $dns = mysql_select_db(DB_NAME,$db);

        if(!$dns){
            die("connot use db:" . mysql_error());
        }

        mysql_set_charset('utf8');


        $logstr = "$systime $ip INFO：▼店舗名存在チェック開始 \r\n";
        error_log($logstr,3,'../log/gen.log');
        //店舗名存在チェック
        $ret=searchShopInfoForUpdate($i_brand_id,$i_shop_name,$u_id);
        if($ret=='1'){

            db_disConn($result, $link);
            $logstr = "$systime ERR：店舗名存在チェックエラー！ \r\n";
            $logstr .= "$systime $ip INFO：▲店舗名存在チェック異常終了 \r\n";
            error_log($logstr,3,'../log/gen.log');

            $err_cd_list[]="02";
            $_SESSION['err_cd_list']=$err_cd_list;
            $url= URL_PATH . "err.php";
            redirect($url);
        }
        $logstr = "$systime $ip INFO：▲店舗名存在チェック正常終了！！ \r\n";
        error_log($logstr,3,'../log/gen.log');

        $logstr = "$systime $ip INFO：▼店舗管理者情報更新開始 \r\n";
        error_log($logstr,3,'../log/gen.log');

        $sql = sprintf("update den_admin set pwd='%s' where u_id='%s'",$i_password,$i_login_id);

        $logstr = "$systime $ip INFO：店舗管理者情報更新 UPDATE SQL文： ".$sql."\r\n";
        error_log($logstr,3,'../log/gen.log');

        $result = mysql_query($sql,$db);
        if(!$result){

            db_disConn($result, $link);
            $logstr = "$systime ERR：店舗管理者情報DB更新異常！ \r\n";
            $logstr .= "$systime $ip INFO：▲店舗管理者情報更新異常終了 \r\n";
            error_log($logstr,3,'../log/gen.log');

            $err_cd_list[]="01";
            $_SESSION['err_cd_list']=$err_cd_list;
            $url= URL_PATH . "err.php";
            redirect($url);
        }
        $logstr = "$systime $ip INFO：▲店舗管理者情報更新正常終了！！ \r\n";
        error_log($logstr,3,'../log/gen.log');

        $logstr = "$systime $ip INFO：▼店舗情報更新開始 \r\n";
        error_log($logstr,3,'../log/gen.log');
        $sql = sprintf("update shop set
                                brand_id=%d,
                                shop_name='%s',
                                status=%d,
                                expired_date_from=%d,
                                expired_date_to=%d,
                                password='%s',
                                area_id=%d,
                                address='%s',
                                latitude='%s',
                                longitude='%s',
                                tel='%s',
                                open_time='%s',
                                rest_day='%s',
                                barcode='%s',
                                update_time=%d
                                where shop_id=%d",
            $i_brand_id,
            $i_shop_name,
            $i_status,
            strtotime($i_expired_date_from),
            strtotime($i_expired_date_to),
            $i_password,
            $i_area_id,
            $i_address,
            $i_latitude,
            $i_longitude,
            $i_tel,
            $i_open_time,
            $i_rest_day,
			$i_barcode,
            strtotime($systime),
            $u_id
        );
        $logstr = "$systime $ip INFO：店舗情報更新 UPDATE SQL文： ".$sql."\r\n";
        error_log($logstr,3,'../log/gen.log');

        $result = mysql_query($sql,$db);
        if(!$result){
            db_disConn($result, $link);
            $logstr = "$systime ERR：店舗情報DB更新異常！ \r\n";
            $logstr .= "$systime $ip INFO：▲店舗情報更新異常終了 \r\n";
            error_log($logstr,3,'../log/gen.log');

            $err_cd_list[]="01";
            $_SESSION['err_cd_list']=$err_cd_list;
            $url= URL_PATH . "err.php";
            redirect($url);
        }

        mysql_close($db);
        $logstr = "$systime $ip INFO：▲店舗情報更新正常終了！！ \r\n";
        error_log($logstr,3,'../log/gen.log');

		if($role!='1'){
			$url= URL_PATH . "m_shop.php?action=search&b_id=".$i_brand_id."&role=".$role."&l_id=".$login_user;
		}else{
			$url= URL_PATH . "m_shop.php?action=search&role=".$role."&l_id=".$login_user;
		}
        redirect($url);
    }

    //削除実行
    if ($action=='deletesubmit'){

        $i_login_id=$_POST['h_login_id'];
        $u_id=$_POST['u_id'];

        $db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        if(!$db){
            die("connot connect:" . mysql_error());
        }

        $dns = mysql_select_db(DB_NAME,$db);

        if(!$dns){
            die("connot use db:" . mysql_error());
        }

        mysql_set_charset('utf8');


        $logstr = "$systime $ip INFO：▼店舗管理者情報削除更新開始 \r\n";
        error_log($logstr,3,'../log/gen.log');

        $sql = sprintf("update den_admin set status=1 where u_id='%s'",$i_login_id);

        $logstr = "$systime $ip INFO：店舗管理者情報削除更新 UPDATE SQL文： ".$sql."\r\n";
        error_log($logstr,3,'../log/gen.log');

        $result = mysql_query($sql,$db);
        if(!$result){

            db_disConn($result, $link);
            $logstr = "$systime ERR：店舗管理者情報DB削除更新異常！ \r\n";
            $logstr .= "$systime $ip INFO：▲店舗管理者情報削除更新異常終了 \r\n";
            error_log($logstr,3,'../log/gen.log');

            $err_cd_list[]="01";
            $_SESSION['err_cd_list']=$err_cd_list;
            $url= URL_PATH . "err.php";
            redirect($url);
        }
        $logstr = "$systime $ip INFO：▲店舗管理者情報削除更新正常終了！！ \r\n";
        error_log($logstr,3,'../log/gen.log');

        $logstr = "$systime $ip INFO：▼店舗情報削除更新開始 \r\n";
        error_log($logstr,3,'../log/gen.log');
        $sql = sprintf("update shop set del_flg=1 where shop_id=%d",$u_id);
        $logstr = "$systime $ip INFO：店舗情報削除更新 UPDATE SQL文： ".$sql."\r\n";
        error_log($logstr,3,'../log/gen.log');

        $result = mysql_query($sql,$db);
        if(!$result){
            db_disConn($result, $link);
            $logstr = "$systime ERR：店舗情報DB削除更新異常！ \r\n";
            $logstr .= "$systime $ip INFO：▲店舗情報削除更新異常終了 \r\n";
            error_log($logstr,3,'../log/gen.log');

            $err_cd_list[]="01";
            $_SESSION['err_cd_list']=$err_cd_list;
            $url= URL_PATH . "err.php";
            redirect($url);
        }

        mysql_close($db);
        $logstr = "$systime $ip INFO：▲店舗情報削除更新正常終了！！ \r\n";
        error_log($logstr,3,'../log/gen.log');


		if($role!='1'){
			$url= URL_PATH . "m_shop.php?action=search&b_id=".$i_brand_id."&role=".$role."&l_id=".$login_user;
		}else{
			$url= URL_PATH . "m_shop.php?action=search&role=".$role."&l_id=".$login_user;
		}
        redirect($url);
    }
?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $sub_title; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">

<!-- jQuery の読み込み-->
<script type="text/javascript"  src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<!-- Google Maps API の読み込み -->
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&language=ja"></script>
<script type="text/javascript" src="../js/qrcode.js"></script>
<script type="text/javascript" src="../js/qr.js"></script>
<script language="javascript" src="../js/print.js"></script>
<script language="javascript">
function  jPrint(){
	$("#qr").jqprint();
}
</script>
</head>
<body onload='update_qrcode()'>
<div class='main'>
<div class='subtitle'><?php echo $sub_title; ?></div>
<form method='post' name='upform'>
<div class='input-area'>
    <label class='w150'>ログインID</label>
    <?php
    if($action=='update'||$action=='delete'||$update_flg=='1'||$role=='3'){
        echo "<label class='w200'>".$i_login_id."</label>";
    }else{
        echo"
            <input type='text' class='w200' name='i_login_id' ".$is_disabled." id='i_login_id' value='".$i_login_id."'/>
            <span class='input-span ml20 fl'>例：shop_001</span>
        ";
    }
    ?>
    <div style='clear:both;'></div>
    <label class='w150'>パスワード</label>
        <input type='password' class='w200' name='i_pwd' id='i_pwd' placeholder='<?php if($havePlaceholder=='YES') echo'';?>' value='<?php echo $i_password;?>' <?php echo $is_disabled;?>/>
        <span class='input-span ml20 fl'>例：shop_pwd_001</span>
    <!--startprint-->
	<?php
		if($action=='update'||$update_flg=='1'||$action=='confirm'){
        	echo "<div style='float:right;margin-right:111px;height:52px;width:52px' id='qr' ></div>";
		}
    ?>
	<!--endprint-->
    <div style='clear:both;'></div>
    <label class='w150'>パスワード確認</label>
        <input type='password' class='w200' name='i_pwd_r' id='i_pwd_r' placeholder='<?php if($havePlaceholder=='YES') echo'';?>'  value='<?php echo $i_password;?>' <?php echo $is_disabled;?>/>
        <span class='input-span ml20 fl'>例：shop_pwd_001</span>
    <div style='clear:both;'></div>
	<?php
		if($action=='update'||$update_flg=='1'||$action=='confirm'){
			echo "<input type='button' name='print' id='print' value='印刷' onclick='jPrint();' style='float:right;margin-right:103px;'>";
		}
	?>
    <label class='w150'>ステータス</label>
        <select name='i_status' id='i_status' class='re_select' <?php echo $is_disabled;?>>
            <option value='1'<?php if ($i_status=='1') {echo 'selected';}?>>アクティブ</option>
            <option value='0'<?php if ($i_status=='0') {echo 'selected';}?>>非アクティブ</option>
        </select>
    <div style='clear:both;'></div>
    <label class='w150'>有効期間</label>
        <input type='text' name='i_expired_date_from' id='i_expired_date_from' class='w100' placeholder='<?php if($havePlaceholder=='YES') echo'';?>' value='<?php echo $i_expired_date_from;?>' <?php echo $is_disabled;?>/>
        <label class='tcenter mg0'>〜</label>
        <input type='text' name='i_expired_date_to' id='i_expired_date_to' class='w100' placeholder='<?php if($havePlaceholder=='YES') echo'';?>' value='<?php echo $i_expired_date_to?>' <?php echo $is_disabled;?>/>
        <span class='input-span ml20 fl'>例：<?php echo $sysdate.' 〜 2024-12-31';?></span>
    <div style='clear:both;'></div>
	<label class='w150'>提携先</label>
		<select name='i_brand_id' id='i_brand_id' class='re_select w200' <?php echo $brand_is_disabled.' '.$is_disabled;?>>
				<?php
		        while($arr_list_row=mysql_fetch_array($result_list_brand)){
		        	if($arr_list_row[brand_id]==$i_brand_id){
		        		echo"<option value=".$arr_list_row[brand_id]." selected >".$arr_list_row[brand_name]. "</option>";
		        	}else{
		        		echo"<option value=".$arr_list_row[brand_id]." >".$arr_list_row[brand_name]. "</option>";
		        	}
		        }
        		?>
		</select>
	<div style='clear:both;'></div>
	<label class='w150'>店舗名</label>
		<input type='text' name='i_shop_name' id='i_shop_name' class='w500' placeholder='<?php if($havePlaceholder=='YES') echo '';?>' value='<?php echo htmlspecialchars_decode($i_shop_name); ?>' <?php echo $is_disabled;?>/>
	<div style='clear:both;'></div>
	<label class='w150'>都道府県</label>
		<select name='i_area_id' id='i_area_id' class='re_select w100' <?php echo $is_disabled;?>>
				<?php
		        while($arr_list_row=mysql_fetch_array($result_list_area)){
		        	if($arr_list_row[id]==$i_area_id){
		        		echo"<option value=".$arr_list_row[id]." selected >".$arr_list_row[name]. "</option>";
		        	}else{
		        		echo"<option value=".$arr_list_row[id]." >".$arr_list_row[name]. "</option>";
		        	}
		        }
        		?>
		</select>
	<div style='clear:both;'></div>
	<label class='w150'>住所</label>
		<input type='text' name='i_address' id='i_address' class='w500' placeholder='<?php if($havePlaceholder=='YES') echo htmlspecialchars_decode($i_address);?>' value='<?php echo $i_address; ?>' <?php echo $is_disabled;?>/>
        <span class='input-span ml20 fl'>例：東京都港区芝三丁目2番18号　NBF芝公園ビル</span>
	<div style='clear:both;'></div>
	<label class='w150'>緯度</label>
		<input type='text' name='i_latitude' id='i_latitude' class='w100' placeholder='<?php if($havePlaceholder=='YES') echo $i_latitude;?>' value='<?php echo $i_latitude; ?>' <?php echo $is_disabled;?>/>
    <div style='clear:both;'></div>
    <label class='w150'>経度</label>
        <input type='text' name='i_longitude' id='i_longitude' class='w100' placeholder='<?php if($havePlaceholder=='YES') echo $i_longitude;?>' value='<?php echo $i_longitude; ?>' <?php echo $is_disabled;?>/>
        <input type='button' id='btn' class='input-button buttonS bBlue ml20 fl mt3' value='緯度経度を取得' <?php echo $is_disabled;?>/>
        <span class='input-span ml20 fl'>※住所をご入力した上、緯度経度を取得してください。</span>
	<div style='clear:both;'></div>
	<label class='w150'>電話番号</label>
		<input type='text' name='i_tel' id='i_tel' class='w200' placeholder='<?php if($havePlaceholder=='YES') echo $i_tel;?>' value='<?php echo $i_tel; ?>' <?php echo $is_disabled;?>/>
        <span class='input-span ml20 fl'>例：03-6366-3131</span>
	<div style='clear:both;'></div>
	<label class='w150'>営業時間</label>
	<input type='text' name='i_open_time' id='i_open_time' class='w500' value='<?php echo $i_open_time; ?>' <?php echo $is_disabled;?>/>
	<span class='input-span ml20 fl'>例：平日10:00 - 18:00  土曜日:10:00 - 19:00  日曜日 11:00 - 20:00</span>
	<div style='clear:both;'></div>
	<label class='w150'>定休日</label>
		<input type='text' name='i_rest_day' id='i_rest_day' class='w300' value='<?php echo $i_rest_day; ?>' <?php echo $is_disabled;?>/>
        <span class='input-span ml20 fl'>例：定休日なし（年末年始除く）または毎週月曜日など</span>
	<div style='clear:both;'></div>
	<label class='w150'>バーコード</label>
		<input type='text' name='i_barcode' id='i_barcode' class='w200' value='<?php echo $i_barcode; ?>' <?php echo $is_disabled;?>/>
        <span class='input-span ml20 fl'>例：14901234567891</span>
	<div style='clear:both;'></div>
    <input type="hidden" name="u_id" value="<?php echo $u_id;?>" />
    <input type="hidden" name="update_flg" value="<?php echo $update_flg;?>"/>
    <input type="hidden" name="h_login_id" value="<?php echo $i_login_id;?>"/>
	<?php
    if($action=='confirm'){
        if($update_flg=='1'){
            echo "
                <input type='button' class='input-button buttonS bGreen ml190 w200 mt40' value='訂正' onclick='editMode();'/>
                <input type='button' class='input-button buttonS bGreen ml100 w200 mt40' value='更新' onclick='updateSubmit();'/>
            ";
        }else{
            echo "
                <input type='button' class='input-button buttonS bGreen ml190 w200 mt40' value='訂正' onclick='editMode();'/>
                <input type='button' class='input-button buttonS bGreen ml100 w200 mt40' value='送信' onclick='confirmSubmit();'/>
            ";
        }
    }
    elseif($action=='delete'){
        echo "<input type='button' class='input-button buttonS bGreen ml190 w200 mt40' value='削除する' onclick='deleteSubmit();'/>";
    }
    else{
		echo "<input type='button' class='input-button buttonS bGreen ml190 w200 mt40' value='確認画面へ' onclick='moveConfirm();'/>";
	}
	?>
</div>
<script type="text/javascript" language="javascript">
	function moveConfirm() {
        //ログインID
        if(document.upform.update_flg.value != "1"){
            if(document.upform.i_login_id.value == ""){
                alert("ログインIDを入力してください。");
                document.upform.i_login_id.focus();
                return false;
            }
        }
        //パスワード
        if(document.upform.i_pwd.value == ""){
            alert("パスワードを入力してください。");
            document.upform.i_pwd.focus();
            return false;
        }
        //パスワード確認
        if(document.upform.i_pwd_r.value != document.upform.i_pwd.value){
            alert("パスワードを確認してください。");
            document.upform.i_pwd_r.focus();
            return false;
        }
		//店舗名
		 if(document.upform.i_shop_name.value == ""){
		  alert("店舗名を入力してください。");
		  document.upform.i_shop_name.focus();
		  return false;
		 }
        //住所
        if(document.upform.i_address.value == ""){
            alert("住所を入力してください。");
            document.upform.i_address.focus();
            return false;
        }
        //緯度経度
        if(document.upform.i_latitude.value == ""||document.upform.i_longitude.value == ""){
            alert("緯度経度を取得してください。");
            document.upform.btn.focus();
            return false;
        }
		  //submit
		document.upform.action="?action=confirm";
		document.upform.submit();
	}
	function confirmSubmit() {
        if(document.upform.update_flg.value != "1"){
            document.getElementById('i_login_id').disabled=false;
        }
        document.getElementById('i_pwd').disabled=false;
        document.getElementById('i_status').disabled=false;
        document.getElementById('i_expired_date_from').disabled=false;
        document.getElementById('i_expired_date_to').disabled=false;
        document.getElementById('i_brand_id').disabled=false;
        document.getElementById('i_shop_name').disabled=false;
        document.getElementById('i_area_id').disabled=false;
        document.getElementById('i_address').disabled=false;
        document.getElementById('i_latitude').disabled=false;
        document.getElementById('i_longitude').disabled=false;
        document.getElementById('i_tel').disabled=false;
        document.getElementById('i_open_time').disabled=false;
        document.getElementById('i_rest_day').disabled=false;
		document.getElementById('i_barcode').disabled=false;

		document.upform.action="?action=insert";
		document.upform.submit();
	}
    function updateSubmit() {
        if(document.upform.update_flg.value != "1"){
            document.getElementById('i_login_id').disabled=false;
        }
        document.getElementById('i_pwd').disabled=false;
        document.getElementById('i_status').disabled=false;
        document.getElementById('i_expired_date_from').disabled=false;
        document.getElementById('i_expired_date_to').disabled=false;
        document.getElementById('i_brand_id').disabled=false;
        document.getElementById('i_shop_name').disabled=false;
        document.getElementById('i_area_id').disabled=false;
        document.getElementById('i_address').disabled=false;
        document.getElementById('i_latitude').disabled=false;
        document.getElementById('i_longitude').disabled=false;
        document.getElementById('i_tel').disabled=false;
        document.getElementById('i_open_time').disabled=false;
        document.getElementById('i_rest_day').disabled=false;
		document.getElementById('i_barcode').disabled=false;

        document.upform.action="?action=updatesubmit";
        document.upform.submit();
    }
    function deleteSubmit() {
        document.upform.action="?action=deletesubmit";
        document.upform.submit();
    }
	function editMode(){
        if(document.upform.update_flg.value != "1"){
            document.getElementById('i_login_id').disabled=false;
        }
		document.getElementById('i_pwd').disabled=false;
		document.getElementById('i_status').disabled=false;
		document.getElementById('i_expired_date_from').disabled=false;
		document.getElementById('i_expired_date_to').disabled=false;
		document.getElementById('i_brand_id').disabled=false;
        document.getElementById('i_shop_name').disabled=false;
        document.getElementById('i_area_id').disabled=false;
        document.getElementById('i_address').disabled=false;
        document.getElementById('i_latitude').disabled=false;
        document.getElementById('i_longitude').disabled=false;
        document.getElementById('i_tel').disabled=false;
        document.getElementById('i_open_time').disabled=false;
        document.getElementById('i_rest_day').disabled=false;
		document.getElementById('i_barcode').disabled=false;

		document.upform.action="?action=edit";
		document.upform.submit();
	}
    //jQuery.noConflict();
    jQuery(document).ready(function($){
        var geocoder;
        // ページロード時実行
        $(function(){
            initialize();
            $("#btn").bind("click",function(){
                getLOL();
            });
//                .trigger("click");
        });

        function getLOL() {
            var address = document.getElementById('i_address').value;
            // geocode (住所から緯度経度を取得)
                geocoder.geocode( { 'address': address, 'language': 'ja'}, function(results, status) {
                if(status==google.maps.GeocoderStatus.OK){
                    var latLngArr = results[0].geometry.location.toUrlValue();
                    var Arrayltlg = latLngArr.split(",");
                    document.getElementById("i_latitude").value = Arrayltlg[0];
                    document.getElementById("i_longitude").value = Arrayltlg[1];
                }else{
                    alert("緯度経度の取得に失敗しました\n"+status);
                }
            });
            return false;
        }
        function initialize(){
//            document.getElementById("i_address").value = "東京都";
            // ジオコードオブジェクト
            geocoder = new google.maps.Geocoder();
            // 緯度・経度：日本
//            var myLatlng=new google.maps.LatLng(36.204824,138.252924);
        }
    });
//	jQuery(function(){
//		var shop_id=document.upform.u_id.value;
//		var brand_id=document.upform.i_brand_id.value;
//		var options = {
//			render	: "table",
//			ecLevel: 'L',
//			mpSize:4,
//			text: '{"qr":{"Shop_id":"'+shop_id+'","Brand_id":"'+brand_id+'"}',
//			width: 88,
//			height: 88
//		};
//		//jQuery('#qr').empty.qrcode(options);
//		jQuery('#qr').qrcode(options);
//	})
	function preview()
	{
		bdhtml=window.document.body.innerHTML;
		sprnstr="<!--startprint-->";
		eprnstr="<!--endprint-->";
		prnhtml=bdhtml.substr(bdhtml.indexOf(sprnstr)+17);
		prnhtml=prnhtml.substring(0,prnhtml.indexOf(eprnstr));
		window.document.body.innerHTML=prnhtml;
		window.print();
	}


</script>
</form>
</div>
</body>
</html>