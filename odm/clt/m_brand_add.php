<?php
session_start();
    header("Expires:Mon,26 Jul 1970 05:00:00 GMT");
    header("Last-Modified:".gmdate("D, d M Y H:i:s")."GMT");
    header("Cache-Control:no-cache,must-revalidate");
    header("Pragma:no-cache");
	//include
	require '../util/include.php';

    $role=$_SESSION['role'];
    $login_user=$_SESSION['login_user'];

	$action = $_GET['action'];
	$sysdate=date('Y-m-d',time());
	$systime=date('Y-m-d H:i:s',time());
	$ip=get_real_ip();

	//プルダウンリスト取得
	$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	if(!$db){
		die("connot connect:" . mysql_error());
	}
	$dns = mysql_select_db(DB_NAME,$db);
	if(!$dns){
		die("connot use db:" . mysql_error());
	}
	mysql_set_charset('utf8');
	$sqlall = "select * from category WHERE 1 and is_show=1";
	$result_list_cat = mysql_query($sqlall,$db);
    $sqlall = "select * from user_rank WHERE 1 and status=1";
    $result_list_prop = mysql_query($sqlall,$db);
	mysql_close($db);

	if ($action=='confirm'){
		$sub_title='提携先管理　- 新規追加確認 -';
		$is_disabled="disabled='disabled'";
		$havePlaceholder='NO';

		//form項目
        $u_id = $_POST['u_id'];
        $update_flg=$_POST['update_flg'];
        $h_img_path0=$_POST['h_img_path0'];
        $h_img_path1=$_POST['h_img_path1'];
        $h_img_path2=$_POST['h_img_path2'];
        $h_img_path3=$_POST['h_img_path3'];
        $i_brand_name = $_POST['i_brand_name'];
		$i_brand_name = addslashes($i_brand_name);
        $i_category_id=$_POST['h_category_id'];
		$arr_category_id=explode(",",$i_category_id);
        $i_prop_id=$_POST['h_prop_id'];
		$arr_prop_id=explode(",",$i_prop_id);
        if($update_flg=='1'){
            $i_login_id=$_POST['h_login_id'];
        }else{
            $i_login_id=$_POST['i_login_id'];
        }
        $i_password=$_POST['i_pwd'];
        $i_status = $_POST['i_status'];
        if($i_status==''){
            $i_status=0;
        }
        $i_sort_order = $_POST['i_sort_order'];
        if($i_sort_order==''){
            $i_sort_order=50;
        }
        $i_discount_rate = $_POST['i_discount_rate'];
        $i_charge_day = $_POST['i_charge_day'];
				$i_rate = $_POST['i_rate'];
				$i_ec_url = $_POST['i_ec_url'];
        $i_content = $_POST['i_content'];
        $i_content=htmlspecialchars($i_content);
        $i_content2 = $_POST['i_content2'];
        $i_content2=htmlspecialchars($i_content2);
        $img_path0=$_POST['h_destination0'];
        $img_path1=$_POST['h_destination1'];
        $img_path2=$_POST['h_destination2'];
        $img_path3=$_POST['h_destination3'];

        $i_expired_date_from= $_POST['i_expired_date_from'];
        if($i_expired_date_from==''){
            $i_expired_date_from=$sysdate;
        }
        $i_expired_date_to= $_POST['i_expired_date_to'];
        if($i_expired_date_to==''){
            $i_expired_date_to='2030-12-31';
        }
        $i=1;
        while($_POST['i_kp_code'.$i]!=''||$_POST['i_rank_code'.$i]!=''||$_POST['i_bl_code'.$i]!=''){
			$i_kp_code[$i]=$_POST['i_kp_code'.$i];
			$i_rank_code[$i]=$_POST['i_rank_code'.$i];
			$i_bl_code[$i]=$_POST['i_bl_code'.$i];
			$i++;
		}
		$i=1;
        while($_POST['i_sort_code'.$i]!=''||$_POST['i_rank_code_sort'.$i]!=''){
			$i_sort_code[$i]=$_POST['i_sort_code'.$i];
			$i_rank_code_sort[$i]=$_POST['i_rank_code_sort'.$i];
			$i++;
		}
	}
	elseif($action=='edit'){
		$sub_title='提携先管理　- 新規追加訂正 -';
		$is_disabled="";
		$havePlaceholder='YES';

		//form項目
        $u_id = $_POST['u_id'];
        $update_flg=$_POST['update_flg'];
        $h_img_path0=$_POST['h_img_path0'];
        $h_img_path1=$_POST['h_img_path1'];
        $h_img_path2=$_POST['h_img_path2'];
        $h_img_path3=$_POST['h_img_path3'];
        $i_brand_name = $_POST['i_brand_name'];
		$i_brand_name = addslashes($i_brand_name);
        $i_category_id=$_POST['h_category_id'];
		$arr_category_id=explode(",",$i_category_id);
        $i_prop_id=$_POST['h_prop_id'];
		$arr_prop_id=explode(",",$i_prop_id);
        if($update_flg=='1'){
            $i_login_id=$_POST['h_login_id'];
        }else{
            $i_login_id=$_POST['i_login_id'];
        }
        $i_password=$_POST['i_pwd'];
        $i_status = $_POST['i_status'];
        if($i_status==''){
            $i_status=0;
        }
        $i_sort_order = $_POST['i_sort_order'];
        if($i_sort_order==''){
            $i_sort_order=50;
        }
        $i_discount_rate = $_POST['i_discount_rate'];
        $i_charge_day = $_POST['i_charge_day'];
				$i_rate = $_POST['i_rate'];
				$i_ec_url = $_POST['i_ec_url'];
        $i_content = $_POST['i_content'];
        $i_content=htmlspecialchars($i_content);
        $i_content2 = $_POST['i_content2'];
        $i_content2=htmlspecialchars($i_content2);
        $img_path0=$_POST['h_destination0'];
        $img_path1=$_POST['h_destination1'];
        $img_path2=$_POST['h_destination2'];
        $img_path3=$_POST['h_destination3'];

        $i_expired_date_from= $_POST['i_expired_date_from'];
        if($i_expired_date_from==''){
            $i_expired_date_from=$sysdate;
        }
        $i_expired_date_to= $_POST['i_expired_date_to'];
        if($i_expired_date_to==''){
            $i_expired_date_to='2030-12-31';
        }
		$i=1;
		while($_POST['i_kp_code'.$i]!=''||$_POST['i_rank_code'.$i]!=''||$_POST['i_bl_code'.$i]!=''){
			$i_kp_code[$i]=$_POST['i_kp_code'.$i];
			$i_rank_code[$i]=$_POST['i_rank_code'.$i];
			$i_bl_code[$i]=$_POST['i_bl_code'.$i];
			$i++;
		}
		$h_num_cnt=$i+1;
		$i=1;
		while($_POST['i_sort_code'.$i]!=''||$_POST['i_rank_code_sort'.$i]!=''){
			$i_sort_code[$i]=$_POST['i_sort_code'.$i];
			$i_rank_code_sort[$i]=$_POST['i_rank_code_sort'.$i];
			$i++;
		}
		$h_num_cnt_sort=$i+1;

        if($img_path0==''){
            $img_path0=BRAND_UPLOAD_FOLDER.'default_img_logo.png';
        }
        if($img_path1==''){
            $img_path1=BRAND_UPLOAD_FOLDER.'default_img.png';
        }
        if($img_path2==''){
            $img_path2=BRAND_UPLOAD_FOLDER.'default_img.png';
        }
        if($img_path3==''){
            $img_path3=BRAND_UPLOAD_FOLDER.'default_img.png';
        }
	}
    elseif ($action=='update'||$action=='delete'){
        if($action=='update'){
            $sub_title='提携先管理　- 編集 -';
            $is_disabled="";
            $havePlaceholder='NO';
        }
        if($action=='delete'){
            $sub_title='提携先管理　- 削除 -';
            $is_disabled="disabled='disabled'";
            $havePlaceholder='NO';
        }

        $u_id = $_GET['u_id'];

        $update_flg='1';

        $logstr = "$systime $ip INFO：▼提携先情報取得開始：brand_id = $u_id \r\n";
        error_log($logstr,3,'../log/gen.log');

        if($u_id==''){

            db_disConn($result, $link);
            $logstr = "$systime ERR：brand_id取得エラー！ \r\n";
            $logstr .= "$systime $ip INFO：▲提携先情報取得異常終了 \r\n";
            error_log($logstr,3,'../log/gen.log');

            $err_cd_list[]="02";
            $_SESSION['err_cd_list']=$err_cd_list;
            $url= URL_PATH . "err.php";
            redirect($url);
        }

        //情報取得
        $db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        if(!$db){
            die("connot connect:" . mysql_error());
        }
        $dns = mysql_select_db(DB_NAME,$db);
        if(!$dns){
            die("connot use db:" . mysql_error());
        }
        mysql_set_charset('utf8');
        $sqlall = "select * from brand WHERE 1";
        $sqlall .= " and brand_id = $u_id";

        $result = mysql_query($sqlall,$db);
        $rs=mysql_fetch_object($result);


        $i_brand_name = $rs->brand_name;
        $i_category_id=$rs->cat_id;
		$arr_category_id=explode(",",$i_category_id);
        $i_prop_id=$rs->property_id;
		$arr_prop_id=explode(",",$i_prop_id);
        $i_login_id=$rs->login_id;
        $i_password=$rs->password;
        $i_status = $rs->status;
        $i_sort_order = $rs->sort_order;
        $i_discount_rate = $rs->discount_rate;
        $i_charge_day = $rs->charge_day;
		$i_rate = $rs->rate;
		$i_ec_url = $rs->ec_url;
        $i_content = $rs->brand_desc;
        $i_content=htmlspecialchars($i_content);

        $i_content2 = $rs->discount_out_contents;
        $i_content2=htmlspecialchars($i_content2);

        $h_img_path0=$rs->brand_logo;
        $h_img_path0=IMG_URL_PATH.$h_img_path0;
        $h_img_path1=$rs->brand_image;
        $h_img_path1=IMG_URL_PATH.$h_img_path1;
        $h_img_path2=$rs->brand_image2;
        $h_img_path2=IMG_URL_PATH.$h_img_path2;
        $h_img_path3=$rs->brand_image3;
        $h_img_path3=IMG_URL_PATH.$h_img_path3;

        $i_expired_date_from= $rs->expired_date_from;
        $i_expired_date_from=date('Y-m-d',$i_expired_date_from);
        $i_expired_date_to= $rs->expired_date_to;
        $i_expired_date_to=date('Y-m-d',$i_expired_date_to);


		$sqlall = "select * from den_kp_code WHERE 1";
		$sqlall .= " and brand_id = $u_id";
		$sqlall .= " order by rank_id";

		$result_kp = mysql_query($sqlall,$db);

		$i=1;
		while($rskp=mysql_fetch_object($result_kp)){
			$i_rank_code[$i]=$rskp->rank_id;
			$i_kp_code[$i]=$rskp->kp_code;
			$i_bl_code[$i]=$rskp->bl_code;
			$i++;
		}
		$h_num_cnt=$i+1;
		
		$sqlall = "select * from den_rank_sort WHERE 1";
		$sqlall .= " and brand_id = $u_id";
		$sqlall .= " order by rank_id";

		$result_sort = mysql_query($sqlall,$db);

		$i=1;
		while($rskp=mysql_fetch_object($result_sort)){
			$i_rank_code_sort[$i]=$rskp->rank_id;
			$i_sort_code[$i]=$rskp->sort_code;
			$i++;
		}
		$h_num_cnt_sort=$i+1;

        mysql_close($db);

        $logstr = "$systime $ip INFO：▲提携先情報取得正常終了：brand_id = $u_id \r\n";
        error_log($logstr,3,'../log/gen.log');

    }
	else{
		$sub_title='提携先管理　- 新規追加 -';
		$is_disabled="";
		$havePlaceholder='YES';
		//form項目
        $i_brand_name = '';
        $i_category_id= '';
        $i_prop_id= '';
        if($role!='1') {
            $i_login_id= $login_user;
        }
        else{
            $i_login_id= '';
        }
        $i_password= '';
        $i_status = '';
        $i_sort_order = '';
        $i_discount_rate = '';
        $i_charge_day = '';
				$i_rate = '';
        $ec_url='';
        $i_content = '';
        $i_content2 = '';
        $img_path0= '';
        $img_path1= '';
        $img_path2= '';
        $img_path3= '';
	}

	if ($action=='confirm'){
        if($update_flg!='1'){
            $logstr = "$systime $ip INFO：▼提携先名存在チェック開始 \r\n";
            error_log($logstr,3,'../log/gen.log');
            //提携先名存在チェック
            $ret=searchBrandInfo($i_brand_name);
            if($ret=='1'){

                db_disConn($result, $link);
                $logstr = "$systime ERR：提携先名存在チェックエラー！ \r\n";
                $logstr .= "$systime $ip INFO：▲提携先名存在チェック異常終了 \r\n";
                error_log($logstr,3,'../log/gen.log');

                $err_cd_list[]="02";
                $_SESSION['err_cd_list']=$err_cd_list;
                $url= URL_PATH . "err.php";
                redirect($url);
            }
            $logstr = "$systime $ip INFO：▲提携先名存在チェック正常終了！！ \r\n";
            error_log($logstr,3,'../log/gen.log');
        }
		$db_file_name_moto=time();
		if($_FILES["upfile0"]["tmp_name"]!=''){
			$file_tmp_name=$_FILES["upfile0"]["tmp_name"];
			$file = $_FILES["upfile0"];
			$pinfo=pathinfo($file["name"]);
			$ftype0=$pinfo['extension'];
			$db_file_name=$i_login_id.'_0';
			$destination0 = BRAND_UPLOAD_FOLDER.$db_file_name.".".$ftype0;
			$show_dest0=BRAND_UPLOAD_FOLDER_ROOT.$db_file_name.".".$ftype0;
			$ret=imgUpload($file_tmp_name,$file,$db_file_name,BRAND_UPLOAD_FOLDER_ROOT);

			if($ret!='0'){
				$err_cd_list[]="01";
				$_SESSION['err_cd_list']=$err_cd_list;
				$url= URL_PATH . "err.php";
				redirect($url);
			}
		}
        if($_FILES["upfile1"]["tmp_name"]!=''){
            $file_tmp_name=$_FILES["upfile1"]["tmp_name"];
            $file = $_FILES["upfile1"];
            $pinfo=pathinfo($file["name"]);
            $ftype1=$pinfo['extension'];
            $db_file_name=$i_login_id.'_1';
            $destination1 = BRAND_UPLOAD_FOLDER.$db_file_name.".".$ftype1;
            $show_dest1=BRAND_UPLOAD_FOLDER_ROOT.$db_file_name.".".$ftype1;
            $ret=imgUpload($file_tmp_name,$file,$db_file_name,BRAND_UPLOAD_FOLDER_ROOT);

            if($ret!='0'){
                $err_cd_list[]="01";
                $_SESSION['err_cd_list']=$err_cd_list;
                $url= URL_PATH . "err.php";
                redirect($url);
            }
        }
        if($_FILES["upfile2"]["tmp_name"]!=''){
            $file_tmp_name=$_FILES["upfile2"]["tmp_name"];
            $file = $_FILES["upfile2"];
            $pinfo=pathinfo($file["name"]);
            $ftype2=$pinfo['extension'];
            $db_file_name=$i_login_id.'_2';
            $destination2 = BRAND_UPLOAD_FOLDER.$db_file_name.".".$ftype2;
            $show_dest2=BRAND_UPLOAD_FOLDER_ROOT.$db_file_name.".".$ftype2;
            $ret=imgUpload($file_tmp_name,$file,$db_file_name,BRAND_UPLOAD_FOLDER_ROOT);

            if($ret!='0'){
                $err_cd_list[]="01";
                $_SESSION['err_cd_list']=$err_cd_list;
                $url= URL_PATH . "err.php";
                redirect($url);
            }
        }
        if($_FILES["upfile3"]["tmp_name"]!=''){
            $file_tmp_name=$_FILES["upfile3"]["tmp_name"];
            $file = $_FILES["upfile3"];
            $pinfo=pathinfo($file["name"]);
            $ftype3=$pinfo['extension'];
            $db_file_name=$i_login_id.'_3';
            $destination3 = BRAND_UPLOAD_FOLDER.$db_file_name.".".$ftype3;
            $show_dest3=BRAND_UPLOAD_FOLDER_ROOT.$db_file_name.".".$ftype3;
            $ret=imgUpload($file_tmp_name,$file,$db_file_name,BRAND_UPLOAD_FOLDER_ROOT);

            if($ret!='0'){
                $err_cd_list[]="01";
                $_SESSION['err_cd_list']=$err_cd_list;
                $url= URL_PATH . "err.php";
                redirect($url);
            }
        }
	}

	//insert
if ($action=='insert'){

    $i_brand_name = $_POST['i_brand_name'];
	$i_brand_name = addslashes($i_brand_name);
    $i_category_id=$_POST['h_category_id'];
	$arr_category_id=explode(",",$i_category_id);
    $i_prop_id=$_POST['h_prop_id'];
	$arr_prop_id=explode(",",$i_prop_id);
    $i_login_id=$_POST['i_login_id'];
    $i_password=$_POST['i_pwd'];
    $i_status = $_POST['i_status'];
    if($i_status==''){
        $i_status=0;
    }
    $i_sort_order = $_POST['i_sort_order'];
    if($i_sort_order==''){
        $i_sort_order=50;
    }
    $i_discount_rate = $_POST['i_discount_rate'];
    $i_charge_day = $_POST['i_charge_day'];
	$i_rate = $_POST['i_rate'];
	$i_ec_url = $_POST['i_ec_url'];
    $i_content = $_POST['i_content'];
	$i_content=htmlspecialchars($i_content);
    $i_content2 = $_POST['i_content2'];
	$i_content2=htmlspecialchars($i_content2);
    $img_path0=$_POST['h_destination0'];
    $img_path1=$_POST['h_destination1'];
    $img_path2=$_POST['h_destination2'];
    $img_path3=$_POST['h_destination3'];

    $i_expired_date_from= $_POST['i_expired_date_from'];
    if($i_expired_date_from==''){
        $i_expired_date_from=$sysdate;
    }
    $i_expired_date_to= $_POST['i_expired_date_to'];
    if($i_expired_date_to==''){
        $i_expired_date_to='2030-12-31';
    }
	$i=1;
	while($_POST['i_kp_code'.$i]!=''||$_POST['i_rank_code'.$i]!=''||$_POST['i_bl_code'.$i]!=''){
		$i_kp_code[$i]=$_POST['i_kp_code'.$i];
		$i_rank_code[$i]=$_POST['i_rank_code'.$i];
		$i_bl_code[$i]=$_POST['i_bl_code'.$i];
		$i++;
	}
	$i=1;
	while($_POST['i_sort_code'.$i]!=''||$_POST['i_rank_code_sort'.$i]!=''){
		$i_sort_code[$i]=$_POST['i_sort_code'.$i];
		$i_rank_code_sort[$i]=$_POST['i_rank_code_sort'.$i];
		$i++;
	}

//    if($img_path0==''){
//        $img_path0=BRAND_UPLOAD_FOLDER.$i_login_id.'_0.png';
//    }
//    if($img_path1==''){
//        $img_path1=BRAND_UPLOAD_FOLDER.$i_login_id.'_1.png';
//    }
//    if($img_path2==''){
//        $img_path2=BRAND_UPLOAD_FOLDER.$i_login_id.'_2.png';
//    }
//    if($img_path3==''){
//        $img_path3=BRAND_UPLOAD_FOLDER.$i_login_id.'_3.png';
//    }

    $db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
    if(!$db){
        die("connot connect:" . mysql_error());
    }

    $dns = mysql_select_db(DB_NAME,$db);

    if(!$dns){
        die("connot use db:" . mysql_error());
    }

    mysql_set_charset('utf8');


    $logstr = "$systime $ip INFO：▼提携先名存在チェック開始 \r\n";
    error_log($logstr,3,'../log/gen.log');
    //提携先名存在チェック
    $ret=searchBrandInfo($i_brand_name);
    if($ret=='1'){

        db_disConn($result, $link);
        $logstr = "$systime ERR：提携先名存在チェックエラー！ \r\n";
        $logstr .= "$systime $ip INFO：▲提携先名存在チェック異常終了 \r\n";
        error_log($logstr,3,'../log/gen.log');

        $err_cd_list[]="02";
        $_SESSION['err_cd_list']=$err_cd_list;
        $url= URL_PATH . "err.php";
        redirect($url);
    }
    $logstr = "$systime $ip INFO：▲提携先名存在チェック正常終了！！ \r\n";
    error_log($logstr,3,'../log/gen.log');

    if($role=='1'){
        $logstr = "$systime $ip INFO：▼提携先管理者情報登録開始 \r\n";
        error_log($logstr,3,'../log/gen.log');

        //管理者情報存在チェック
        $ret=searchAdminInfo($i_login_id);
        if($ret=='1'){

            db_disConn($result, $link);
            $logstr = "$systime ERR：提携先管理者情報DB登録存在チェックエラー！ \r\n";
            $logstr .= "$systime $ip INFO：▲提携先管理者情報登録異常終了 \r\n";
            error_log($logstr,3,'../log/gen.log');

            $err_cd_list[]="02";
            $_SESSION['err_cd_list']=$err_cd_list;
            $url= URL_PATH . "err.php";
            redirect($url);
        }

        $sql = sprintf("insert into den_admin (u_id,pwd,role,insert_time) values ('%s','%s',%d,%d)",
            $i_login_id,$i_password,2,strtotime($systime)
        );

        $logstr = "$systime $ip INFO：提携先管理者情報登録 INSERT SQL文： ".$sql."\r\n";
        error_log($logstr,3,'../log/gen.log');

        $result = mysql_query($sql,$db);
        if(!$result){

            db_disConn($result, $link);
            $logstr = "$systime ERR：提携先管理者情報DB登録異常！ \r\n";
            $logstr .= "$systime $ip INFO：▲提携先管理者情報登録異常終了 \r\n";
            error_log($logstr,3,'../log/gen.log');

            $err_cd_list[]="01";
            $_SESSION['err_cd_list']=$err_cd_list;
            $url= URL_PATH . "err.php";
            redirect($url);
        }
        $logstr = "$systime $ip INFO：▲提携先管理者情報登録正常終了！！ \r\n";
        error_log($logstr,3,'../log/gen.log');
    }
    $logstr = "$systime $ip INFO：▼提携先情報登録開始 \r\n";
    error_log($logstr,3,'../log/gen.log');
    $sql = sprintf("insert into brand (
                            cat_id,
                            property_id,
                            brand_name,
                            brand_logo,
                            brand_image,
                            brand_image2,
                            brand_image3,
                            brand_desc,
                            sort_order,
                            login_id,
                            password,
                            discount_rate,
                            charge_day,
                            rate,
                            ec_url,
                            discount_out_contents,
                            status,
                            expired_date_from,
                            expired_date_to,
                            insert_time
                            ) values
                            (%d,%d,'%s','%s','%s','%s','%s','%s',%d,'%s','%s','%s','%s',%d,'%s','%s',%d,%d,%d,%d)",
        $i_category_id,
        $i_prop_id,
        $i_brand_name,
        $img_path0,
        $img_path1,
        $img_path2,
        $img_path3,
        $i_content,
        $i_sort_order,
        $i_login_id,
        $i_password,
        $i_discount_rate,
        $i_charge_day,
				$i_rate,
				$i_ec_url,
        $i_content2,
        $i_status,
        strtotime($i_expired_date_from),
        strtotime($i_expired_date_to),
        strtotime($systime)
    );
    $logstr = "$systime $ip INFO：提携先情報登録 INSERT SQL文： ".$sql."\r\n";
    error_log($logstr,3,'../log/gen.log');

    $result = mysql_query($sql,$db);
    if(!$result){
        db_disConn($result, $link);
        $logstr = "$systime ERR：提携先情報DB登録異常！ \r\n";
        $logstr .= "$systime $ip INFO：▲提携先情報登録異常終了 \r\n";
        error_log($logstr,3,'../log/gen.log');

        $err_cd_list[]="01";
        $_SESSION['err_cd_list']=$err_cd_list;
        $url= URL_PATH . "err.php";
        redirect($url);
    }
	$logstr = "$systime $ip INFO：▲提携先情報登録正常終了！！ \r\n";
	error_log($logstr,3,'../log/gen.log');


	$sqlbrandid = "select * from brand WHERE 1 and login_id='".$i_login_id."'";
	$result_list_brandid = mysql_query($sqlbrandid,$db);
	$rs=mysql_fetch_object($result_list_brandid);
	$kp_brand_id=$rs->brand_id;

	$i=1;
	while (($i_rank_code[$i]!=''||$i_kp_code[$i]!=''||$i_bl_code[$i]!='')&&$kp_brand_id!=''){
		$logstr = "$systime $ip INFO：▼クーポンコード情報登録開始 \r\n";
		error_log($logstr,3,'../log/gen.log');
		$sql = sprintf("insert into den_kp_code (brand_id,rank_id,kp_code,bl_code) values (%d,'%s','%s','%s')",$kp_brand_id,$i_rank_code[$i],$i_kp_code[$i],$i_bl_code[$i]);
		$logstr = "$systime $ip INFO：クーポンコード情報登録 INSERT SQL文： ".$sql."\r\n";
		error_log($logstr,3,'../log/gen.log');

		$result = mysql_query($sql,$db);
		if(!$result){
			db_disConn($result, $link);
			$logstr = "$systime ERR：クーポンコード情報DB登録異常！ \r\n";
			$logstr .= "$systime $ip INFO：▲クーポンコード情報登録異常終了 \r\n";
			error_log($logstr,3,'../log/gen.log');

			$err_cd_list[]="01";
			$_SESSION['err_cd_list']=$err_cd_list;
			$url= URL_PATH . "err.php";
			redirect($url);
		}
		$logstr = "$systime $ip INFO：▲クーポンコード情報登録正常終了！！ \r\n";
		error_log($logstr,3,'../log/gen.log');
		$i++;
	}
	$i=1;
	while (($i_rank_code_sort[$i]!=''||$i_sort_code[$i]!='')&&$kp_brand_id!=''){
		$logstr = "$systime $ip INFO：▼会員種別ごと並び順情報登録開始 \r\n";
		error_log($logstr,3,'../log/gen.log');
		$sql = sprintf("insert into den_rank_sort (brand_id,rank_id,sort_code) values (%d,'%s','%s')",$kp_brand_id,$i_rank_code_sort[$i],$i_sort_code[$i]);
		$logstr = "$systime $ip INFO：会員種別ごと並び順情報登録 INSERT SQL文： ".$sql."\r\n";
		error_log($logstr,3,'../log/gen.log');

		$result = mysql_query($sql,$db);
		if(!$result){
			db_disConn($result, $link);
			$logstr = "$systime ERR：会員種別ごと並び順情報DB登録異常！ \r\n";
			$logstr .= "$systime $ip INFO：▲会員種別ごと並び順情報登録異常終了 \r\n";
			error_log($logstr,3,'../log/gen.log');

			$err_cd_list[]="01";
			$_SESSION['err_cd_list']=$err_cd_list;
			$url= URL_PATH . "err.php";
			redirect($url);
		}
		$logstr = "$systime $ip INFO：▲会員種別ごと並び順情報登録正常終了！！ \r\n";
		error_log($logstr,3,'../log/gen.log');
		$i++;
	}

	mysql_close($db);
    $url= URL_PATH . "m_brand.php?action=search&role=".$role."&l_id=".$login_user;

    redirect($url);
}

//更新実行
if ($action=='updatesubmit'){

    $u_id=$_POST['u_id'];
    $i_brand_name = $_POST['i_brand_name'];
	$i_brand_name = addslashes($i_brand_name);
    $i_category_id=$_POST['h_category_id'];
	$arr_category_id=explode(",",$i_category_id);
    $i_prop_id=$_POST['h_prop_id'];
	$arr_prop_id=explode(",",$i_prop_id);
    $i_login_id=$_POST['h_login_id'];
    $i_password=$_POST['i_pwd'];
    $i_status = $_POST['i_status'];
    if($i_status==''){
        $i_status=0;
    }
    $i_sort_order = $_POST['i_sort_order'];
    if($i_sort_order==''){
        $i_sort_order=50;
    }
    $i_discount_rate = $_POST['i_discount_rate'];
    $i_charge_day = $_POST['i_charge_day'];
	$i_rate = $_POST['i_rate'];
	$i_ec_url = $_POST['i_ec_url'];
    $i_content = $_POST['i_content'];
	$i_content=htmlspecialchars($i_content);
    $i_content2 = $_POST['i_content2'];
	$i_content2=htmlspecialchars($i_content2);
	$i=1;
	while($_POST['i_kp_code'.$i]!=''||$_POST['i_rank_code'.$i]!=''||$_POST['i_bl_code'.$i]!=''){
		$i_kp_code[$i]=$_POST['i_kp_code'.$i];
		$i_rank_code[$i]=$_POST['i_rank_code'.$i];
		$i_bl_code[$i]=$_POST['i_bl_code'.$i];
		$i++;
	}
	$i=1;
	while($_POST['i_sort_code'.$i]!=''||$_POST['i_rank_code_sort'.$i]!=''){
		$i_sort_code[$i]=$_POST['i_sort_code'.$i];
		$i_rank_code_sort[$i]=$_POST['i_rank_code_sort'.$i];
		$i++;
	}
	$img_path0=$_POST['h_destination0'];
	if ($img_path0=='') {
		$img_path0=$_POST['h_img_path0'];
		$img_path0=str_replace(IMG_URL_PATH,'',$img_path0);
	}
	$img_path1=$_POST['h_destination1'];
	if ($img_path1=='') {
		$img_path1=$_POST['h_img_path1'];
		$img_path1=str_replace(IMG_URL_PATH,'',$img_path1);
	}
	$img_path2=$_POST['h_destination2'];
	if ($img_path2=='') {
		$img_path2=$_POST['h_img_path2'];
		$img_path2=str_replace(IMG_URL_PATH,'',$img_path2);
	}
	$img_path3=$_POST['h_destination3'];
	if ($img_path3=='') {
		$img_path3=$_POST['h_img_path3'];
		$img_path3=str_replace(IMG_URL_PATH,'',$img_path3);
	}

    $i_expired_date_from= $_POST['i_expired_date_from'];
    if($i_expired_date_from==''){
        $i_expired_date_from=$sysdate;
    }
    $i_expired_date_to= $_POST['i_expired_date_to'];
    if($i_expired_date_to==''){
        $i_expired_date_to='2030-12-31';
    }

    $db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
    if(!$db){
        die("connot connect:" . mysql_error());
    }

    $dns = mysql_select_db(DB_NAME,$db);

    if(!$dns){
        die("connot use db:" . mysql_error());
    }

    mysql_set_charset('utf8');


    $logstr = "$systime $ip INFO：▼提携先名存在チェック開始 \r\n";
    error_log($logstr,3,'../log/gen.log');
    //提携先名存在チェック
    $ret=searchBrandInfoForUpdate($i_brand_name,$u_id);
    if($ret=='1'){

        db_disConn($result, $link);
        $logstr = "$systime ERR：提携先名存在チェックエラー！ \r\n";
        $logstr .= "$systime $ip INFO：▲提携先名存在チェック異常終了 \r\n";
        error_log($logstr,3,'../log/gen.log');

        $err_cd_list[]="02";
        $_SESSION['err_cd_list']=$err_cd_list;
        $url= URL_PATH . "err.php";
        redirect($url);
    }
    $logstr = "$systime $ip INFO：▲提携先名存在チェック正常終了！！ \r\n";
    error_log($logstr,3,'../log/gen.log');

    $logstr = "$systime $ip INFO：▼提携先管理者情報更新開始 \r\n";
    error_log($logstr,3,'../log/gen.log');

    $sql = sprintf("update den_admin set pwd='%s' where u_id='%s'",$i_password,$i_login_id);

    $logstr = "$systime $ip INFO：提携先管理者情報更新 UPDATE SQL文： ".$sql."\r\n";
    error_log($logstr,3,'../log/gen.log');

    $result = mysql_query($sql,$db);
    if(!$result){

        db_disConn($result, $link);
        $logstr = "$systime ERR：提携先管理者情報DB更新異常！ \r\n";
        $logstr .= "$systime $ip INFO：▲提携先管理者情報更新異常終了 \r\n";
        error_log($logstr,3,'../log/gen.log');

        $err_cd_list[]="01";
        $_SESSION['err_cd_list']=$err_cd_list;
        $url= URL_PATH . "err.php";
        redirect($url);
    }
    $logstr = "$systime $ip INFO：▲提携先管理者情報更新正常終了！！ \r\n";
    error_log($logstr,3,'../log/gen.log');

    $logstr = "$systime $ip INFO：▼提携先情報更新開始 \r\n";
    error_log($logstr,3,'../log/gen.log');
    $sql = sprintf("update brand set
                            cat_id='%s',
                            property_id='%s',
                            brand_name='%s',
                            brand_logo='%s',
                            brand_image='%s',
                            brand_image2='%s',
                            brand_image3='%s',
                            brand_desc='%s',
                            sort_order=%d,
                            password='%s',
                            discount_rate='%s',
                            charge_day='%s',
                            rate=%d,
                            ec_url='%s',
                            discount_out_contents='%s',
                            status=%d,
                            expired_date_from=%d,
                            expired_date_to=%d,
                            update_time=%d
                            where brand_id=%d",
            $i_category_id,
            $i_prop_id,
            $i_brand_name,
						$img_path0,
						$img_path1,
						$img_path2,
						$img_path3,
            $i_content,
            $i_sort_order,
            $i_password,
            $i_discount_rate,
            $i_charge_day,
						$i_rate,
						$i_ec_url,
            $i_content2,
            $i_status,
            strtotime($i_expired_date_from),
            strtotime($i_expired_date_to),
            strtotime($systime),
            $u_id
    );
    $logstr = "$systime $ip INFO：提携先情報更新 UPDATE SQL文： ".$sql."\r\n";
    error_log($logstr,3,'../log/gen.log');

    $result = mysql_query($sql,$db);
    if(!$result){
        db_disConn($result, $link);
        $logstr = "$systime ERR：提携先情報DB更新異常！ \r\n";
        $logstr .= "$systime $ip INFO：▲提携先情報更新異常終了 \r\n";
        error_log($logstr,3,'../log/gen.log');

        $err_cd_list[]="01";
        $_SESSION['err_cd_list']=$err_cd_list;
        $url= URL_PATH . "err.php";
        redirect($url);
    }

    $logstr = "$systime $ip INFO：▲提携先情報更新正常終了！！ \r\n";
    error_log($logstr,3,'../log/gen.log');

	$logstr = "$systime $ip INFO：▼クーポンコード情報削除処理開始 \r\n";
	error_log($logstr,3,'../log/gen.log');
	$sql = sprintf("delete from den_kp_code where brand_id=%d",$u_id);
	$logstr = "$systime $ip INFO：クーポンコード情報削除 DELETE SQL文： ".$sql."\r\n";
	error_log($logstr,3,'../log/gen.log');

	$result = mysql_query($sql,$db);
	if(!$result){
		db_disConn($result, $link);
		$logstr = "$systime ERR：クーポンコード情報DB削除異常！ \r\n";
		$logstr .= "$systime $ip INFO：▲クーポンコード情報削除異常終了 \r\n";
		error_log($logstr,3,'../log/gen.log');

		$err_cd_list[]="01";
		$_SESSION['err_cd_list']=$err_cd_list;
		$url= URL_PATH . "err.php";
		redirect($url);
	}
	$logstr = "$systime $ip INFO：▲クーポンコード情報削除処理正常終了！！ \r\n";
	error_log($logstr,3,'../log/gen.log');

	$i=1;
	while (($i_rank_code[$i]!=''||$i_kp_code[$i]!=''||$i_bl_code[$i]!='')&&$u_id!=''){
		$logstr = "$systime $ip INFO：▼クーポンコード情報登録開始 \r\n";
		error_log($logstr,3,'../log/gen.log');
		$sql = sprintf("insert into den_kp_code (brand_id,rank_id,kp_code,bl_code) values (%d,'%s','%s','%s')",$u_id,$i_rank_code[$i],$i_kp_code[$i],$i_bl_code[$i]);
		$logstr = "$systime $ip INFO：クーポンコード情報登録 INSERT SQL文： ".$sql."\r\n";
		error_log($logstr,3,'../log/gen.log');

		$result = mysql_query($sql,$db);
		if(!$result){
			db_disConn($result, $link);
			$logstr = "$systime ERR：クーポンコード情報DB登録異常！ \r\n";
			$logstr .= "$systime $ip INFO：▲クーポンコード情報登録異常終了 \r\n";
			error_log($logstr,3,'../log/gen.log');

			$err_cd_list[]="01";
			$_SESSION['err_cd_list']=$err_cd_list;
			$url= URL_PATH . "err.php";
			redirect($url);
		}
		$logstr = "$systime $ip INFO：▲クーポンコード情報登録正常終了！！ \r\n";
		error_log($logstr,3,'../log/gen.log');
		$i++;
	}
	
	
	$logstr = "$systime $ip INFO：▼会員種別ごと並び順情報削除処理開始 \r\n";
	error_log($logstr,3,'../log/gen.log');
	$sql = sprintf("delete from den_rank_sort where brand_id=%d",$u_id);
	$logstr = "$systime $ip INFO：会員種別ごと並び順情報削除 DELETE SQL文： ".$sql."\r\n";
	error_log($logstr,3,'../log/gen.log');

	$result = mysql_query($sql,$db);
	if(!$result){
		db_disConn($result, $link);
		$logstr = "$systime ERR：会員種別ごと並び順情報DB削除異常！ \r\n";
		$logstr .= "$systime $ip INFO：▲会員種別ごと並び順情報削除異常終了 \r\n";
		error_log($logstr,3,'../log/gen.log');

		$err_cd_list[]="01";
		$_SESSION['err_cd_list']=$err_cd_list;
		$url= URL_PATH . "err.php";
		redirect($url);
	}
	$logstr = "$systime $ip INFO：▲会員種別ごと並び順情報削除処理正常終了！！ \r\n";
	error_log($logstr,3,'../log/gen.log');

	$i=1;
	while (($i_rank_code_sort[$i]!=''||$i_sort_code[$i]!='')&&$u_id!=''){
		$logstr = "$systime $ip INFO：▼会員種別ごと並び順情報登録開始 \r\n";
		error_log($logstr,3,'../log/gen.log');
		$sql = sprintf("insert into den_rank_sort (brand_id,rank_id,sort_code) values (%d,'%s','%s')",$u_id,$i_rank_code_sort[$i],$i_sort_code[$i]);
		$logstr = "$systime $ip INFO：会員種別ごと並び順情報登録 INSERT SQL文： ".$sql."\r\n";
		error_log($logstr,3,'../log/gen.log');

		$result = mysql_query($sql,$db);
		if(!$result){
			db_disConn($result, $link);
			$logstr = "$systime ERR：会員種別ごと並び順情報DB登録異常！ \r\n";
			$logstr .= "$systime $ip INFO：▲会員種別ごと並び順情報登録異常終了 \r\n";
			error_log($logstr,3,'../log/gen.log');

			$err_cd_list[]="01";
			$_SESSION['err_cd_list']=$err_cd_list;
			$url= URL_PATH . "err.php";
			redirect($url);
		}
		$logstr = "$systime $ip INFO：▲会員種別ごと並び順情報登録正常終了！！ \r\n";
		error_log($logstr,3,'../log/gen.log');
		$i++;
	}

	mysql_close($db);
	$url= URL_PATH . "m_brand.php?action=search&role=".$role."&l_id=".$login_user;
    redirect($url);
}

//削除実行
if ($action=='deletesubmit'){

    $i_login_id=$_POST['h_login_id'];
    $u_id=$_POST['u_id'];

    $db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
    if(!$db){
        die("connot connect:" . mysql_error());
    }

    $dns = mysql_select_db(DB_NAME,$db);

    if(!$dns){
        die("connot use db:" . mysql_error());
    }

    mysql_set_charset('utf8');


    $logstr = "$systime $ip INFO：▼提携先管理者情報削除更新開始 \r\n";
    error_log($logstr,3,'../log/gen.log');

    $sql = sprintf("update den_admin set status=1 where u_id='%s'",$i_login_id);

    $logstr = "$systime $ip INFO：提携先管理者情報削除更新 UPDATE SQL文： ".$sql."\r\n";
    error_log($logstr,3,'../log/gen.log');

    $result = mysql_query($sql,$db);
    if(!$result){

        db_disConn($result, $link);
        $logstr = "$systime ERR：提携先管理者情報DB削除更新異常！ \r\n";
        $logstr .= "$systime $ip INFO：▲提携先管理者情報削除更新異常終了 \r\n";
        error_log($logstr,3,'../log/gen.log');

        $err_cd_list[]="01";
        $_SESSION['err_cd_list']=$err_cd_list;
        $url= URL_PATH . "err.php";
        redirect($url);
    }
    $logstr = "$systime $ip INFO：▲提携先管理者情報削除更新正常終了！！ \r\n";
    error_log($logstr,3,'../log/gen.log');

    $logstr = "$systime $ip INFO：▼提携先情報削除更新開始 \r\n";
    error_log($logstr,3,'../log/gen.log');
    $sql = sprintf("update brand set del_flg=1 where brand_id=%d",$u_id);
    $logstr = "$systime $ip INFO：提携先情報削除更新 UPDATE SQL文： ".$sql."\r\n";
    error_log($logstr,3,'../log/gen.log');

    $result = mysql_query($sql,$db);
    if(!$result){
        db_disConn($result, $link);
        $logstr = "$systime ERR：提携先情報DB削除更新異常！ \r\n";
        $logstr .= "$systime $ip INFO：▲提携先情報削除更新異常終了 \r\n";
        error_log($logstr,3,'../log/gen.log');

        $err_cd_list[]="01";
        $_SESSION['err_cd_list']=$err_cd_list;
        $url= URL_PATH . "err.php";
        redirect($url);
    }

    mysql_close($db);
    $logstr = "$systime $ip INFO：▲提携先情報削除更新正常終了！！ \r\n";
    error_log($logstr,3,'../log/gen.log');


	$url= URL_PATH . "m_brand.php?action=search&role=".$role."&l_id=".$login_user;
    redirect($url);
}
?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $sub_title; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache,must-revalidate">
<meta http-equiv="expires" content="Wed, 23 Aug 2006 12:40:27 UTC" />
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">

<link href="../css/common.css" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../js/multiselectSrc/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="../js/assets/style.css" />
<link rel="stylesheet" type="text/css" href="../js/assets/prettify.css" />
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>

<script type="text/javascript" src="../js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/assets/prettify.js"></script>
<script type="text/javascript" src="../js/multiselectSrc/jquery.multiselect.js"></script>
<script type="text/javascript">
$(function(){
	$("#i_prop_id").multiselect({
		noneSelectedText: "選択してください",
		checkAllText: "全て選択",
		uncheckAllText: '全て選択解除',
		selectedList:3,
		selectedText: '# 個選択',
		height:'auto',
		minWidth:500
	});
	$("#i_category_id").multiselect({
		noneSelectedText: "選択してください",
		checkAllText: "全て選択",
		uncheckAllText: '全て選択解除',
		selectedList:3,
		selectedText: '# 個選択',
		height:'auto',
		minWidth:500
	});
});
</script>
</head>
<?php
if ($is_disabled=="disabled='disabled'"){
	echo"
	<body onload='setDisableMultiselect();'>
	";
}else{
	echo"
	<body>
	";
}
?>
<div class='main'>
<div class='subtitle'><?php echo $sub_title; ?></div>
<form enctype='multipart/form-data' method='post' name='upform'>
<div class='input-area'>
    <label class='w150'>ログインID</label>
    <?php
    if($action=='update'||$action=='delete'||$update_flg=='1'||$role!='1'){
        echo "<label class='w200'>".$i_login_id."</label>";
    }else{
        echo"
            <input type='text' class='w200' name='i_login_id' ".$is_disabled." id='i_login_id' value='".$i_login_id."'/>
            <span class='input-span ml20 fl'>例：brand_001</span>
        ";
    }
    ?>
    <div style='clear:both;'></div>
    <label class='w150'>パスワード</label>
        <input type='password' class='w200' name='i_pwd' id='i_pwd' placeholder='<?php if($havePlaceholder=='YES') echo'';?>' value='<?php echo $i_password;?>' <?php echo $is_disabled;?>/>
        <span class='input-span ml20 fl'>例：brand_pwd_001</span>
    <div style='clear:both;'></div>
    <label class='w150'>パスワード確認</label>
        <input type='password' class='w200' name='i_pwd_r' id='i_pwd_r' placeholder='<?php if($havePlaceholder=='YES') echo'';?>'  value='<?php echo $i_password;?>' <?php echo $is_disabled;?>/>
        <span class='input-span ml20 fl'>例：brand_pwd_001</span>
    <div style='clear:both;'></div>
    <label class='w150'>ステータス</label>
    <select name='i_status' id='i_status' class='re_select' <?php echo $is_disabled;?>>
        <option value='1'<?php if ($i_status=='1') {echo 'selected';}?>>アクティブ</option>
        <option value='0'<?php if ($i_status=='0') {echo 'selected';}?>>非アクティブ</option>
    </select>
    <div style='clear:both;'></div>
    <label class='w150'>有効期間</label>
        <input type='text' name='i_expired_date_from' id='i_expired_date_from' class='w100' placeholder='<?php if($havePlaceholder=='YES') echo'';?>' value='<?php echo $i_expired_date_from;?>' <?php echo $is_disabled;?>/>
        <label class='tcenter mg0'>〜</label>
        <input type='text' name='i_expired_date_to' id='i_expired_date_to' class='w100' placeholder='<?php if($havePlaceholder=='YES') echo'';?>' value='<?php echo $i_expired_date_to;?>' <?php echo $is_disabled;?>/>
        <span class='input-span ml20 fl'>例：<?php echo $sysdate.' 〜 2024-12-31';?></span>
    <div style='clear:both;'></div>
    <label class='w150'>対象会員種別</label>
		<select name='i_prop_id' id='i_prop_id' multiple="multiple" size="5" <?php if($role!='1'){echo 'disabled';}?>>
			<?php
				$i=0;
				while($arr_list_row=mysql_fetch_array($result_list_prop)){
					if($arr_list_row[id]==$arr_prop_id[$i]){
						echo"<option value=".$arr_list_row[id]." selected >".$arr_list_row[rank_name]. "</option>";
						$i++;
					}else{
						echo"<option value=".$arr_list_row[id]." >".$arr_list_row[rank_name]. "</option>";
					}
				}
			?>
		</select>
    <div style='clear:both;'></div>
	<label class='w150'>提携先名称</label>
	<?php
	if($i_login_id=='bigcamera'){
		echo"
		<input type='text' name='i_brand_name' id='i_brand_name' class='w500' value='".htmlspecialchars_decode($i_brand_name)."' disabled='disabled'/>
		";
	}else{
		echo"
		<input type='text' name='i_brand_name' id='i_brand_name' class='w500' value='".htmlspecialchars_decode($i_brand_name)."' ".$is_disabled."/>
		";
	}
	?>
	<div style='clear:both;'></div>
	<label class='w150'>カテゴリ</label>
		<select name='i_category_id' id='i_category_id' multiple="multiple" size="5" <?php echo $is_disabled;?>>
				<?php
					$i=0;
		        while($arr_list_row=mysql_fetch_array($result_list_cat)){
		        	if($arr_list_row[cat_id]==$arr_category_id[$i]){
		        		echo"<option value=".$arr_list_row[cat_id]." selected >".$arr_list_row[cat_name]. "</option>";
						$i++;
		        	}else{
		        		echo"<option value=".$arr_list_row[cat_id]." >".$arr_list_row[cat_name]. "</option>";
		        	}
		        }
        		?>
		</select>
	<div style='clear:both;'></div>
	<label class='w150'>割引率(表示用)</label>
		<input type='text' class='w150' name='i_discount_rate' id='i_discount_rate' value='<?php echo $i_discount_rate;?>' <?php echo $is_disabled;?>/><span class='input-span mr20'>※5%　OFF、配送料　無料　など半角スペース区切ってフリーテキスト</span>
	<div style='clear:both;'></div>
	<label class='w150'>割引率(計算用)</label>
		<input type='text' class='w150' name='i_rate' id='i_rate' value='<?php echo $i_rate;?>' <?php echo $is_disabled;?>/><span class='input-span mr20'>%　半角数字で入力してください</span>
	<div style='clear:both;'></div>
	<label class='w150'>ECサイトURL</label>
		<input type='text' class='w500' name='i_ec_url' id='i_ec_url' value='<?php echo $i_ec_url;?>' <?php echo $is_disabled;?>/><span class='input-span mr20'>※ECの場合のみ入力</span>
	<div style='clear:both;'></div>

    <label class='w150'>並び順番</label>
    <input type='text' class='w100' name='i_sort_order' id='i_sort_order' placeholder='<?php if($havePlaceholder=='YES') echo'50';?>'  value='<?php echo $i_sort_order;?>' <?php echo $is_disabled;?>/><span class='input-span mr20'>※指定しない場合，登録先後順で並び</span>
    <div style='clear:both;'></div>
	<label class='w150'>締め日</label>
		<input type='text' class='w150' name='i_charge_day' id='i_charge_day' value='<?php echo $i_charge_day;?>' <?php echo $is_disabled;?>/><span class='input-span mr20'>日</span>
	<div style='clear:both;'></div>
	<label class='w150'>詳細</label>
	<textarea name='i_content' id='i_content' placeholder='<?php if($havePlaceholder=='YES') echo'詳細内容を入力してください';?>' <?php echo $is_disabled;?>><?php echo htmlspecialchars_decode($i_content);?></textarea>
	<div style='clear:both;'></div>
	<label class='w150'>セール除外品</label>
	<textarea name='i_content2' id='i_content2' placeholder='<?php if($havePlaceholder=='YES') echo'セール除外品内容を入力してください';?>' <?php echo $is_disabled;?>><?php echo htmlspecialchars_decode($i_content2);?></textarea>
	<div style='clear:both;'></div>
    <label class='w150'>店舗Logo</label>
    <?php
    if($action=='confirm'){
        if($update_flg=='1'){
            if($_FILES["upfile0"]["tmp_name"]!=''){
                echo "<img src=\"".$show_dest0."\" width='160px' height='40px' class='mr20'>";
            }else{
                echo "<img src=\"".$h_img_path0."\" width='160px' height='40px' class='mr20'>";
            }
        }else{
            if($_FILES["upfile0"]["tmp_name"]!=''){
                echo "<img src=\"".$show_dest0."\" width='160px' height='40px' class='mr20'>";
            }else{
                echo "<span class='input-span'>添付画像なし</span>";
            }
        }
    }
    elseif($action=='update'||$action=='delete'||$action=='edit'){
        echo "<img src=\"".$h_img_path0."\" width='160px' height='40px' class='mr20'>
              <input name='upfile0' id='upfile0' type='file'>
        ";
    }
    else{
        echo"
            <input name='upfile0' id='upfile0' type='file'>
		";
    }
    ?>
    <div style='clear:both;'></div>
	<label class='w150'>店舗画像</label>
    <?php
    if($action=='confirm'){
        if($update_flg=='1'){
            if($_FILES["upfile1"]["tmp_name"]!=''){
                echo "<img src=\"".$show_dest1."\" width='160px' height='113px' class='mr20'>";
            }else{
                echo "<img src=\"".$h_img_path1."\" width='160px' height='113px' class='mr20'>";
            }
            if($_FILES["upfile2"]["tmp_name"]!=''){
                echo "<img src=\"".$show_dest2."\" width='160px' height='113px' class='mr20'>";
            }else{
                echo "<img src=\"".$h_img_path2."\" width='160px' height='113px' class='mr20'>";
            }
            if($_FILES["upfile3"]["tmp_name"]!=''){
                echo "<img src=\"".$show_dest3."\" width='160px' height='113px' class='mr20'>";
            }else{
                echo "<img src=\"".$h_img_path3."\" width='160px' height='113px' class='mr20'>";
            }
        }else{
            if($_FILES["upfile1"]["tmp_name"]!=''){
                echo "<img src=\"".$show_dest1."\" width='160px' height='113px' class='mr20'>";
            }
            if($_FILES["upfile2"]["tmp_name"]!=''){
                echo "<img src=\"".$show_dest2."\" width='160px' height='113px' class='mr20'>";
            }
            if($_FILES["upfile3"]["tmp_name"]!=''){
                echo "<img src=\"".$show_dest3."\" width='160px' height='113px' class='mr20'>";
            }
            if(($_FILES["upfile0"]["tmp_name"]=='')&&($_FILES["upfile1"]["tmp_name"]=='')&&($_FILES["upfile2"]["tmp_name"]=='')&&($_FILES["upfile3"]["tmp_name"]=='')){
                echo "<span class='input-span'>添付画像なし</span>";
            }
        }
    }
    elseif($action=='update'||$action=='delete'||$action=='edit'){
        echo "<img src=\"".$h_img_path1."\" width='160px' height='113px' class='mr20'>
              <input name='upfile1' id='upfile1' type='file'></br>
        ";
        echo "<img src=\"".$h_img_path2."\" width='160px' height='113px' class='mr20 ml190'>
              <input name='upfile2' id='upfile2' type='file'></br>
        ";
        echo "<img src=\"".$h_img_path3."\" width='160px' height='113px' class='mr20 ml190'>
              <input name='upfile3' id='upfile3' type='file'></br>
        ";
    }
    else{
        echo"
			<input name='upfile1' id='upfile1' type='file'></br>
			<input name='upfile2' id='upfile2' type='file'></br>
			<input name='upfile3' id='upfile3' type='file'>
		";
    }
    ?>
    <div style='clear:both;'></div>
    <label class='w500 mt3'>対象会員種別とクーポンコード設定</label>
    <div style='clear:both;'></div>
	<?php
		if($action=='confirm'||$action=='delete'){
			$i=1;
			while ($i_rank_code[$i]!=''||$i_kp_code[$i]!=''||$i_bl_code[$i]!=''){
				echo"
					<div name='kp_code_row' id='kp_code_row'>
						<label class='w150'>会員種別コード</label>
						<input type='text' class='w150' name='i_rank_code".$i."' id='i_rank_code".$i."' ".$is_disabled." value=".$i_rank_code[$i].">
						<label class='w150'>クーポンコード</label>
						<input type='text' class='w150' name='i_kp_code".$i."' id='i_kp_code".$i."' ".$is_disabled." value=".$i_kp_code[$i].">
						<label class='w150'>会員種別識別番号</label>
						<input type='text' class='w100' name='i_bl_code".$i."' id='i_bl_code".$i."' ".$is_disabled." value=".$i_bl_code[$i].">
						<div style='clear:both;'></div>
					</div>
				";
				$i++;
			}
		}elseif($action=='update'||$action=='edit'){
			$i=1;
			while ($i_rank_code[$i]!=''||$i_kp_code[$i]!=''||$i_bl_code[$i]!=''){
				echo"
					<div name='kp_code_row_edit' id='kp_code_row_edit'>
						<label class='w150'>会員種別コード</label>
						<input type='text' class='w150' name='i_rank_code".$i."' id='i_rank_code".$i."' ".$is_disabled." value=".$i_rank_code[$i].">
						<label class='w150'>クーポンコード</label>
						<input type='text' class='w150' name='i_kp_code".$i."' id='i_kp_code".$i."' ".$is_disabled." value=".$i_kp_code[$i].">
						<label class='w150'>会員種別識別番号</label>
						<input type='text' class='w100' name='i_bl_code".$i."' id='i_bl_code".$i."' ".$is_disabled." value=".$i_bl_code[$i].">
						<div style='clear:both;'></div>
					</div>
				";
				$i++;
			}
			echo"
				<div name='kp_code_row' id='kp_code_row'>
					<label class='w150'>会員種別コード</label>
					<input type='text' class='w150' name='i_rank_code".$i."' id='i_rank_code".$i."' ". $is_disabled." />
					<label class='w150'>クーポンコード</label>
					<input type='text' class='w150' name='i_kp_code".$i."' id='i_kp_code".$i."' ".$is_disabled." />
					<label class='w150'>会員種別識別番号</label>
					<input type='text' class='w100' name='i_bl_code".$i."' id='i_bl_code".$i."' ".$is_disabled." />
					<input type='button' onclick='addKpCodeRow()' id='btn".$i."' value='追加'/>
					<div style='clear:both;'></div>
				</div>
			";
		}
		else{
			echo"
				<div name='kp_code_row' id='kp_code_row'>
					<label class='w150'>会員種別コード</label>
					<input type='text' class='w150' name='i_rank_code1' id='i_rank_code1' ". $is_disabled." />
					<label class='w150'>クーポンコード</label>
					<input type='text' class='w150' name='i_kp_code1' id='i_kp_code1' ".$is_disabled." />
					<label class='w150'>会員種別識別番号</label>
					<input type='text' class='w100' name='i_bl_code1' id='i_bl_code1' ".$is_disabled." />
					<input type='button' onclick='addKpCodeRow()' id='btn1' value='追加'/>
					<div style='clear:both;'></div>
				</div>
			";
		}
	?>
	<div name='copy_row' id='copy_row' style='display:none;'>
		<label class='w150'>会員種別コード</label>
		<input type='text' id='i_rank_code' name='i_rank_code' class='w150'/>
		<label class='w150'>クーポンコード</label>
		<input type='text' id='i_kp_code' name='i_kp_code' class='w150'/>
		<label class='w150'>会員種別識別番号</label>
		<input type='text' id='i_bl_code' name='i_bl_code' class='w100'/>
		<input type='button' onclick='addKpCodeRow()' id='btn' value='追加'/>
		<div style='clear:both;'></div>
	</div>
	<label class='w500 mt3'>会員種別ごと並び順</label>
    <div style='clear:both;'></div>
	<?php
		if($action=='confirm'||$action=='delete'){
			$i=1;
			while ($i_rank_code_sort[$i]!=''||$i_sort_code[$i]!=''){
				echo"
					<div name='sort_code_row' id='sort_code_row'>
						<label class='w150'>会員種別コード</label>
						<input type='text' class='w150' name='i_rank_code_sort".$i."' id='i_rank_code_sort".$i."' ".$is_disabled." value=".$i_rank_code_sort[$i].">
						<label class='w150'>並び順</label>
						<input type='text' class='w150' name='i_sort_code".$i."' id='i_sort_code".$i."' ".$is_disabled." value=".$i_sort_code[$i].">
						<div style='clear:both;'></div>
					</div>
				";
				$i++;
			}
		}elseif($action=='update'||$action=='edit'){
			$i=1;
			while ($i_rank_code_sort[$i]!=''||$i_sort_code[$i]!=''){
				echo"
					<div name='kp_code_row_edit' id='kp_code_row_edit'>
						<label class='w150'>会員種別コード</label>
						<input type='text' class='w150' name='i_rank_code_sort".$i."' id='i_rank_code_sort".$i."' ".$is_disabled." value=".$i_rank_code_sort[$i].">
						<label class='w150'>並び順</label>
						<input type='text' class='w150' name='i_sort_code".$i."' id='i_sort_code".$i."' ".$is_disabled." value=".$i_sort_code[$i].">
						<div style='clear:both;'></div>
					</div>
				";
				$i++;
			}
			echo"
				<div name='sort_code_row' id='sort_code_row'>
					<label class='w150'>会員種別コード</label>
					<input type='text' class='w150' name='i_rank_code_sort".$i."' id='i_rank_code_sort".$i."' ". $is_disabled." />
					<label class='w150'>並び順</label>
					<input type='text' class='w150' name='i_sort_code".$i."' id='i_sort_code".$i."' ".$is_disabled." />
					<input type='button' onclick='addSortCodeRow()' id='btn".$i."' value='追加'/>
					<div style='clear:both;'></div>
				</div>
			";
		}
		else{
			echo"
				<div name='sort_code_row' id='sort_code_row'>
					<label class='w150'>会員種別コード</label>
					<input type='text' class='w150' name='i_rank_code_sort1' id='i_rank_code_sort1' ". $is_disabled." />
					<label class='w150'>並び順</label>
					<input type='text' class='w150' name='i_sort_code1' id='i_sort_code1' ".$is_disabled." />
					<input type='button' onclick='addSortCodeRow()' id='btn_sort1' value='追加'/>
					<div style='clear:both;'></div>
				</div>
			";
		}
	?>
	<div name='sort_copy_row' id='sort_copy_row' style='display:none;'>
		<label class='w150'>会員種別コード</label>
		<input type='text' id='i_rank_code_sort' name='i_rank_code_sort' class='w150'/>
		<label class='w150'>並び順</label>
		<input type='text' id='i_sort_code' name='i_sort_code' class='w150'/>
		<input type='button' onclick='addSortCodeRow()' id='btn_sort' value='追加'/>
		<div style='clear:both;'></div>
	</div>
    <input type="hidden" name='h_destination0' value="<?php echo $destination0;?>"/>
    <input type="hidden" name='h_destination1' value="<?php echo $destination1;?>"/>
    <input type="hidden" name='h_destination2' value="<?php echo $destination2;?>"/>
    <input type="hidden" name='h_destination3' value="<?php echo $destination3;?>"/>
    <input type="hidden" name='u_id' value="<?php echo $u_id;?>" />
    <input type="hidden" name='update_flg' value="<?php echo $update_flg;?>"/>
    <input type="hidden" name='h_login_id' value="<?php echo $i_login_id;?>"/>
    <input type="hidden" name='h_img_path0' value="<?php echo $h_img_path0;?>"/>
    <input type="hidden" name='h_img_path1' value="<?php echo $h_img_path1;?>"/>
    <input type="hidden" name='h_img_path2' value="<?php echo $h_img_path2;?>"/>
    <input type="hidden" name='h_img_path3' value="<?php echo $h_img_path3;?>"/>
    <input type="hidden" name='h_prop_id' value="<?php echo $i_prop_id;?>"/>
    <input type="hidden" name='h_category_id' value="<?php echo $i_category_id;?>"/>
    <?php
    if($action=='update'||$action=='edit'){
		echo"<input type='hidden' name='h_num_cnt' id='h_num_cnt' value='".$h_num_cnt."'/>";
		echo"<input type='hidden' name='h_num_cnt_sort' id='h_num_cnt_sort' value='".$h_num_cnt_sort."'/>";
    }else{
    		echo"<input type='hidden' name='h_num_cnt' id='h_num_cnt' value='2'/>";
		echo"<input type='hidden' name='h_num_cnt_sort' id='h_num_cnt_sort' value='2'/>";
    }
    ?>
	<?php
	if($action=='confirm'){
        if($update_flg=='1'){
            echo "
                <input type='button' class='buttonS bGreen ml190 w200 mt40' value='訂正' onclick='editMode();'/>
                <input type='button' class='buttonS bGreen ml100 w200 mt40' value='更新' onclick='updateSubmit();'/>
            ";
        }else{
            echo "
                <input type='button' class='buttonS bGreen ml190 w200 mt40' value='訂正' onclick='editMode();'/>
                <input type='button' class='buttonS bGreen ml100 w200 mt40' value='送信' onclick='confirmSubmit();'/>
            ";
        }
	}
    elseif($action=='delete'){
        echo "<input type='button' class='buttonS bGreen ml190 w200 mt40' value='削除する' onclick='deleteSubmit();'/>";
    }
    else{
		echo "<input type='button' class='buttonS bGreen ml190 w200 mt40' value='確認画面へ' onclick='moveConfirm();'/>";
	}
	?>
</div>

<script type="text/javascript" language="javascript">
	function moveConfirm() {
		//提携先名称
		 if(document.upform.i_brand_name.value == ""){
		  alert("提携先名称を入力してください。");
		  document.upform.i_brand_name.focus();
		  return false;
		 }
		 //カテゴリ
		 if(document.upform.i_category_id.value == ""){
		  alert("カテゴリを選択してください。");
		  document.upform.i_category_id.focus();
		  return false;
		 }
		 //割引率
		 if(document.upform.i_discount_rate.value == ""){
		  alert("割引率（表示用）を入力してください。");
		  document.upform.i_discount_rate.focus();
		  return false;
		 }
		 //締め日
		 if(document.upform.i_charge_day.value == ""){
		  alert("締め日を入力してください。");
		  document.upform.i_charge_day.focus();
		  return false;
		 }
		//割引率
		if(document.upform.i_rate.value == ""){
			alert("割引率（計算用）を入力してください。");
			document.upform.i_rate.focus();
			return false;
		}
		 //詳細
		 if(document.upform.i_content.value == ""){
		  alert("詳細内容を入力してください。");
		  document.upform.i_content.focus();
		  return false;
		 }
		 //セール除外品
		 if(document.upform.i_content2.value == ""){
		  alert("セール除外品を入力してください。");
		  document.upform.i_content2.focus();
		  return false;
		 }
        //ログインID
        if(document.upform.update_flg.value != "1"){
            if(document.upform.i_login_id.value == ""){
                alert("ログインIDを入力してください。");
                document.upform.i_login_id.focus();
                return false;
            }
        }

        //パスワード
        if(document.upform.i_pwd.value == ""){
            alert("パスワードを入力してください。");
            document.upform.i_pwd.focus();
            return false;
        }
        //パスワード確認
        if(document.upform.i_pwd_r.value != document.upform.i_pwd.value){
            alert("パスワードを確認してください。");
            document.upform.i_pwd_r.focus();
            return false;
        }
        if(document.upform.update_flg.value != "1"){
            //店舗ロゴ
            if(document.upform.upfile0.value == ""){
                alert("店舗ロゴを指定してください。");
                document.upform.upfile0.focus();
                return false;
            }
            //店舗画像１
            if(document.upform.upfile1.value == ""){
                alert("店舗画像１を指定してください。");
                document.upform.upfile1.focus();
                return false;
            }
            //店舗画像２
            if(document.upform.upfile2.value == ""){
                alert("店舗画像２を指定してください。");
                document.upform.upfile2.focus();
                return false;
            }
            //店舗画像３
            if(document.upform.upfile3.value == ""){
                alert("店舗画像３を指定してください。");
                document.upform.upfile3.focus();
                return false;
            }
        }

		var val_prop = $("#i_prop_id").multiselect("getChecked").map(function(){return this.value;}).get();
		var val_cat = $("#i_category_id").multiselect("getChecked").map(function(){return this.value;}).get();

		document.upform.h_prop_id.value=val_prop;
		document.upform.h_category_id.value=val_cat;
		document.getElementById('i_brand_name').disabled=false;

		//submit
		document.upform.action="?action=confirm";
		document.upform.submit();
	}
	function setDisableMultiselect(){
		var state = false;
		var $widget_prop = $("#i_prop_id").multiselect();
		var $widget_cat = $("#i_category_id").multiselect();
		state = !state;
		$widget_prop.multiselect(state ? 'disable' : 'enable');
		$widget_cat.multiselect(state ? 'disable' : 'enable');
	}
	function confirmSubmit() {
		document.getElementById('i_brand_name').disabled=false;
		document.getElementById('i_category_id').disabled=false;
		//		document.getElementById('i_prop_id').disabled=false;
		var $widget = $("select").multiselect();
		$widget.multiselect('enable');
		document.getElementById('i_login_id').disabled=false;
		document.getElementById('i_pwd').disabled=false;
		document.getElementById('i_pwd_r').disabled=false;
		document.getElementById('i_discount_rate').disabled=false;
		document.getElementById('i_charge_day').disabled=false;
		document.getElementById('i_rate').disabled=false;
		document.getElementById('i_bl_code').disabled=false;
		document.getElementById('i_ec_url').disabled=false;
		document.getElementById('i_sort_order').disabled=false;
		document.getElementById('i_status').disabled=false;
		document.getElementById('i_content').disabled=false;
		document.getElementById('i_content2').disabled=false;

		$('input').attr("disabled",false);

		document.upform.action="?action=insert";
		document.upform.submit();
	}
    function updateSubmit() {
			document.getElementById('i_brand_name').disabled=false;
			document.getElementById('i_category_id').disabled=false;
			//        document.getElementById('i_prop_id').disabled=false;
			var $widget = $("select").multiselect();
			$widget.multiselect('enable');
			document.getElementById('i_pwd').disabled=false;
			document.getElementById('i_pwd_r').disabled=false;
			document.getElementById('i_discount_rate').disabled=false;
			document.getElementById('i_charge_day').disabled=false;
			document.getElementById('i_rate').disabled=false;
			document.getElementById('i_bl_code').disabled=false;
			document.getElementById('i_ec_url').disabled=false;
			document.getElementById('i_sort_order').disabled=false;
			document.getElementById('i_status').disabled=false;
			document.getElementById('i_content').disabled=false;
			document.getElementById('i_content2').disabled=false;
		$('input').attr("disabled",false);

        document.upform.action="?action=updatesubmit";
        document.upform.submit();
    }
    function deleteSubmit() {
        document.upform.action="?action=deletesubmit";
        document.upform.submit();
    }

	function editMode(){
		document.getElementById('i_brand_name').disabled=false;
		document.getElementById('i_category_id').disabled=false;
		//        document.getElementById('i_prop_id').disabled=false;
		var $widget = $("select").multiselect();
		$widget.multiselect('enable');
		if(document.upform.update_flg.value != "1"){
				document.getElementById('i_login_id').disabled=false;
		}
		document.getElementById('i_pwd').disabled=false;
		document.getElementById('i_pwd_r').disabled=false;
		document.getElementById('i_discount_rate').disabled=false;
		document.getElementById('i_charge_day').disabled=false;
		document.getElementById('i_rate').disabled=false;
		document.getElementById('i_bl_code').disabled=false;
		document.getElementById('i_ec_url').disabled=false;
		document.getElementById('i_sort_order').disabled=false;
		document.getElementById('i_status').disabled=false;
		document.getElementById('i_content').disabled=false;
		document.getElementById('i_content2').disabled=false;
		$('input').attr("disabled",false);

		document.upform.action="?action=edit";
		document.upform.submit();
	}
	function addKpCodeRow(){
		var rownum=document.getElementById('h_num_cnt').value;
		var currentBtn = document.getElementById('btn'+(Number(rownum)-1));

		var thenew= document.createElement('div');
		thenew.innerHTML = document.getElementById('copy_row').innerHTML.replace(new RegExp("i_rank_code",'g'),"i_rank_code"+rownum);
		thenew.innerHTML = thenew.innerHTML.replace(new RegExp("i_kp_code",'g'),"i_kp_code"+rownum);
		thenew.innerHTML = thenew.innerHTML.replace(new RegExp("i_bl_code",'g'),"i_bl_code"+rownum);
		thenew.innerHTML = thenew.innerHTML.replace("btn","btn"+rownum);

		document.getElementById('kp_code_row').appendChild(thenew);

		currentBtn.style.display="none";

		document.getElementById('h_num_cnt').value=(Number(rownum)+1);
	}
	function addSortCodeRow(){
		var rownum=document.getElementById('h_num_cnt_sort').value;
		var currentBtn = document.getElementById('btn_sort'+(Number(rownum)-1));

		var thenew= document.createElement('div');
		thenew.innerHTML = document.getElementById('sort_copy_row').innerHTML.replace(new RegExp("i_rank_code_sort",'g'),"i_rank_code_sort"+rownum);
		thenew.innerHTML = thenew.innerHTML.replace(new RegExp("i_sort_code",'g'),"i_sort_code"+rownum);
		thenew.innerHTML = thenew.innerHTML.replace("btn_sort","btn_sort"+rownum);

		document.getElementById('sort_code_row').appendChild(thenew);

		currentBtn.style.display="none";

		document.getElementById('h_num_cnt_sort').value=(Number(rownum)+1);
	}
</script>
</form>
</div>
</body>
</html>