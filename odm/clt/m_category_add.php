<?php
	//include
	require '../util/include.php';

	$action = $_GET['action'];
	$sysdate=date('Y-m-d',time());
	$systime=date('Y-m-d H:i:s',time());
	$ip=get_real_ip();

	mysql_close($db);

	$sub_title='カテゴリ情報管理　- 新規追加 -';

	//form項目
	$i_category_name = '';
	$i_status ='';

	//insert
	if ($action=='insert'){
		$logstr = "$systime $ip INFO：▼カテゴリ情報登録開始 \r\n";
		error_log($logstr,3,'../log/gen.log');

		$i_category_name = $_POST['i_category_name'];
		$i_category_name = htmlspecialchars($i_category_name);
		$i_status = $_POST['i_status'];

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("insert into category (cat_name,is_show,insert_time) values (%s,%d,%d)",
				quote_smart($i_category_name,true),
				$i_status,
				strtotime($systime));
				
		$logstr = "$systime $ip INFO：カテゴリ情報登録 INSERT SQL文： ".$sql."\r\n";
		error_log($logstr,3,'../log/gen.log');

		$result = mysql_query($sql,$db);
		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
			$logstr = "$systime ERR：カテゴリ情報DB登録異常！ \r\n";
        	$logstr .= "$systime $ip INFO：▲カテゴリ情報登録異常終了 \r\n";
			error_log($logstr,3,'../log/gen.log');

			$err_cd_list[]="01";
			$_SESSION['err_cd_list']=$err_cd_list;
			$url= URL_PATH . "err.php";
			redirect($url);
		}

		mysql_close($db);
		$logstr = "$systime $ip INFO：▲カテゴリ情報登録正常終了！！ \r\n";
		error_log($logstr,3,'../log/gen.log');
		$url= URL_PATH . "m_category.php?action=search";
		redirect($url);
	}
?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $sub_title; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
</head>
<body>
<div class='main'>
<div class='subtitle'><?php echo $sub_title; ?></div>
<form enctype='multipart/form-data' method='post' name='upform'>
<div class='input-area'>
	<label class='w100'>カテゴリ名</label>
		<input type='text' name='i_category_name' id='i_category_name' class='w200'/>
	<div style='clear:both;'></div>
	<label class='w100'>ステータス</label>
		<select name='i_status' id='i_status' class='re_select'>
			<option value='1'>アクティブ</option>
			<option value='0'>非アクティブ</option>
		</select>
	<div style='clear:both;'></div>
	<input type='button' class='buttonS bGreen ml100 w200 mt40' value='送信' onclick='confirmSubmit();'/>

</div>

<script type="text/javascript" language="javascript">
	function confirmSubmit() {
		//カテゴリ
		if(document.upform.i_category_name.value == ""){
			alert("カテゴリ名を選択してください。");
			document.upform.i_brand_id.focus();
			return false;
		}
		//submit
		document.upform.action="?action=insert";
		document.upform.submit();
	}
</script>
</form>
</div>
</body>
</html>