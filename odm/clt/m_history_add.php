<?php
    //include
    require '../util/include.php';

    $action = $_GET['action'];
    $sysdate=date('Y-m-d',time());
    $systime=date('Y-m-d H:i:s',time());
    $systime_i=date('H:i:s',time());
    $ip=get_real_ip();
	$role=$_SESSION['role'];
	$login_user=$_SESSION['login_user'];

    //プルダウンリスト取得
    $db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
    if(!$db){
        die("connot connect:" . mysql_error());
    }
    $dns = mysql_select_db(DB_NAME,$db);
    if(!$dns){
        die("connot use db:" . mysql_error());
    }
    mysql_set_charset('utf8');
    //カテゴリプルダウンリスト取得
    $sqlall = "select * from den_category_info WHERE 1";
    $result_list_cate = mysql_query($sqlall,$db);
	//提携先プルダウンリスト取得
	if($role=='2'){
		$sqlall = "select * from brand WHERE 1 and del_flg=0 and login_id='".$login_user."'  order by brand_id,sort_order";
		$brand_list = mysql_query($sqlall,$db);
		$brand_is_disabled="disabled='disabled'";
	}
	elseif($role=='3'){
		$sqlall = "select bd.* from brand bd,shop sp WHERE 1 and bd.brand_id=sp.brand_id
                        and bd.del_flg=0 and sp.login_id='".$login_user."'  order by bd.brand_id,bd.sort_order";
		$brand_list = mysql_query($sqlall,$db);

		$brand_is_disabled="disabled='disabled'";
	}
	else{
		$sqlall = "select * from brand WHERE 1 and del_flg=0 order by brand_id,sort_order";
		$brand_list = mysql_query($sqlall,$db);
		$brand_is_disabled='';
	}
	if($role=='2'){
		$sqlall = "select * from brand WHERE 1 and del_flg=0 and login_id='".$login_user."'  order by brand_id,sort_order";
		$rate_list = mysql_query($sqlall,$db);
	}
	elseif($role=='3'){
		$sqlall = "select bd.* from brand bd,shop sp WHERE 1 and bd.brand_id=sp.brand_id
                        and bd.del_flg=0 and sp.login_id='".$login_user."'  order by bd.brand_id,bd.sort_order";
		$rate_list = mysql_query($sqlall,$db);
	}
	else{
		$sqlall = "select * from brand WHERE 1 and del_flg=0 order by brand_id,sort_order";
		$rate_list = mysql_query($sqlall,$db);
	}

    //都道府県プルダウンリスト取得
    $sqlall = "select * from den_area WHERE 1";
    $result_list_area = mysql_query($sqlall,$db);
    //購入履歴
		if($role=='2'){
			$sql_rank = "select * from brand where login_id='{$login_user}'";
			$query_rank = mysql_query($sql_rank,$db);

			$rs_rank=mysql_fetch_object($query_rank);
			$property_id=$rs_rank->property_id;
		}

		if($role=='3'){
			$sql_rank = "select bd.* from brand bd,shop sp WHERE 1 and bd.brand_id=sp.brand_id
													and bd.del_flg=0 and sp.login_id='{$login_user}'  order by bd.brand_id,bd.sort_order";
			$query_rank = mysql_query($sql_rank,$db);
			$rs_rank=mysql_fetch_object($query_rank);
			$property_id=$rs_rank->property_id;
		}
    $sqlAll = "select * from user_rank WHERE 1 and status = 1 ";
		if($role!='1'){
			$sqlAll .= "and id in ({$property_id}) ";
		}
    $result_instroduce_list = mysql_query($sqlAll,$db);
    //店舗名
	if($role=='2'){
		$sqlAll = "select sp.* from brand bd,shop sp WHERE 1 and bd.brand_id=sp.brand_id
				and bd.del_flg=0 and bd.login_id='".$login_user."'  order by bd.brand_id,bd.sort_order";
	}
	elseif($role=='3'){
		$sqlAll = "select * from shop WHERE 1
				and del_flg=0 and login_id='".$login_user."'";
	}else{
    	$sqlAll = "select * from shop WHERE 1";
	}
    $result_shop_list = mysql_query($sqlAll,$db);

    mysql_close($db);

    if ($action=='confirm'){
        $sub_title='購入履歴管理 - 新規追加確認 -';
        $is_disabled="disabled='disabled'";
        $havePlaceholder='NO';

		//hidden項目
		$u_id = $_POST['u_id'];
		$update_flg=$_POST['update_flg'];
		if($update_flg=='1'){
			$i_login_id=$_POST['h_login_id'];
		}else{
			$i_login_id=$_POST['i_login_id'];
		}

        //form項目
        $i_date = $_POST['i_date'];
        $i_time = $_POST['i_time'];
        $i_amount = $_POST['i_amount'];
        $i_property = $_POST['i_property'];
        $i_brand_id = $_POST['i_brand_id'];
        $i_shop_name = $_POST['i_shop_name'];
        $i_period = $_POST['i_period'];
        $i_sex = $_POST['i_sex'];
        $i_area = $_POST['i_area'];
        $i_commission = $_POST['i_commission'];
        $i_medium = $_POST['i_medium'];
        $i_note = $_POST['i_note'];
        $i_note=htmlspecialchars($i_note);
    }
	elseif ($action=='update'){

		$sub_title='購入履歴管理　- 編集 -';
		$is_disabled="";
		$havePlaceholder='NO';

		$u_id = $_GET['u_id'];
		$_SESSION['se_supply_time_from']="";
		$_SESSION['se_supply_time_to']="";
		$_SESSION['se_property']="";
		$_SESSION['se_brand_id']="";
		$_SESSION['se_area']="";
		$_SESSION['se_shop_area']="";
		$_SESSION['se_shop_name']="";
		$_SESSION['se_status']="";
		$_SESSION['se_supply_time_from']=$_GET['a'];
		$_SESSION['se_supply_time_to']=$_GET['b'];
		$_SESSION['se_property']=$_GET['c'];
		$_SESSION['se_brand_id']=$_GET['d'];
		$_SESSION['se_area']=$_GET['e'];
		$_SESSION['se_shop_area']=$_GET['i'];
		$_SESSION['se_shop_name']=$_GET['j'];
		$_SESSION['se_status']=$_GET['s_status'];

		$update_flg='1';

		$logstr = "$systime $ip INFO：▼購入履歴情報取得開始：id = $u_id \r\n";
		error_log($logstr,3,'../log/gen.log');

		if($u_id==''){

			db_disConn($result, $link);
			$logstr = "$systime ERR：id取得エラー！ \r\n";
			$logstr .= "$systime $ip INFO：▲購入履歴情報取得異常終了 \r\n";
			error_log($logstr,3,'../log/gen.log');

			$err_cd_list[]="99";
			$_SESSION['err_cd_list']=$err_cd_list;
			$url= URL_PATH . "err.php";
			redirect($url);
		}

		//情報取得
		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}
		$dns = mysql_select_db(DB_NAME,$db);
		if(!$dns){
			die("connot use db:" . mysql_error());
		}
		mysql_set_charset('utf8');
		$sqlall = "select * from den_history_info WHERE 1";
		$sqlall .= " and id = $u_id";

		$result = mysql_query($sqlall,$db);
		$rs=mysql_fetch_object($result);

		//日付
		$i_date = date('Y-m-d',$rs->date);
		//時間帯
		$i_time = $rs->time_slot;
		//金額
		$i_amount =$rs->amount;
		//手数料
		$i_commission=$rs->commission;
		//購入履歴
		$i_property=$rs->rank_id;
		//提携先ID
		$i_brand_id=$rs->brand_id;
		//店舗ID
		$i_shop_name=$rs->shop_id;
		//年代
		$i_period=$rs->period;
		//性別
		$i_sex=$rs->sex;
		//都道府県ID
		$i_area=$rs->address;
		//媒体
		$i_medium=$rs->medium;
		//rank_id
		$i_rank=$rs->rank_id;
		//備考
		$i_note =$rs->note;
		$i_note = htmlspecialchars($i_note);

		mysql_close($db);

		$logstr = "$systime $ip INFO：▲購入履歴情報取得正常終了：id = $u_id \r\n";
		error_log($logstr,3,'../log/gen.log');

	}
    elseif($action=='edit'){
        $sub_title='購入履歴管理 - 新規追加訂正 -';
        $is_disabled="";
        $havePlaceholder='YES';
        //form項目
        $i_date = $_POST['i_date'];
        $i_time = $_POST['i_time'];
        $i_amount = $_POST['i_amount'];
        $i_property = $_POST['i_property'];
        $i_brand_id = $_POST['i_brand_id'];
        $i_shop_name = $_POST['i_shop_name'];
        $i_period = $_POST['i_period'];
        $i_sex = $_POST['i_sex'];
        $i_area = $_POST['i_area'];
        $i_commission = $_POST['i_commission'];
        $i_medium = $_POST['i_medium'];
        $i_note = $_POST['i_note'];
        $i_note=htmlspecialchars($i_note);
    }
    elseif($action=='insert'){
        $logstr = "$systime $ip INFO：▼購入履歴情報登録開始 \r\n";
        error_log($logstr,3,'../log/gen.log');

        $i_date = strtotime($_POST['i_date']);
        $i_time = $_POST['i_time'];
        $i_amount = $_POST['i_amount'];
        $i_property = $_POST['i_property'];
        $i_brand_id = $_POST['i_brand_id'];
        $i_shop_name = $_POST['i_shop_name'];
        $i_period = $_POST['i_period'];
        $i_sex = $_POST['i_sex'];
        $i_area = $_POST['i_area'];
        $i_commission = $_POST['i_commission'];
        $i_medium = $_POST['i_medium'];
        $i_note = $_POST['i_note'];
		$i_note=htmlspecialchars($i_note);
        $i_del_flg = 0;
        $i_rank = '';
        $i_address = '';
		if($role=='3'){
			$i_status = '0';
		}
		elseif($role=='2'){
			$i_status = '1';
		}
		else{
			$i_status = '2';
		}
		$rep_month=date('Y年m月',$i_date);
        $insert_time = strtotime($systime);
        $update_time = '0';

        $db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        if(!$db){
            die("connot connect:" . mysql_error());
        }

        $dns = mysql_select_db(DB_NAME,$db);

        if(!$dns){
            die("connot use db:" . mysql_error());
        }

        mysql_set_charset('utf8');

		$sql = sprintf("insert into den_history_info (date,time_slot,amount,commission,property_id,brand_id,shop_id,period,sex,area_id,medium,note,del_flg,rank_id,address,status,rep_month,insert_time,update_time) values
                       (%d,'%s',%d,%d,'%s',%d,%d,%d,%d,'%s','%s','%s',%d,'%s','%s',%d,'%s',%d,%d)",
        $i_date,$i_time,$i_amount,$i_commission,$i_property,$i_brand_id,$i_shop_name,$i_period,$i_sex,$i_area,$i_medium,
        $i_note,$i_del_flg,$i_property,$i_area,$i_status,$rep_month,$insert_time,$update_time
		);

        $logstr = "$systime $ip INFO：購入履歴情報登録 INSERT SQL文： ".$sql."\r\n";
        error_log($logstr,3,'../log/gen.log');
        $result = mysql_query($sql,$db);
        

        if(!$result){
            $rowCnt = -1;
            db_disConn($result, $link);
            $logstr = "$systime ERR：購入履歴情報DB登録異常！ \r\n";
            $logstr .= "$systime $ip INFO：▲購入履歴情報登録異常終了 \r\n";
            error_log($logstr,3,'../log/gen.log');

            $err_cd_list[]="01";
            $_SESSION['err_cd_list']=$err_cd_list;
            $url= URL_PATH . "err.php";
            redirect($url);
        }
        $logstr = "$systime $ip INFO：▲購入履歴情報録正常終了！！ \r\n";
        error_log($logstr,3,'../log/gen.log');

		if($i_date==''||$i_shop_name==''||$i_property==''){
			$logstr = "$systime $ip INFO：▼購入履歴情報スタータス更新開始 \r\n";
			error_log($logstr,3,'../log/gen.log');
			$sql = sprintf("update den_history_info set status=9 where insert_time=%d",$insert_time);
			$logstr = "$systime $ip INFO：購入履歴情報更新 UPDATE SQL文： ".$sql."\r\n";
			error_log($logstr,3,'../log/gen.log');

			$result = mysql_query($sql,$db);
			if(!$result){
				db_disConn($result, $link);
				$logstr = "$systime ERR：購入履歴情報スタータスDB更新異常！ \r\n";
				$logstr .= "$systime $ip INFO：▲購入履歴情報スタータス更新異常終了 \r\n";
				error_log($logstr,3,'../log/gen.log');

				$err_cd_list[]="01";
				$_SESSION['err_cd_list']=$err_cd_list;
				$url= URL_PATH . "err.php";
				redirect($url);
			}
			$logstr = "$systime $ip INFO：▲購入履歴情報スタータス更新正常終了！！ \r\n";
			error_log($logstr,3,'../log/gen.log');
		}
			else{
			$logstr = "$systime $ip INFO：▼購入履歴情報スタータス更新開始 \r\n";
			error_log($logstr,3,'../log/gen.log');
			$sql = sprintf("update den_history_info set status=0 where id=%d",$u_id);
			$logstr = "$systime $ip INFO：購入履歴情報更新 UPDATE SQL文： ".$sql."\r\n";
			error_log($logstr,3,'../log/gen.log');

			$result = mysql_query($sql,$db);
			if(!$result){
				db_disConn($result, $link);
				$logstr = "$systime ERR：購入履歴情報スタータスDB更新異常！ \r\n";
				$logstr .= "$systime $ip INFO：▲購入履歴情報スタータス更新異常終了 \r\n";
				error_log($logstr,3,'../log/gen.log');

				$err_cd_list[]="01";
				$_SESSION['err_cd_list']=$err_cd_list;
				$url= URL_PATH . "err.php";
				redirect($url);
			}
			$logstr = "$systime $ip INFO：▲購入履歴情報スタータス更新正常終了！！ \r\n";
			error_log($logstr,3,'../log/gen.log');
		}
		mysql_close($db);
//        $url= URL_PATH . "m_history.php?action=search&role=".$role."&l_id=".$login_user;
		if($role!='1'){
			$url= URL_PATH . "m_history.php?action=search&b_id=".$i_brand_id."&role=".$role."&l_id=".$login_user.
							'&s_brand_id='.$_SESSION['se_brand_id'].
							'&s_property='.$_SESSION['se_property'].
							'&s_shop_area='.$_SESSION['se_shop_area'].
							'&s_shop_name='.$_SESSION['se_shop_name'].
							'&i_supply_time_from='.$_SESSION['se_supply_time_from'].
							'&i_supply_time_to='.$_SESSION['se_supply_time_to'].
							'&s_area='.$_SESSION['se_area'].
							'&s_status='.$_SESSION['se_status'];
		}else{
			$url= URL_PATH . "m_history.php?action=search&role=".$role."&l_id=".$login_user.
							'&s_brand_id='.$_SESSION['se_brand_id'].
							'&s_property='.$_SESSION['se_property'].
							'&s_shop_area='.$_SESSION['se_shop_area'].
							'&s_shop_name='.$_SESSION['se_shop_name'].
							'&i_supply_time_from='.$_SESSION['se_supply_time_from'].
							'&i_supply_time_to='.$_SESSION['se_supply_time_to'].
							'&s_area='.$_SESSION['se_area'].
							'&s_status='.$_SESSION['se_status'];
		}
        redirect($url);
  }
	elseif($action=='new'){
			$_SESSION['se_supply_time_from']="";
			$_SESSION['se_supply_time_to']="";
			$_SESSION['se_property']="";
			$_SESSION['se_brand_id']="";
			$_SESSION['se_area']="";
			$_SESSION['se_shop_area']="";
			$_SESSION['se_shop_name']="";
			$_SESSION['se_status']="";

			$_SESSION['se_supply_time_from']=$_GET['a'];
			$_SESSION['se_supply_time_to']=$_GET['b'];
			$_SESSION['se_property']=$_GET['c'];
			$_SESSION['se_brand_id']=$_GET['d'];
			$_SESSION['se_area']=$_GET['e'];
			$_SESSION['se_shop_area']=$_GET['i'];
			$_SESSION['se_shop_name']=$_GET['j'];
			$_SESSION['se_status']=$_GET['s_status'];

			$sub_title='購入履歴管理 - 新規追加 -';
			$is_disabled="";
			$havePlaceholder='YES';

			//form項目
			$i_date = $sysdate;
			$i_time   = '';
			$i_amount = '';
			$i_property    = '';
			$i_brand_id  = '';
			$i_shop_name = '';
			$i_period = '';
			$i_sex = '';
			$i_area = '';
			$i_commission = '';
			$i_medium = '';
			$i_note = '';
	}
	elseif ($action=='updatesubmit'){
		$u_id=$_POST['u_id'];

		$i_date = strtotime($_POST['i_date']);
		$i_time = $_POST['i_time'];
		$i_amount = $_POST['i_amount'];
		$i_property = $_POST['i_property'];
		$i_brand_id = $_POST['i_brand_id'];
		$i_shop_name = $_POST['i_shop_name'];
		$i_period = $_POST['i_period'];
		$i_sex = $_POST['i_sex'];
		$i_area = $_POST['i_area'];
		$i_commission = $_POST['i_commission'];
		$i_medium = $_POST['i_medium'];
		$i_note = $_POST['i_note'];
		$i_note=htmlspecialchars($i_note);
		$i_del_flg = '';
		$i_rank = '';
		$i_address = '';
		$insert_time = '';
		$update_time = '';
		$rep_month=date("Y年m月",$i_date);

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$logstr = "$systime $ip INFO：▼購入履歴情報更新開始 \r\n";
		error_log($logstr,3,'../log/gen.log');
		$sql = sprintf("update den_history_info set
                                date=%d,
                                time_slot='%s',
                                amount=%d,
                                commission=%d,
                                property_id='%s',
                                brand_id=%d,
                                shop_id=%d,
                                period=%d,
                                sex=%d,
                                area_id='%s',
                                medium='%s',
                                note='%s',
                                rank_id='%s',
                                address='%s',
                                update_time=%d,
                                rep_month='%s'  
                                where id=%d",
			$i_date,
			$i_time,
			$i_amount,
			$i_commission,
			$i_property,
			$i_brand_id,
			$i_shop_name,
			$i_period,
			$i_sex,
			$i_area,
			$i_medium,
			$i_note,
			$i_property,
			$i_area,
			strtotime($systime),
			$rep_month,
			$u_id
		);

		$logstr = "$systime $ip INFO：購入履歴情報更新 UPDATE SQL文： ".$sql."\r\n";
		error_log($logstr,3,'../log/gen.log');

		$result = mysql_query($sql,$db);
		if(!$result){
			db_disConn($result, $link);
			$logstr = "$systime ERR：購入履歴情報DB更新異常！ \r\n";
			$logstr .= "$systime $ip INFO：▲購入履歴情報更新異常終了 \r\n";
			error_log($logstr,3,'../log/gen.log');

			$err_cd_list[]="01";
			$_SESSION['err_cd_list']=$err_cd_list;
			$url= URL_PATH . "err.php";
			redirect($url);
		}

		$logstr = "$systime $ip INFO：▲購入履歴情報更新正常終了！！ \r\n";
		error_log($logstr,3,'../log/gen.log');


		if($i_date==''||$i_brand_id==''||$i_shop_name==''||$i_property==''){
			$logstr = "$systime $ip INFO：▼購入履歴情報スタータス更新開始 \r\n";
			error_log($logstr,3,'../log/gen.log');
			$sql = sprintf("update den_history_info set status=9 where id=%d",$u_id);
			$logstr = "$systime $ip INFO：購入履歴情報更新 UPDATE SQL文： ".$sql."\r\n";
			error_log($logstr,3,'../log/gen.log');

			$result = mysql_query($sql,$db);
			if(!$result){
				db_disConn($result, $link);
				$logstr = "$systime ERR：購入履歴情報スタータスDB更新異常！ \r\n";
				$logstr .= "$systime $ip INFO：▲購入履歴情報スタータス更新異常終了 \r\n";
				error_log($logstr,3,'../log/gen.log');

				$err_cd_list[]="01";
				$_SESSION['err_cd_list']=$err_cd_list;
				$url= URL_PATH . "err.php";
				redirect($url);
			}
			$logstr = "$systime $ip INFO：▲購入履歴情報スタータス更新正常終了！！ \r\n";
			error_log($logstr,3,'../log/gen.log');
		}else{
			$logstr = "$systime $ip INFO：▼購入履歴情報スタータス更新開始 \r\n";
			error_log($logstr,3,'../log/gen.log');
			$sql = sprintf("update den_history_info set status=0 where id=%d",$u_id);
			$logstr = "$systime $ip INFO：購入履歴情報更新 UPDATE SQL文： ".$sql."\r\n";
			error_log($logstr,3,'../log/gen.log');

			$result = mysql_query($sql,$db);
			if(!$result){
				db_disConn($result, $link);
				$logstr = "$systime ERR：購入履歴情報スタータスDB更新異常！ \r\n";
				$logstr .= "$systime $ip INFO：▲購入履歴情報スタータス更新異常終了 \r\n";
				error_log($logstr,3,'../log/gen.log');

				$err_cd_list[]="01";
				$_SESSION['err_cd_list']=$err_cd_list;
				$url= URL_PATH . "err.php";
				redirect($url);
			}
			$logstr = "$systime $ip INFO：▲購入履歴情報スタータス更新正常終了！！ \r\n";
			error_log($logstr,3,'../log/gen.log');
		}
		mysql_close($db);
//		$url= URL_PATH . "m_history.php?action=search";
		if($role!='1'){
			$url= URL_PATH . "m_history.php?action=search&b_id=".$i_brand_id."&role=".$role."&l_id=".$login_user.
							'&s_brand_id='.$_SESSION['se_brand_id'].
							'&s_property='.$_SESSION['se_property'].
							'&s_shop_area='.$_SESSION['se_shop_area'].
							'&s_shop_name='.$_SESSION['se_shop_name'].
							'&i_supply_time_from='.$_SESSION['se_supply_time_from'].
							'&i_supply_time_to='.$_SESSION['se_supply_time_to'].
							'&s_area='.$_SESSION['se_area'].
							'&s_status='.$_SESSION['se_status'];
		}else{
			$url= URL_PATH . "m_history.php?action=search&role=".$role."&l_id=".$login_user.
							'&s_brand_id='.$_SESSION['se_brand_id'].
							'&s_property='.$_SESSION['se_property'].
							'&s_shop_area='.$_SESSION['se_shop_area'].
							'&s_shop_name='.$_SESSION['se_shop_name'].
							'&i_supply_time_from='.$_SESSION['se_supply_time_from'].
							'&i_supply_time_to='.$_SESSION['se_supply_time_to'].
							'&s_area='.$_SESSION['se_area'].
							'&s_status='.$_SESSION['se_status'];
		}
		redirect($url);
	}
	else{
			$sub_title='購入履歴管理 - 新規追加 -';
			$is_disabled="";
			$havePlaceholder='YES';

			//form項目
			$i_date = $sysdate;
			$i_time   = '';
			$i_amount = '';
			$i_property    = '';
			$i_brand_id  = '';
			$i_shop_name = '';
			$i_period = '';
			$i_sex = '';
			$i_area = '';
			$i_commission = '';
			$i_medium = '';
			$i_note = '';
	}
?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $sub_title;?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
 <script>
	function createAjax(){
			var xmlHttp = false;
			if (window.XMLHttpRequest){
				xmlHttp = new XMLHttpRequest();
			}else if(window.ActiveXObject){
				try{
					xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
				}catch(e){
					try{
						xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
					}catch(e){
						xmlHttp = false;
					}
				}
			}
			return xmlHttp;	
	}
	
	var ajax = null;
	function getBrand(rank_id){
		ajax = createAjax();
		ajax.onreadystatechange=function(){
			if(ajax.readyState == 4){
				if(ajax.status == 200){				
					var brands = ajax.responseXML.getElementsByTagName("brand");
					$('i_brand_id').length = 0;
					var myoption = document.createElement("option");
					myoption.value = "";
					myoption.innerText = "";
					$('i_brand_id').appendChild(myoption);
					for(var i=0;i<brands.length;i++){
						var brand_id = brands[i].childNodes[0].childNodes[0].nodeValue;
						var brand_name = brands[i].childNodes[1].childNodes[0].nodeValue;
						var myoption = document.createElement("option");
						myoption.value = brand_id;
						myoption.innerText = brand_name;
						$('i_brand_id').appendChild(myoption);
					}
				}
			}
		}
 
		ajax.open("post","m_getList.php",true);
		ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajax.send("rank_id="+rank_id);
 
	}
 
	function getShop(brand_id){
		ajax = createAjax();
		ajax.onreadystatechange=function(){
			if(ajax.readyState == 4){
				if(ajax.status == 200){
					var shops = ajax.responseXML.getElementsByTagName("shop");
					$('i_shop_name').length = 0;
					var myoption = document.createElement("option");
					myoption.value = "";
					myoption.innerText = "";
					$('i_shop_name').appendChild(myoption);
					for(var i=0;i<shops.length;i++){
						var shop_id = shops[i].childNodes[0].childNodes[0].nodeValue;
						var shop_name = shops[i].childNodes[1].childNodes[0].nodeValue;
						var myoption = document.createElement("option");
						myoption.value = shop_id;
						myoption.innerText = shop_name;
						$('i_shop_name').appendChild(myoption);
					}
				}
			}
		}
		ajax.open("post","m_getList.php",true);
		ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajax.send("brand_id="+brand_id);

		var rate_id='h_rate'+brand_id;
		document.getElementById('i_commission').value=document.getElementById(rate_id).value*0.01*document.getElementById('i_amount').value;
	}
 
	function $(id){
		return document.getElementById(id);
	}
 
 </script>
</head>
<body>
<div class='main'>
<div class='subtitle'><?php echo $sub_title; ?></div>
<form enctype='multipart/form-data' method='post' name='upform'>
<div class='input-area'>
    <label class='w100'>日付</label>
        <input type='text' name='i_date' id='i_date' class='w200' placeholder='<?php if($havePlaceholder=='YES') echo$systime;?>' value='<?php echo $i_date; ?>' <?php echo $is_disabled;?>/>
        <span class='input-span ml20 fl'>※必須</span>
    <div style='clear:both;'></div>
    <label class='w100'>時間帯</label>
        <input type='text' name='i_time' id='i_time' class='w200' value='<?php if($i_time==''){echo $systime_i;}else{echo $i_time;} ?>' <?php echo $is_disabled;?>/>
    <div style='clear:both;'></div>
    <label class='w100'>金額</label>
        <input type='text' name='i_amount' id='i_amount' class='w200' placeholder='<?php if($havePlaceholder=='YES') echo $i_amount;?>' value='<?php echo $i_amount; ?>' <?php echo $is_disabled;?>/>
        <span class='input-span ml20 fl'>円　※必須</span>
    <div style='clear:both;'></div>
    <label class='w100'>紹介元属性</label>
		<?php if($role=='1'){?>
        <select name='i_property' id='i_property' class='re_select w500 fl' <?php echo $is_disabled;?> onchange='getBrand(this.value);'>
		<?php }else{?>
        <select name='i_property' id='i_property' class='re_select w500 fl' <?php echo $is_disabled;?>'>
		<?php }?>
            <option value=''></option>
                <?php
                while($arr_list_row=mysql_fetch_array($result_instroduce_list)){
                    if($arr_list_row[rank_id]==$i_property){
                        echo"<option value=".$arr_list_row[rank_id]." selected >".$arr_list_row[rank_name]. "</option>";
                    }else{
                        echo"<option value=".$arr_list_row[rank_id]." >".$arr_list_row[rank_name]. "</option>";
                    }
                }
                ?>
        </select>
    <span class='input-span ml20 fl'>※必須</span>
    <div style='clear:both;'></div>
	<?php
		if($role!='1'){
			$rs=mysql_fetch_object($brand_list);
			if($rs->brand_id!=''&&$role!=''&&$login_user!=''){
				echo"
							<input type='hidden' name='h_brand_id' value='".$rs->brand_id."'/>
						";
			}
		}
	?>
    <label class='w100'>提携先</label>
        <select name='i_brand_id' id='i_brand_id' class='re_select w200 fl' <?php echo $is_disabled;?> onchange='getShop(this.value);'>
                <?php
					if($role!='1'){
						echo"<option value=".$rs->brand_id." selected>".$rs->brand_name. "</option>";
						echo"<input type='hidden' name='h_rate' value='".$arr_list_row[rate]."'/>";
					}else{
						echo "<option value=''></option>";
						//while($arr_list_row=mysql_fetch_array($brand_list)){
						//	if($arr_list_row[brand_id]==$i_brand_id){
						//		echo"<option value=".$arr_list_row[brand_id]." selected >".$arr_list_row[brand_name]. "</option>";
						//	}else{
						//		echo"<option value=".$arr_list_row[brand_id]." >".$arr_list_row[brand_name]. "</option>";
						//	}
						//}
					}
                ?>
        </select>
        <?php
        while($arr_list_row=mysql_fetch_array($rate_list)){
        	echo"<input type='hidden' name='h_rate".$arr_list_row[brand_id]."' id='h_rate".$arr_list_row[brand_id]."' value='".$arr_list_row[rate]."'/>";
        }
        ?>
    <div style='clear:both;'></div>
    
 	<?php
		if($role=='3'){
			$rs=mysql_fetch_object($result_shop_list);
			if($rs->shop_id!=''&&$role!=''&&$login_user!=''){
				echo"
							<input type='hidden' name='h_shop_id' value='".$rs->shop_id."'/>
						";
			}
		}
	?>
    <label class='w100'>店舗名</label>
        <select name='i_shop_name' id='i_shop_name' class='re_select w500 fl' <?php echo $is_disabled;?>>
        	<?php if($role=='3'){
						echo"<option value=".$rs->shop_id." selected>".$rs->shop_name. "</option>";
						}elseif($role=='2'){
                while($arr_list_row=mysql_fetch_array($result_shop_list)){
                    if($arr_list_row[shop_id]==$i_shop_name){
                        echo"<option value=".$arr_list_row[shop_id]." selected >".$arr_list_row[shop_name]. "</option>";
                    }else{
                        echo"<option value=".$arr_list_row[shop_id]." >".$arr_list_row[shop_name]. "</option>";
                    }
                }
						}else{?>
            <option value=''></option>
                <?php
                //while($arr_list_row=mysql_fetch_array($result_shop_list)){
                //    if($arr_list_row[shop_id]==$i_shop_name){
                //        echo"<option value=".$arr_list_row[shop_id]." selected >".$arr_list_row[shop_name]. "</option>";
                //    }else{
                //        echo"<option value=".$arr_list_row[shop_id]." >".$arr_list_row[shop_name]. "</option>";
                //    }
                //}
                ?>
         <?php }?>
        </select>
    <span class='input-span ml20 fl'>※必須</span>
    <div style='clear:both;'></div>
    <label class='w100'>年代</label>
        <select name='i_period' id='i_period' class='re_select w100 fl' <?php echo $is_disabled;?>>
            <?php 
                $arr = array("","10","20","30","40","50", "60", "70", "80", "90", "00");
                for ($i=0; $i < count($arr); $i++) { 
                    if ($arr[$i]==$i_period) {
                        echo "<option value=".$arr[$i]." selected >".$arr[$i]. "</option>";
                    }else{
                        echo"<option value=".$arr[$i]." >".$arr[$i]. "</option>";
                    }
                }
            ?>
        </select>
    <div style='clear:both;'></div>
    <label class='w100'>性別</label>
        <select name='i_sex' id='i_sex' class='re_select' <?php echo $is_disabled;?>>
            <option value=''<?php if ($i_sex=='') {echo 'selected';}?>></option>
            <option value='1'<?php if ($i_sex=='1') {echo 'selected';}?>>男性</option>
            <option value='2'<?php if ($i_sex=='2') {echo 'selected';}?>>女性</option>
        </select>
    <div style='clear:both;'></div>
    <label class='w100'>都道府県</label>
        <select name='i_area' id='i_area' class='re_select w100' <?php echo $is_disabled;?>>
            <option value=''></option>
                <?php
                while($arr_list_row=mysql_fetch_array($result_list_area)){
                    if($arr_list_row[name]==$i_area){
                        echo"<option value=".$arr_list_row[name]." selected >".$arr_list_row[name]. "</option>";
                    }else{
                        echo"<option value=".$arr_list_row[name]." >".$arr_list_row[name]. "</option>";
                    }
                }
                ?>
        </select>
    <div style='clear:both;'></div>
    <label class='w100'>手数料</label>
        <input type='text' name='i_commission' id='i_commission' class='w200' placeholder='<?php if($havePlaceholder=='YES') echo $i_commission;?>' value='<?php echo $i_commission; ?>' <?php echo $is_disabled;?>/>
        <span class='input-span ml20 fl'>円　※必須</span>
    <div style='clear:both;'></div>
    <label class='w100'>媒体</label>
        <input type='text' name='i_medium' id='i_medium' class='w200' placeholder='<?php if($havePlaceholder=='YES') echo $i_medium;?>' value='<?php echo $i_medium; ?>' <?php echo $is_disabled;?>/>
    <div style='clear:both;'></div>
    <label class='w100'>備考</label>
    <textarea name='i_note' id='i_note' placeholder='<?php if($havePlaceholder=='YES') echo'';?>' <?php echo $is_disabled;?>><?php echo htmlspecialchars_decode($i_note);?></textarea>
    <div style='clear:both;'></div>
    <input type="hidden" name="update_flg" value="<?php echo $update_flg;?>"/>
    <input type="hidden" name="u_id" value="<?php echo $u_id;?>" />
    <?php
    if($action=='confirm'){
		if($update_flg=='1'){
			echo "
			<input type='button' class='buttonS bGreen ml143 w200 mt40' value='訂正' onclick='editMode();'/>
			<input type='button' class='buttonS bGreen ml100 w200 mt40' value='更新' onclick='updateSubmit();'/>
		";
		}else{
			echo "
			<input type='button' class='buttonS bGreen ml143 w200 mt40' value='訂正' onclick='editMode();'/>
			<input type='button' class='buttonS bGreen ml100 w200 mt40' value='送信' onclick='confirmSubmit();'/>
		";
		}
    }else{
    		echo "<input type='button' class='buttonS bGreen ml143 w200 mt40' value='キャンセル' onclick='back();'/>";
        echo "<input type='button' class='buttonS bGreen ml143 w200 mt40' value='確認画面へ' onclick='moveConfirm();'/>";
    }
    ?>
</div>

<script type="text/javascript" language="javascript">
    function moveConfirm() {
        //日付
         if(document.upform.i_date.value == ""){
          alert("日付を入力してください。");
          document.upform.i_date.focus();
          return false;
         }
         //
         if(document.upform.i_amount.value == ""){
          alert("金額を入力してください。");
          document.upform.i_amount.focus();
          return false;
         }
         //
         if(document.upform.i_commission.value == ""){
          alert("手数料を入力してください。");
          document.upform.i_commission.focus();
          return false;
         }
         //
         if(document.upform.i_property.value == ""){
          alert("紹介元属性を選択してください。");
          document.upform.i_property.focus();
          return false;
         }
         //
         if(document.upform.i_shop_name.value == ""){
          alert("店舗名を選択してください。");
          document.upform.i_shop_name.focus();
          return false;
         }
         
        //submit
        document.upform.action="?action=confirm";
        document.upform.submit();
    }
    function confirmSubmit() {
        document.getElementById('i_date').disabled=false;
        document.getElementById('i_time').disabled=false;
        document.getElementById('i_amount').disabled=false;
        document.getElementById('i_property').disabled=false;
        document.getElementById('i_brand_id').disabled=false;
        document.getElementById('i_shop_name').disabled=false;
        document.getElementById('i_period').disabled=false;
        document.getElementById('i_sex').disabled=false;
        document.getElementById('i_area').disabled=false;
        document.getElementById('i_commission').disabled=false;
        document.getElementById('i_medium').disabled=false;
        document.getElementById('i_note').disabled=false;

        document.upform.action="?action=insert";
        document.upform.submit();
    }
	function updateSubmit() {
		document.getElementById('i_date').disabled=false;
		document.getElementById('i_time').disabled=false;
		document.getElementById('i_amount').disabled=false;
		document.getElementById('i_property').disabled=false;
		document.getElementById('i_brand_id').disabled=false;
		document.getElementById('i_shop_name').disabled=false;
		document.getElementById('i_period').disabled=false;
		document.getElementById('i_sex').disabled=false;
		document.getElementById('i_area').disabled=false;
		document.getElementById('i_commission').disabled=false;
		document.getElementById('i_medium').disabled=false;
		document.getElementById('i_note').disabled=false;

		document.upform.action="?action=updatesubmit";
		document.upform.submit();
	}
    function editMode(){
        document.getElementById('i_date').disabled=false;
        document.getElementById('i_time').disabled=false;
        document.getElementById('i_amount').disabled=false;
        document.getElementById('i_property').disabled=false;
        document.getElementById('i_brand_id').disabled=false;
        document.getElementById('i_shop_name').disabled=false;
        document.getElementById('i_period').disabled=false;
        document.getElementById('i_sex').disabled=false;
        document.getElementById('i_area').disabled=false;
        document.getElementById('i_commission').disabled=false;
        document.getElementById('i_medium').disabled=false;
        document.getElementById('i_note').disabled=false;

        document.upform.action="?action=edit";
        document.upform.submit();
    }
	function show(brand_id) {
		var rate_id='h_rate'+brand_id;
		document.getElementById('i_commission').value=document.getElementById(rate_id).value*0.01*document.getElementById('i_amount').value;
	}
	function back() {
		window.history.back(-1);
	}
</script>
</form>
</div>
</body>
</html>