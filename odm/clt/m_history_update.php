<?php
    //include
    require '../util/include.php';

    $action = $_GET['action'];
    $u_id = $_GET['u_id'];
    $sysdate=date('Y-m-d',time());
    $systime=date('Y-m-d H:i:s',time());
    $ip=get_real_ip();


    //プルダウンリスト取得
    $db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
    if(!$db){
        die("connot connect:" . mysql_error());
    }
    $dns = mysql_select_db(DB_NAME,$db);
    if(!$dns){
        die("connot use db:" . mysql_error());
    }
    mysql_set_charset('utf8');
    //紹介元属性プルダウンリスト取得
    $sqlall = "select * from user_rank WHERE 1";
    $result_list_property = mysql_query($sqlall,$db);
    //提携先プルダウンリスト取得
    $sqlall = "select * from brand WHERE 1";
    $result_list_brand = mysql_query($sqlall,$db);
    //カテゴリプルダウンリスト取得
    $sqlall = "select * from category WHERE 1";
    $result_list_cate = mysql_query($sqlall,$db);
    //SHOPプルダウンリスト取得
    $sqlall = "select * from shop WHERE 1";
    $result_list_shop = mysql_query($sqlall,$db);
    //都道府県プルダウンリスト取得
    $sqlall = "select * from den_area WHERE 1";
    $result_list_area = mysql_query($sqlall,$db);

    mysql_close($db);

    if ($action=='confirm'){
        $sub_title='購入履歴管理 - 新規追加確認 -';
        $is_disabled="disabled='disabled'";
        $havePlaceholder='NO';

        //form項目
        $upt_date        = $_POST['i_date'];
        $upt_date        = date('Y-m-d',$upt_date);
        $upt_time_slot   = $_POST['i_time'];
        $upt_amount      = $_POST['i_amount'];
        $upt_property_id = $_POST['i_property'];
        $upt_brand_id    = $_POST['i_brand'];
        $upt_shop_id     = $_POST['i_shop_name'];
        $upt_period      = $_POST['i_period'];
        $upt_sex         = $_POST['i_sex'];
        $upt_area_id     = $_POST['i_area'];
        $upt_commission  = $_POST['i_commission'];
        $upt_medium      = $_POST['i_medium'];
        $upt_note        = $_POST['i_note'];
        $upt_note        = str_replace("<br />","\n",$upt_note);
        $upt_note        = htmlspecialchars($upt_note);
    }
    elseif($action=='edit'){
        $sub_title='購入履歴管理 - 新規追加訂正 -';
        $is_disabled="";
        $havePlaceholder='YES';
        //form項目
        $upt_date        = $_POST['i_date'];
        $upt_date        = date('Y-m-d',$upt_date);
        $upt_time_slot   = $_POST['i_time'];
        $upt_amount      = $_POST['i_amount'];
        $upt_property_id = $_POST['i_property'];
        $upt_brand_id    = $_POST['i_brand'];
        $upt_shop_id   = $_POST['i_shop_name'];
        $upt_period      = $_POST['i_period'];
        $upt_sex         = $_POST['i_sex'];
        $upt_area_id     = $_POST['i_area'];
        $upt_commission  = $_POST['i_commission'];
        $upt_medium      = $_POST['i_medium'];
        $upt_note        = $_POST['i_note'];
        $upt_note        = str_replace("<br />","\n",$upt_note);
        $upt_note        = htmlspecialchars($upt_note);
    }
    elseif ($action=='update'){
        $sub_title='購入履歴管理 - 編集 -';
        $is_disabled="";
        $havePlaceholder='YES';

        //情報取得
        $db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        if(!$db){
            die("connot connect:" . mysql_error());
        }
        $dns = mysql_select_db(DB_NAME,$db);
        if(!$dns){
            die("connot use db:" . mysql_error());
        }
        mysql_set_charset('utf8');
        $sqlall = "select * from den_history_info WHERE 1";
        if($u_id!='') {
            $sqlall .= " and id = $u_id";
        }

        $result = mysql_query($sqlall,$db);
        $rs=mysql_fetch_object($result);

        $upt_id=$rs->id;
        $upt_date=$rs->date;
        $upt_date=date('Y-m-d',$upt_date);
        $upt_time_slot=$rs->time_slot;
        $upt_amount=$rs->amount;
        $upt_commission=$rs->commission;
        $upt_property_id=$rs->property_id;
        $upt_brand_id=$rs->brand_id;
        $upt_shop_id=$rs->shop_id;
        $upt_period=$rs->period;
        $upt_sex=$rs->sex;
        $upt_area_id=$rs->area_id;
        $upt_medium=$rs->medium;
        $upt_note=$rs->note;
        $upt_note=str_replace("<br />","\n",$upt_note);
        $upt_note=htmlspecialchars($upt_note);
        $upt_del_flg=$rs->del_flg;
        $upt_insert_time=$rs->insert_time;
        $upt_update_time=$rs->update_time;

        mysql_close($db);
    }else{

        //insert
        if ($action=='insert'){
            $logstr = "$systime $ip INFO：▼購入履歴情報登録開始 \r\n";
            error_log($logstr,3,'../log/gen.log');
    
            $upt_date        = $_POST['i_date'];
            $upt_date        = date('Y-m-d',$upt_date);
            $upt_time_slot   = $_POST['i_time'];
            $upt_amount      = $_POST['i_amount'];
            $upt_property_id = $_POST['i_property'];
            $upt_brand_id    = $_POST['i_brand'];
            $upt_shop_id     = $_POST['i_shop_name'];
            $upt_period      = $_POST['i_period'];
            $upt_sex         = $_POST['i_sex'];
            $upt_area_id     = $_POST['i_area'];
            $upt_commission  = $_POST['i_commission'];
            $upt_medium      = $_POST['i_medium'];
            $upt_note        = $_POST['i_note'];
            $upt_note        = str_replace("<br />","\n",$upt_note);
            $upt_note        = htmlspecialchars($upt_note);

            $db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
            if(!$db){
                die("connot connect:" . mysql_error());
            }

            $dns = mysql_select_db(DB_NAME,$db);

            if(!$dns){
                die("connot use db:" . mysql_error());
            }

            mysql_set_charset('utf8');

            if($u_id!='') {
                $sql = "
                    UPDATE den_history_info 
                    SET 
                        date        = '".$upt_date."',
                        time_slot   = '".$upt_time_slot."',
                        amount      = '".$upt_amount."',
                        commission  = '".$upt_commission."',
                        property_id = '".$upt_property_id."',
                        brand_id    = '".$upt_brand_id."',
                        shop_id     = '".$upt_shop_id."',
                        period      = '".$upt_period."',
                        sex         = '".$upt_sex."',
                        area_id     = '".$upt_area_id."',
                        medium      = '".$upt_medium."',
                        note        = '".$upt_note."'
                    WHERE 1
                    AND id = '".$u_id."'";
            }

            $logstr = "$systime $ip INFO：購入履歴情報登録 INSERT SQL文： ".$sql."\r\n";
            error_log($logstr,3,'../log/gen.log');
            $result = mysql_query($sql,$db);
    
            if(!$result){
                $rowCnt = -1;
                db_disConn($result, $link);
                $logstr = "$systime ERR：購入履歴情報DB登録異常！ \r\n";
                $logstr .= "$systime $ip INFO：▲購入履歴情報登録異常終了 \r\n";
                error_log($logstr,3,'../log/gen.log');
    
                $err_cd_list[]="01";
                $_SESSION['err_cd_list']=$err_cd_list;
                $url= URL_PATH . "err.php";
                redirect($url);
            }

            mysql_close($db);
            $logstr = "$systime $ip INFO：▲購入履歴情報録正常終了！！ \r\n";
            error_log($logstr,3,'../log/gen.log');
            $url= URL_PATH . "m_history.php?action=search";
            redirect($url);
        }

		$sub_title='購入履歴管理 - 新規追加 -';
		$is_disabled="";
		$havePlaceholder='YES';
		//form項目
		$upt_date        = '';
        $upt_time_slot   = '';;
        $upt_amount      = '';;
        $upt_property_id = '';;
        $upt_brand_id    = '';;
        $upt_shop_id     = '';;
        $upt_period      = '';;
        $upt_sex         = '';;
        $upt_area_id     = '';;
        $upt_commission  = '';;
        $upt_medium      = '';;
        $upt_note        = '';;
	}
?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $sub_title; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
</head>
<body>
<div class='main'>
<div class='subtitle'><?php echo $sub_title;?></div>
<form enctype='multipart/form-data' method='post' name='upform'>
<div class='input-area'>
    <label class='w100'>日付</label>
        <input type='text' name='i_date' id='i_date' class='w200'value='<?php echo $upt_date; ?>' <?php echo $is_disabled;?>/>
    <div style='clear:both;'></div>
    <label class='w100'>時間帯</label>
        <input type='text' name='i_time' id='i_time' class='w200'value='<?php echo $upt_time_slot; ?>' <?php echo $is_disabled;?>/>
    <div style='clear:both;'></div>
    <label class='w100'>金額</label>
        <input type='text' name='i_amount' id='i_amount' class='w200'value='<?php echo $upt_amount; ?>' <?php echo $is_disabled;?>/>
    <div style='clear:both;'></div>
    <label class='w100'>紹介元属性</label>
        <select name='i_property' id='i_property' class='re_select w300' <?php echo $is_disabled;?>>
            <option value=''></option>
                <?php
                while($arr_list_row=mysql_fetch_array($result_list_property)){
                    if($arr_list_row[id]==$upt_property_id){
                        echo"<option value=".$arr_list_row[id]." selected >".$arr_list_row[rank_name]. "</option>";
                    }else{
                        echo"<option value=".$arr_list_row[id]." >".$arr_list_row[rank_name]. "</option>";
                    }
                }
                ?>
        </select>
    <div style='clear:both;'></div>
    <label class='w100'>提携先</label>
        <select name='i_brand' id='i_brand' class='re_select w200' <?php echo $is_disabled;?>>
            <option value=''></option>
                <?php
                while($arr_list_row=mysql_fetch_array($result_list_brand)){
                    if($arr_list_row[brand_id]==$upt_brand_id){
                        echo"<option value=".$arr_list_row[brand_id]." selected >".$arr_list_row[brand_name]. "</option>";
                    }else{
                        echo"<option value=".$arr_list_row[brand_id]." >".$arr_list_row[brand_name]. "</option>";
                    }
                }
                ?>
        </select>
    <div style='clear:both;'></div>
    <label class='w100'>店舗名</label>
        <select name='i_shop_name' id='i_shop_name' class='re_select w500' <?php echo $is_disabled;?>>
            <option value=''></option>
                <?php
                    while($arr_list_row=mysql_fetch_array($result_list_shop)){
                        if($arr_list_row[shop_id]==$upt_shop_id){
                            echo"<option value=".$arr_list_row[shop_id]." selected >".$arr_list_row[shop_name]. "</option>";
                        }else{
                            echo"<option value=".$arr_list_row[shop_id]." >".$arr_list_row[shop_name]. "</option>";
                        }
                }
                ?>
        </select>
    <div style='clear:both;'></div>
    <label class='w100'>年代</label>
        <select name='i_period' id='i_period' class='re_select w100 fl' <?php echo $is_disabled;?>>
            <?php 
                $arr = array("","50", "60", "70", "80", "90", "00");
                for ($i=0; $i < count($arr); $i++) { 
                    if ($arr[$i]==$upt_period) {
                        echo "<option value=".$arr[$i]." selected >".$arr[$i]. "</option>";
                    }else{
                        echo"<option value=".$arr[$i]." >".$arr[$i]. "</option>";
                    }
                }
            ?>
        </select>
    <div style='clear:both;'></div>
    <label class='w100'>性別</label>
        <select name='i_sex' id='i_sex' class='re_select' <?php echo $is_disabled;?>>
            <option value=''<?php if ($upt_sex=='') {echo 'selected';}?>></option>
            <option value='1'<?php if ($upt_sex=='1') {echo 'selected';}?>>男性</option>
            <option value='2'<?php if ($upt_sex=='2') {echo 'selected';}?>>女性</option>
        </select>
    <div style='clear:both;'></div>
    <label class='w100'>都道府県</label>
        <select name='i_area' id='i_area' class='re_select w100' <?php echo $is_disabled;?>>
            <option value=''></option>
                <?php
                while($arr_list_row=mysql_fetch_array($result_list_area)){
                    if($arr_list_row[id]==$upt_area_id){
                        echo"<option value=".$arr_list_row[id]." selected >".$arr_list_row[name]. "</option>";
                    }else{
                        echo"<option value=".$arr_list_row[id]." >".$arr_list_row[name]. "</option>";
                    }
                }
                ?>
        </select>
    <div style='clear:both;'></div>
    <label class='w100'>手数料</label>
        <input type='text' name='i_commission' id='i_commission' class='w200'value='<?php echo $upt_commission; ?>' <?php echo $is_disabled;?>/>
    <div style='clear:both;'></div>
    <label class='w100'>媒体</label>
        <input type='text' name='i_medium' id='i_medium' class='w200'value='<?php echo $upt_medium; ?>' <?php echo $is_disabled;?>/>
    <div style='clear:both;'></div>
    <label class='w100'>備考</label>
    <textarea name='i_note' id='i_note'<?php echo $is_disabled;?>><?php echo htmlspecialchars_decode($upt_note);?></textarea>
    <div style='clear:both;'></div>
    <?php
    if($action=='confirm'){
        echo "
            <input type='button' class='buttonS bGreen ml143 w200 mt40' value='訂正' onclick='editMode();'/>
            <input type='button' class='buttonS bGreen ml100 w200 mt40' value='送信' onclick='confirmSubmit($u_id);'/>
        ";
    }else{
        echo "<input type='button' class='buttonS bGreen ml143 w200 mt40' value='確認画面へ' onclick='moveConfirm($u_id);'/>";
    }
    ?>
</div>

<script type="text/javascript" language="javascript">
    function moveConfirm(u_id) {
        //日付
         if(document.upform.i_date.value == ""){
          alert("日付を入力してください。");
          document.upform.i_date.focus();
          return false;
         }
        //提携先
        if(document.upform.i_brand.value == ""){
        alert("提携先を選択してください。");
        document.upform.i_brand.focus();
        return false;
        }
        //備考
         if(document.upform.i_note.value == ""){
            alert("詳細内容を入力してください。");
            document.upform.i_note.focus();
            return false;
        }
        //submit
        document.upform.action="?action=confirm&u_id="+u_id;
        document.upform.submit();
    }
    function confirmSubmit(u_id) {
        document.getElementById('i_date').disabled=false;
        document.getElementById('i_time').disabled=false;
        document.getElementById('i_amount').disabled=false;
        document.getElementById('i_property').disabled=false;
        document.getElementById('i_brand').disabled=false;
        document.getElementById('i_shop_name').disabled=false;
        document.getElementById('i_period').disabled=false;
        document.getElementById('i_sex').disabled=false;
        document.getElementById('i_area').disabled=false;
        document.getElementById('i_commission').disabled=false;
        document.getElementById('i_medium').disabled=false;
        document.getElementById('i_note').disabled=false;

        document.upform.action="?action=insert&u_id="+u_id;
        document.upform.submit();
    }

    function editMode(){
        document.getElementById('i_date').disabled=false;
        document.getElementById('i_time').disabled=false;
        document.getElementById('i_amount').disabled=false;
        document.getElementById('i_property').disabled=false;
        document.getElementById('i_brand').disabled=false;
        document.getElementById('i_shop_name').disabled=false;
        document.getElementById('i_period').disabled=false;
        document.getElementById('i_sex').disabled=false;
        document.getElementById('i_area').disabled=false;
        document.getElementById('i_commission').disabled=false;
        document.getElementById('i_medium').disabled=false;
        document.getElementById('i_note').disabled=false;

        document.upform.action="?action=edit";
        document.upform.submit();
    }
</script>
</form>
</div>
</body>
</html>