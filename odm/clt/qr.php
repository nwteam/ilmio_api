<?php
//引入phpqrcode库文件
include('../util/phpqrcode/phpqrcode.php');
// 二维码数据
$data = '{"qr":{"Shop_id":"8","Brand_id":"2"}}';
// 生成的文件名
$filename = 'qr.png';
// 纠错级别：L、M、Q、H
$errorCorrectionLevel = 'L';
// 点的大小：1到10
$matrixPointSize = 4;
//创建一个二维码文件
QRcode::png($data, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
//输入二维码到浏览器
QRcode::png($data);
?>