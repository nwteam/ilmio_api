<?php
switch($a)
{
	case 'shop_info':
		check_parameter(array($shop_id));
		$shop_id = intval($shop_id);
		check_sign($_GET,$sign);
		
		$info = get_shop_info($shop_id);
		
		if(empty($info))
		{
			rencode( '店舗データが存在しませんでした。管理員へのご連絡お願いします。',1,'msg');
		}
		
		$brand = get_brand_info($info['brand_id']);
		$info['brand_logo'] = $brand['brand_logo'];
		rencode( $info,0);
		break;
		
	case 'search_shop':
		check_parameter(array($keyword));
		$keyword = trim($keyword);
		$rank_id = trim($rank_id);
		check_sign($_GET,$sign);
		
		$list = search_shop_mysqli($keyword,$rank_id);
		
		if(empty($list))
		{
			rencode( '店舗データが存在しませんでした。管理員へのご連絡お願いします。',1,'msg');
		}
		
		rencode( $list,0);
		
		break;
	
	//按经纬度搜索附近的店铺
	case 'nearby_shop' :
		check_parameter(array($latitude,$longitude));
		$latitude = trim($latitude);//纬度
		$longitude = trim($longitude);//经度
		$rank_id = trim($rank_id);
		check_sign($_GET,$sign);
		
		
		$list = nearby_shop_mysqli($longitude,$latitude,$rank_id);
		
		if(empty($list))
		{
			rencode( 'あなたの付近では店舗見つかりませんでした。',1,'msg');
		}
		
		foreach($list as $key => $r)
		{
			unset($list[$key]['distance']);
		}
		
		rencode( $list,0);
		break;
		
	
}

?>