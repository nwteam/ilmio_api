<?php
switch($a)
{
	case 'brand_list':
		check_parameter(array($cat_id));
		$cat_id = intval($cat_id);
		$brand_id = intval($brand_id);
		check_sign($_GET,$sign);
		$list = get_brand_list($cat_id,$rank_id);
		
		if(empty($list))
		{
			rencode( 'ブランドデータが存在しませんでした。管理員へのご連絡お願いします。',1,'msg');
		}
		rencode( $list,0);
		
		break;
		
	case 'brand_info':
		check_parameter(array($brand_id));
		$brand_id = intval($brand_id);
		$rank_id = trim($rank_id);
		
		check_sign($_GET,$sign);
		
		$info = get_brand_info($brand_id);
		
		
		
		
		if(empty($info))
		{
			rencode( 'ブランドデータが存在しませんでした。管理員へのご連絡お願いします。',1,'msg');
		}
		
		$info['kp_code'] = get_kp_code($brand_id,$rank_id);
		$info['bl_code'] = get_bl_code($brand_id,$rank_id);
		
		$login_id = substr($info['login_id'],strlen($info['login_id'])-4);
		
		if($login_id == '_web')
		{
			$info['qr_flg'] = 1;
		}else{
			$info['qr_flg'] = 0;
		}
		
		rencode( $info,0);
		
		break;
		
	case 'brand_list_index':
		check_parameter(array($type));
		$type = trim($type);
		$brand_id = intval($brand_id);
		check_sign($_GET,$sign);
		$list = get_brand_list_index($rank_id);
		
		rencode( $list,0);
		
		break;
		
	case 'search_brand_list':
		check_parameter(array($type));
		$type = trim($type);
		$rank_id = trim($rank_id);
		check_sign($_GET,$sign);
		$list = get_brand_list_search($rank_id);
		
		rencode( $list,0);
		break;
		
	//品牌下店铺列表
	case 'brand_shop_list' :
		check_parameter(array($brand_id));
		$brand_id = intval($brand_id);
		check_sign($_GET,$sign);
		
		
		$list = get_shop_list_by_brand_id($brand_id);
		
		if(empty($list))
		{
			rencode( '店舗データが存在しませんでした。管理員へのご連絡お願いします。',1,'msg');
		}
		
		foreach($list as $key => $r)
		{
			$arr[$key]['shop_name'] = $r['shop_name'];
			$arr[$key]['address'] = $r['address'];
			$arr[$key]['shop_id'] = $r['shop_id'];
		}
		rencode( $arr,0);
		break;
		
	case 'brand_search' :
		check_parameter(array($keyword));
		$keyword = trim($keyword);
		$rank_id = trim($rank_id);
		check_sign($_GET,$sign);
		
		$list = search_brand_list($keyword,$rank_id);
		if(empty($list))
		{
			rencode( 'ブランドデータが存在しませんでした。管理員へのご連絡お願いします。',1,'msg');
		}
		
		rencode( $list,0);
		break;

	case 'brand_list_index_all':
		//check_version($version_chk_flg);
		if($version_chk_flg=="2"){
			if(!check_new_version($ver)){
				rencode( '最新バージョンのアプリが公開されています。最新のアプリにアップデートしましょう！',1,'msg');
			}else{
				check_parameter(array($type));
				$type = trim($type);
				$brand_id = intval($brand_id);
				check_sign($_GET,$sign);
				$list = get_brand_list_index_all();
				rencode( $list,0);
			}
		}
		if($version_chk_flg=="1"){
			check_parameter(array($type));
			$type = trim($type);
			$brand_id = intval($brand_id);
			check_sign($_GET,$sign);
			$list = get_brand_list_index_all();

			rencode( $list,0);
		}else{
			rencode( '最新バージョンのアプリが公開されています。最新のアプリにアップデートしましょう！',1,'msg');
		}

		break;

		
	
}

?>