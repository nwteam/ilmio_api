<?php
switch($a)
{
	case 'news_list':
		check_parameter(array($page));
		$page = intval($page);
		check_sign($_GET,$sign);
		$page_number = 10;
		
		$list = get_news_list($page,$page_number);
		
		$total_number = get_new_number();
		$l = ($total_number%$page_number) > 0 ? 1 : 0;
		$total_page = intval($total_number/$page_number) + $l;
		
		$arr['news_list'] = $list;
		$arr['total_page'] = $total_page;
		
		rencode( $arr,0);
		break;
		
	case 'news_info':
		check_parameter(array($id));
		$id = intval($id);
		check_sign($_GET,$sign);
		
		$info = get_news_info($id);
		
		$info['supply_time_from'] = ($info['supply_time_from'] <= 0) ? 0 : date('Y-m-d H:i:s',$info['supply_time_from']);
		$info['supply_time_to'] = ($info['supply_time_to'] <= 0) ? 0 : date('Y-m-d H:i:s',$info['supply_time_to']);
		$info['online_date'] = ($info['online_date'] <= 0) ? 0 : date('Y-m-d H:i:s',$info['online_date']);
		$info['insert_time'] = ($info['insert_time'] <= 0) ? 0 : date('Y-m-d H:i:s',$info['insert_time']);
		$info['update_time'] = ($info['update_time'] <= 0) ? 0 : date('Y-m-d H:i:s',$info['update_time']);
		
		rencode( $info,0);
		
		break;

		
	
}

?>