<?php
switch($a)
{
	case 'register':
		check_parameter(array($birthday,$sex,$address));
		check_sign($_GET,$sign);
		
		//$email 			= trim($email);
		$building_type	= trim(urldecode($building_type));
		$device_id		= trim($device_id);
		$device_type	= intval($device_type);//1 iphone  2 android
		$push_flg		= intval($push_flg);//0：On（默认值）1：Off
		$birthday 		= trim($birthday);//生日 格式 1999-01-11
		$sex 			= ($sex == 1)? 1:0; //性别 1男  0女
		$address 		= trim(urldecode($address));//地址
		$rank_id 		= trim(urldecode($rank_id));//地址
		
		
		
		
		$user_id = add_user($birthday,$sex,$address,$building_type,$device_id,$device_type,$push_flg,$rank_id);
		
		
		
		if($user_id > 0)
		{
			
			rencode( $user_id,0,'user_id');
		}else{
			rencode( 0,1,'user_id');
		}
		
		break;
		
	case 'check_email':
		check_parameter(array($email));
		$email 			= trim($email);
		check_sign($_GET,$sign);
		
		if(!check_email($email))
		{
			rencode( 'メールアドレスが既に存在しました。',1,'msg');
		}else{
			rencode( 'メールアドレスがご利用できます。',0,'msg');
		}
		
		break;
	case 'get_login_code':
		check_parameter(array($id));
		$id 			= intval($id);
		check_sign($_GET,$sign);

		$rank_info = get_rank_info_by_id($id);
		rencode( $rank_info,0);
		break;
	case 'check_user_rank':
		check_parameter(array($rank_id));
		$rank_id 			= trim($rank_id);
		check_sign($_GET,$sign);

		$info_rank = get_rank_info($rank_id);
		if(empty($info_rank))
		{
			$new_rank_id 	= substr($rank_id,0,3).date('y',time()).str_pad(substr($rank_id,-1,3),3,'0',STR_PAD_LEFT);
			$rank_id		=$new_rank_id;
		}
		
		//验证rank_id
		$rank_check = check_rank_id($rank_id);
		
		if($rank_check)
		{
			$info = get_rank_info($rank_id);
			$info['expired_date_from'] = date('Y-m-d',$info['expired_date_from']);
			$info['expired_date_to'] = date('Y-m-d',$info['expired_date_to']);
			$info['msg'] = '成功しました。';
			rencode( $info,0);
		}
		
		break;

	case 'user_info':
		check_parameter(array($user_id));
		$user_id 		= intval($user_id);
		check_sign($_GET,$sign);
		$info = get_user_info($user_id);
		
		rencode( $info,0);
		break;
		
	case 'update_user_info':

		check_parameter(array($user_id));
		check_sign($_GET,$sign);

		$user_id 		= intval(html_tag_chg($user_id));
		$building_type	= html_tag_chg(SBC_DBC(trim(urldecode($building_type)),0));
		$device_id		= html_tag_chg(SBC_DBC(trim($device_id),0));
		$device_type	= intval(html_tag_chg($device_type));//1 iphone  2 android
		$push_flg		= intval(html_tag_chg($push_flg));//0：On（默认值）1：Off
		$birthday 		=html_tag_chg(SBC_DBC( trim($birthday),0));//生日 格式 1999-01-11
		$sex 			= (html_tag_chg($sex) == 1)? 1:0; //性别 1男  0女
		$address 		= html_tag_chg(SBC_DBC(trim(urldecode($address)),0));//地址
		$rank_id 		= trim($rank_id);//地址
		$rank_id		=html_tag_chg(SBC_DBC($rank_id,0));
		$info_rank = get_rank_info($rank_id);
		if(empty($info_rank))
		{
			$new_rank_id 	= substr($rank_id,0,3).date('y',time()).str_pad(substr($rank_id,-1,3),3,'0',STR_PAD_LEFT);
			$rank_id		=$new_rank_id;
		}
		$up_flg 		= intval(html_tag_chg($up_flg));//1 会員種別  2 ユーザー情報  3 通知
		
		//验证rank_id
		check_rank_id($rank_id);
		if($up_flg=="1"){
			$var = update_rank_id($user_id,$rank_id);
		}elseif($up_flg=="2"){
			$var = update_user_info($user_id,$birthday,$sex,$address,$building_type,$device_id,$device_type);
		}elseif($up_flg=="3"){
			$var = update_push_flg($user_id,$push_flg);
		}else{
			$var = update_user($birthday,$sex,$address,$building_type,$device_id,$device_type,$push_flg,$user_id,$rank_id);
		}

		$info = get_rank_info($rank_id);
		if($var)
		{
			rencode( $info,0);
		}else{
			rencode( 'エラーが発生しました。しばらく待ってから再度試してみてください',1,'msg');
		}
		break;
		
	case 'history_info':
		check_parameter(array($shop_id,$brand_id,$date,$time_sort,$sex,$medium));
		check_sign($_GET,$sign);
		$rep_month=date('Y年m月',time());
		$date=time();
		$time_sort=date('H:i:s',time());
		$arr = array(
			'date' => $date,
			'time_slot' => $time_sort,
			'amount' => 0,
			'commission' => 0,
			'property_id' => $property_id,
			'brand_id' => $brand_id,
			'shop_id' => $shop_id,
			'period' => $period,
			'sex' => $sex,
			'area_id' => $area_id,
			'medium' => $medium,
			'note' => '',
			'del_flg' => '0',
			'rank_id' => $rank_id,
			'address' => $address,
			'rep_month' => $rep_month,
			'insert_time' => time(),
		);
		
		//验证rank_id
		check_rank_id($rank_id);
		
		$db->autoExecute('den_history_info',$arr,'INSERT');
		
		$shop_info = get_shop_info($shop_id);
		$brand_info = get_brand_info($brand_id);
		
		
		$res['shop_id'] = $shop_id;
		$res['brand_id'] = $shop_info['brand_id'];
		$res['brand_logo'] = $brand_info['brand_logo'];
		$res['brand_image'] = $brand_info['brand_image'];
		$res['discount_rate'] = $brand_info['discount_rate'];
		$res['address'] = $shop_info['shop_name'];
		$res['barcode'] = $shop_info['barcode'];;
		rencode( $res,0);
		break;
		
		
	
}

?>