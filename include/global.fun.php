<?php

/** 
 * public function
 * $Author: zoubing 
 * 
*/


/* ------------------------------  用户部分  --------------------------- */


//检测邮箱是否已被注册
function check_email($eamil)
{
	global $db;
	
	$mail = $db->getOne("select email from users where email='$eamil'");
	
	if($mail > 0)
	{
		return false;//已经注册
	}else{
		return true;//未注册
	}
}

//检查邮箱是否合法
function check_email_format($email)
{
	 if(preg_match('/^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/',$email))
	 {
	 	return true;//验证通过
	 }else{
	 	return false;//验证是吧
	 }
}
//添加用户
function add_user($birthday,$sex,$address,$building_type,$devicetoken,$phone_type,$push_flg,$rank_id)
{
	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

	if ($mysqli->connect_errno) {
		echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}
	$mysqli->query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");

	if (!($stmt = $mysqli->prepare("INSERT INTO users (birthday,sex,address,building_type,device_id,device_type,push_flg,rank_id,reg_time) VALUES (?,?,?,?,?,?,?,?,?)"))) {
		echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
	}

	if (!$stmt->bind_param("sssssssss", $birthday,$sex,$address,$building_type,$devicetoken,$phone_type,$push_flg,$rank_id,time())) {
		echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
	}


	if (!$stmt->execute()) {
		echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
	}

	//$arr = array('birthday'=>$birthday,'sex'=>$sex,'address'=>$address,'building_type'=>$building_type,
	//'device_id'=>$devicetoken,'device_type'=>$phone_type,'push_flg'=>$push_flg,'rank_id'=>$rank_id,'reg_time'=>time());

	//$db->autoExecute('users',$arr,'INSERT');

	return mysqli_insert_id($mysqli);
}

//获取用户等级信息
function get_rank_info($rank_id)
{
	global $db;

	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

	if ($mysqli->connect_errno) {
		echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}
	$mysqli->query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");

//	if (!($stmt = $mysqli->prepare("select id,rank_id,rank_name,expired_date_from,expired_date_to from user_rank where rank_id=?"))) {
//		echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
//	}
	$rank_id1=substr($rank_id,0,3)."%";
	$rank_id2="%".str_pad(substr($rank_id,-1,3),3,'0',STR_PAD_LEFT);
	if (!($stmt = $mysqli->prepare("select id,rank_id,rank_name,expired_date_from,expired_date_to from user_rank where rank_id like ? and rank_id like ? "))) {
		echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
	}

	if (!$stmt->bind_param("ss", $rank_id1,$rank_id2)) {
		echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
	}

	if (!$stmt->execute()) {
		echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	$res = $stmt->get_result();

	//$rank_id 		= mysqli_real_escape_string(SBC_DBC($rank_id,0));

	//return $db->getRow("select id,rank_id,rank_name,expired_date_from,expired_date_to from user_rank where rank_id='$rank_id'");
	return mysqli_fetch_assoc($res);
	
}
function get_rank_info_by_id($id)
{
	global $db;

	return $db->getRow("select id,rank_id,rank_name,expired_date_from,expired_date_to from user_rank where id='$id'");

}

//获取用户信息
function get_user_info($user_id)
{
	global $db;
	
	return $db->getRow("select sex,birthday,address,building_type,push_flg,device_id,device_type,rank_id from users where user_id='$user_id'");
}

function update_user($birthday,$sex,$address,$building_type,$device_id,$device_type,$push_flg,$user_id,$rank_id)
{
	global $db;

	$building_type	= mysqli_real_escape_string($building_type);
	$device_id		= mysqli_real_escape_string($device_id);
	$birthday 		= mysqli_real_escape_string($birthday);
	$address 		= mysqli_real_escape_string($address);
	$rank_id 		= mysqli_real_escape_string($rank_id);

	$arr = array('birthday'=>$birthday,'sex'=>$sex,'address'=>$address,'building_type'=>$building_type,'device_type'=>$device_type,'push_flg'=>$push_flg,'device_id'=>$device_id,'rank_id'=>$rank_id);
	
	return $db->autoExecute('users',$arr,'UPDATE'," user_id= '$user_id'");
}
function update_rank_id($user_id,$rank_id)
{
	global $db;
	
	$arr = array('rank_id'=>$rank_id);
	
	return $db->autoExecute('users',$arr,'UPDATE'," user_id= '$user_id'");
}
function update_user_info($user_id,$birthday,$sex,$address,$building_type,$device_id,$device_type)
{
	global $db;
	
	$arr = array('birthday'=>$birthday,'sex'=>$sex,'address'=>$address,'building_type'=>$building_type,'device_type'=>$device_type);
	
	return $db->autoExecute('users',$arr,'UPDATE'," user_id= '$user_id'");
}
function update_push_flg($user_id,$push_flg)
{
	global $db;
	
	$arr = array('push_flg'=>$push_flg);
	
	return $db->autoExecute('users',$arr,'UPDATE'," user_id= '$user_id'");
}

//验证rank_id
function check_rank_id($rank_id)
{
	$time = time();
	$info = get_rank_info($rank_id);

	if(empty($info))
	{
		rencode( 'ログインコードが正しくありません。',1,'msg');
	}else{
		if($info['expired_date_from'] > $time || $info['expired_date_to'] < $time)
		{
			rencode( 'ログインコードの有効期限が切れています。設定画面にて有効なログインコードを設定してください。',1,'msg');
		}
	}

	return true;
}


/* ------------------------------  分类部分  --------------------------- */

//获取分类列表
function get_cat_list()
{
	global $db;
	
	return $db->getAll("select cat_id,cat_name from category");
}

//获取单个分类信息
function get_cat_info($cat_id)
{
	global $db;
	
	return $db->getRow("select cat_id,cat_name,keywords,cat_desc,parent_id from category where cat_id='$cat_id'");
}



//获取分类名称
function get_cat_names($cat_id)
{
	global $db;
	if(!empty($cat_id))
	{
		$list = $db->getAll("select cat_name from category where cat_id in($cat_id)");
		foreach($list as $key => $r)
		{
			$str .= $r['cat_name'].' ';
		}

		return $str;
	}else{
		return false;
	}
}

function get_cat_name($cat_id)
{
	global $db;
	return $db->getOne("select cat_name from category where cat_id ='$cat_id'");
}

/* ------------------------------  品牌部分  --------------------------- */

//获取品牌列表
function get_brand_list($cat_id,$rank_id='')
{
	global $db;
	
	if(!empty($rank_id))
	{
		$condition = " and find_in_set('$rank_id',property_id)";
	}else{
		$condition = '';
	}
	
	if($cat_id == 1)
	{
		$list = $db->getAll("select b.brand_id,b.brand_name,b.brand_image,ur.sort_code,c.cat_id from brand as b,category as c,(select drs.* from user_rank ur, den_rank_sort drs where ur.rank_id=drs.rank_id and ur.id=$rank_id) as ur where b.cat_id=c.cat_id and ur.brand_id=b.brand_id and b.status=1 $condition order by ur.sort_code asc limit 60");

		foreach($list as $key => $r)
		{
			$ss = explode(',',$r['cat_id']);
			$catid = intval($ss[0]);
			unset($list[$key]['cat_id']);
			$list[$key]['cat_name'] = get_cat_name($catid);
		}
	}else{
		$list = $db->getAll("select b.brand_id,b.brand_name,b.brand_image,ur.sort_code from brand as b,category as c,(select drs.* from user_rank ur, den_rank_sort drs where ur.rank_id=drs.rank_id and ur.id=$rank_id) as ur where b.cat_id=c.cat_id and ur.brand_id=b.brand_id and b.status=1 and FIND_IN_SET('$cat_id',b.cat_id) $condition order by ur.sort_code asc limit 60 ");
		$cat_name = get_cat_name($cat_id);
		foreach($list as $key => $r)
		{
			$list[$key]['cat_name'] = $cat_name;
		}
	}
	return $list;
}

//获取品牌信息
function get_brand_info($brand_id)
{
	global $db;
	
	//return $db->getRow("select b.*,c.cat_name from brand as b,category as c left join den_new_info as n on n.supply_id= b.brand_id where b.cat_id=c.cat_id and b.brand_id='$brand_id' and b.status=1");
	return $db->getRow("select b.*,c.cat_name,n.* from brand as b inner join category as c on b.cat_id=c.cat_id left join (select * from den_new_info where del_flg = 0 and supply_id ='$brand_id' and status ='1' order by id desc limit 0,1) as n on n.supply_id=b.brand_id where b.brand_id='$brand_id' and b.status=1");
}

//获取kp_code
function get_kp_code($brand_id,$rank_id)
{
	global $db;
	
	return $db->getOne("SELECT kp_code FROM den_kp_code WHERE brand_id='$brand_id' AND rank_id ='$rank_id'");
}

//获取bl_code
function get_bl_code($brand_id,$rank_id)
{
	global $db;

	return $db->getOne("SELECT bl_code FROM den_kp_code WHERE brand_id='$brand_id' AND rank_id ='$rank_id'");
}

//获取app首页品牌列表
function get_brand_list_index($rank_id)
{
	global $db;
	
	if(!empty($rank_id))
	{
		$condition = " and find_in_set('$rank_id',property_id)";
	}else{
		$condition = '';
	}
	
	return $db->getAll("select b.brand_id,b.brand_name,b.brand_image,ur.sort_code,c.cat_name from brand as b,category as c,(select drs.* from user_rank ur, den_rank_sort drs where ur.rank_id=drs.rank_id and ur.id=$rank_id) as ur where b.cat_id=c.cat_id and ur.brand_id=b.brand_id and b.status=1 $condition order by ur.sort_code asc limit 60");
}
//获取app首页品牌列表(ログインしない場合)
function get_brand_list_index_all()
{
	global $db;

	return $db->getAll("select b.brand_id,b.brand_name,b.brand_image,c.cat_name from brand as b,category as c where b.cat_id=c.cat_id and b.status=1 order by b.brand_id asc limit 60");
}
function get_brand_list_search($rank_id)
{
	global $db;
	
	if(!empty($rank_id))
	{
		$condition = " and find_in_set('$rank_id',b.property_id)";
	}else{
		$condition = '';
	}

	return $db->getAll("select b.brand_id,b.brand_name,b.brand_image,ur.sort_code,c.cat_name from brand as b,category as c,(select drs.* from user_rank ur, den_rank_sort drs where ur.rank_id=drs.rank_id and ur.id=$rank_id) as ur where b.cat_id=c.cat_id and ur.brand_id=b.brand_id and b.status=1 $condition order by ur.sort_code asc limit 60");
}
//根据品牌名称搜索获取品牌id
function search_brand_id($keyword)
{
	global $db;
	
	return $db->getOne("select brand_id from brand where brand_name like '%$keyword%' and status=1");
}

//根据品牌名称搜索多个品牌id，拼成sql形式
function search_brand_ids($keyword)
{
	global $db;
	
	$ids = $db->getAll("select brand_id from brand where brand_name like '%$keyword%' and status=1");
	
	$arr = array();
	if(!empty($ids))
	{
		foreach($ids as $r)
		{
			$arr[] = $r['brand_id'];
		}
	}
	
	return implode(',',$arr);
}

//根据关键词搜索品牌，返回列表
function search_brand_list($keyword,$rank_id)
{
	global $db;

	if(!empty($rank_id))
	{
		$condition = " and find_in_set('$rank_id',property_id)";
	}else{
		$condition = '';
	}
	$query = "select brand_id from brand where brand_name like ? $condition";
	//$query = "select brand_id from brand where 1 $condition";

	$stmt = $mysqli->prepare($query);
	$keyword = "%" . $keyword . "%";
	$stmt->bind_param('s',$keyword);
	$stmt->execute();
	$res = $stmt->get_result();
	$ids = $res->fetch_all();
	$arr = array();
	if(!empty($ids))
	{
		foreach($ids as $r)
		{
			$arr[] = $r['0'];
		}
	}

	$brand_ids = implode(',',$arr);

	if(!empty($brand_ids))
	{
		$sql = "select s.shop_id,s.shop_name,s.address,s.brand_id from shop as s left join brand as b on b.brand_id=s.brand_id where (s.shop_name like ? or s.address like ? or b.brand_name like ?) and s.brand_id in (?) and s.status=1 and s.del_flg=0";
		$stmt = $mysqli->prepare($sql);
		//$brand_ids = $brand_ids;
		$keyword = "%" . $keyword . "%";
		$stmt->bind_param('ssss',$keyword,$keyword,$keyword,$brand_ids);
	}else{
		$sql = "select s.shop_id,s.shop_name,s.address,s.brand_id from shop as s left join brand as b on b.brand_id=s.brand_id where (s.shop_name like ? or s.address like ? or b.brand_name like ?) and s.status=1 and s.del_flg=0";
		$stmt = $mysqli->prepare($sql);
		$keyword = "%" . $keyword . "%";
		$stmt->bind_param('sss',$keyword,$keyword,$keyword);
	}

	$stmt->execute();
	$res = $stmt->get_result();
	$result = $res->fetch_all();

	foreach($result as $key => $r)
	{
		$arrs[$key]['shop_id'] = $r[0];
		$arrs[$key]['shop_name'] = $r[1];
		$arrs[$key]['address'] = $r[2];
		$arrs[$key]['brand_id'] = $r[3];
	}

	return $arrs;
	//return $db->getAll("select brand_id,brand_name,brand_note,brand_logo from brand where brand_name like '%$keyword%' and status=1 $condition order by brand_name asc");
}



/* ------------------------------  店铺部分  --------------------------- */

//根据品牌id获取商店列表
function get_shop_list_by_brand_id($brand_id)
{
	global $db;
	
	return $db->getAll("select * from shop where brand_id = '$brand_id' and status=1 and del_flg=0");
}

//获取商店详情
function get_shop_info($shop_id)
{
	global $db;
	
	return $db->getRow("select * from shop where shop_id='$shop_id' and status=1 and del_flg=0");
}
//搜索店铺列表
function search_shop_mysqli($keyword,$rank_id)
{

	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

	if ($mysqli->connect_errno) {
		echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}
	$mysqli->query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");

	if(!empty($rank_id))
	{
		$condition = " and find_in_set('$rank_id',property_id)";
	}else{
		$condition = '';
	}

	$query = "select brand_id from brand where brand_name like ? $condition";
//	$query = "select brand_id from brand where 1 $condition";

	$stmt = $mysqli->prepare($query);
	$keyword = "%" . $keyword . "%";
	$stmt->bind_param('s',$keyword);
	$stmt->execute();
	$res = $stmt->get_result();
	$ids = $res->fetch_all();
	$arr = array();
	if(!empty($ids))
	{
		foreach($ids as $r)
		{
			$arr[] = $r['0'];
		}
	}

	$brand_ids = implode(',',$arr);

	if(!empty($brand_ids))
	{
		$sql = "select s.shop_id,s.shop_name,s.address,s.brand_id from shop as s left join brand as b on b.brand_id=s.brand_id where (s.shop_name like ? or s.address like ? or b.brand_name like ?) and s.brand_id in (?) and s.status=1 and s.del_flg=0";
		$stmt = $mysqli->prepare($sql);
		//$brand_ids = $brand_ids;
		$stmt->bind_param('ssss',$keyword,$keyword,$keyword,$brand_ids);

	}else{
		$sql = "select s.shop_id,s.shop_name,s.address,s.brand_id from shop as s left join brand as b on b.brand_id=s.brand_id where (s.shop_name like ? or s.address like ? or b.brand_name like ?) and s.status=1 and s.del_flg=0";
		$stmt = $mysqli->prepare($sql);

		$stmt->bind_param('sss',$keyword,$keyword,$keyword);
	}


	$stmt->execute();
	$res = $stmt->get_result();
	$result = $res->fetch_all();

	foreach($result as $key => $r)
	{
		$arrs[$key]['shop_id'] = $r[0];
		$arrs[$key]['shop_name'] = $r[1];
		$arrs[$key]['address'] = $r[2];
		$arrs[$key]['brand_id'] = $r[3];
	}

	return $arrs;
	//return $db->getAll("select shop_id,shop_name,address,brand_id from shop where shop_name like '%$keyword%' or address like '%$keyword%' $brand_where");
}
//搜索店铺列表
function search_shop($keyword)
{
	global $db;
	
	$brand_ids = search_brand_ids($keyword);
	if(!empty($brand_ids))
	{
		$brand_where = "or brand_id in ($brand_ids)";
	}else{
		$brand_where = '';
	}
	
	return $db->getAll("select shop_id,shop_name,address,brand_id from shop where (shop_name like '%$keyword%' or address like '%$keyword%' $brand_where)  and status=1 and del_flg=0");
}

//按经纬度搜索店铺
function nearby_shop($longitude , $latitude)
{
	global $db;

	return $db->getAll("select * from (SELECT shop_id,shop_name,shop_note,brand_id,longitude,latitude, (POWER( MOD( ABS( longitude - '$longitude') , 360 ) , 2 ) + POWER( ABS( latitude - '$latitude') , 2 )) *1000000 AS distance
			FROM  `shop`  where status = 1  and del_flg=0 ORDER BY distance   LIMIT 10) as c where c.distance < 1200");
}

//按经纬度搜索店铺 /10km 1200 / 20km 3200 / 30km 7200 / 40km 13000 
function nearby_shop_mysqli($longitude , $latitude , $rank_id)
{
	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

	if ($mysqli->connect_errno) {
		echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}
	$mysqli->query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");

	if(!empty($rank_id))
	{
		$condition = " and find_in_set('$rank_id',b.property_id)";
	}else{
		$condition = '';
	}
	
	$query = "select * from (SELECT s.shop_id,s.shop_name,s.shop_note,s.brand_id,s.longitude,s.latitude, (POWER( MOD( ABS( s.longitude - ?) , 360 ) , 2 ) + POWER( ABS( s.latitude - ?) , 2 )) *100000 AS distance
			FROM  `shop` as s left join brand as b on b.brand_id=s.brand_id where s.status = 1 and s.del_flg=0 $condition ORDER BY distance ) as c where c.distance < 7200";
	$stmt = $mysqli->prepare($query);
	$stmt->bind_param('ss',$longitude,$latitude);
	$stmt->execute();
	$res = $stmt->get_result();
	$res = $res->fetch_all();

	foreach($res as $key => $r)
	{
		$arrs[$key]['shop_id'] = $r[0];
		$arrs[$key]['shop_name'] = $r[1];
		$arrs[$key]['shop_note'] = $r[2];
		$arrs[$key]['brand_id'] = $r[3];
		$arrs[$key]['longitude'] = $r[4];
		$arrs[$key]['latitude'] = $r[5];
	}

	return $arrs;

}


/* ------------------------------  新闻部分  --------------------------- */
function get_page_sql($page,$page_number = 10)
{
	$page = ($page > 0) ? $page : 1;
	$start = ($page-1)*$page_number;
	return $limit = " limit $start,$page_number";
}

function get_news_list($page,$page_number)
{
	global $db;
	
	$limit = get_page_sql($page,$page_number);
	
	return $db->getAll("select n.id,n.title,left(n.contents,18) as contents,i.brand_name as sender,from_unixtime(n.insert_time,'%m月%d日') as insert_time from den_new_info as n,brand as i where n.supply_id=i.brand_id and n.status=1 and i.del_flg=0 and i.status=1 order by id desc $limit");
}

function get_new_number()
{
	global $db;
	
	return $db->getOne("select count(*) from den_new_info where status=1 ");
}

function get_news_info($id)
{
	global $db;
	
	return $db->getRow("select n.*,i.brand_name as sender from den_new_info as n,brand as i where n.supply_id=i.brand_id and n.status=1 and i.del_flg=0 and i.status=1 and n.id='$id'");
}


?>