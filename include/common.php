<?php

/**
 * 
 * public function
 * $Author: zoubing 
 * 
*/


function rencode($arr,$error = 0,$name = '')
{
	if($name == '')$name = 'list';
	if(empty($arr))$error = -1;
	$res['result']['error'] = "$error";
	$res['result'][$name] = $arr;
	
	
	
	die(encodes($res));
}

function check_sign($arr,$get_sign)
{
	$secret_key = 'thisisajapanesekey';//私钥
	
    ksort($arr);//按参数名首字母正序排列
    reset($arr);//将指针移至首位

    //此处$arr为排序后的请求参数数组，遍历数组以“参数名=参数值&”的方式拼得原始加密串，注：a、c、sign三个参数不参与加密
    $sign = '';
    foreach ($arr AS $key=>$val)
    {
	    if ($key != 'sign' && $key != 'a' && $key != 'c')
	    {
	    	$sign .= "$key=$val&";
	    }
    }
    substr($sign, 0, -1) . $secret_key;
    $sign = substr($sign, 0, -1) . $secret_key;//去掉最后一个&符后拼接上secret_key
    
    $temp_sign = strtolower(md5($sign));//md5加密并转小写，得到最终的加密串
    //echo $temp_sign;
    $get_sign = strtolower($get_sign);
    
	if($temp_sign == $get_sign)
	{
		return true;
	}else{
		rencode( 'signが正しくありません。',1,'msg');
	}
	
}

function SBC_DBC($str,$args2) { //0:半角->全角；1，全角->半角
    $DBC = Array( '＆','＜' , '＞' , '＂' , '＇','｜');
  $SBC = Array('&','<', '>', '"', '\'','|');
 if($args2==0)
  return str_replace($SBC,$DBC,$str);  //半角->全角
 if($args2==1)
  return str_replace($DBC,$SBC,$str);  //全角->半角
 else
  return false;
}
function html_tag_chg($val){
	$val = str_replace("onchange","ｏｎｃｈａｎｇｅ", $val);
	$val = str_replace("onmouseover","ｏｎｍｏｕｓｅｏｖｅｒ", $val);
	$val = str_replace("onload","ｏｎｌｏａｄ", $val);
	$val = str_replace("onerror","ｏｎｅｒｒｏｒ", $val);
	$val = str_replace("javascript","ｊａｖａｓｃｒｉｐｔ", $val);
	$val = str_replace("document","ｄｏｃｕｍｅｎｔ", $val);
	$val = str_replace("onclick","ｏｎｃｌｉｃｋ", $val);
	$val = str_replace("alert","ａｌｅｒｔ", $val);
	$val = str_replace("script","ｓｃｒｉｐｔ", $val);

	return $val;
}
function check_parameter($arr)
{
	foreach($arr as $key => $r)
	{
		if($r == '')
		{
			rencode('ログインコードを入力してください。',1,'msg');
		}
	}
}

function curl($path)
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, "$path");
    curl_setopt($curl, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 6.1; zh-CN; rv:1.9.2.10) Gecko/20100914 Firefox/3.6.10 QQDownload/1.7');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $contents = curl_exec($curl);
    curl_close($curl);
    return $contents;
}


function sdate($str)
{
	return date("$str",time());
}

function encodes($arg, $force = true)
    {
        static $_force;
        if (is_null($_force))
        {
            $_force = $force;
        }

        if ($_force && function_exists('json_encode'))
        {
            return json_encode($arg);
        }

        $returnValue = '';
        $c           = '';
        $i           = '';
        $l           = '';
        $s           = '';
        $v           = '';
        $numeric     = true;

        switch (gettype($arg))
        {
            case 'array':
                foreach ($arg AS $i => $v)
                {
                    if (!is_numeric($i))
                    {
                        $numeric = false;
                        break;
                    }
                }

                if ($numeric)
                {
                    foreach ($arg AS $i => $v)
                    {
                        if (strlen($s) > 0)
                        {
                            $s .= ',';
                        }
                        $s .= encodes($arg[$i]);
                    }

                    $returnValue = '[' . $s . ']';
                }
                else
                {
                    foreach ($arg AS $i => $v)
                    {
                        if (strlen($s) > 0)
                        {
                            $s .= ',';
                        }
                        $s .= encodes($i) . ':' . encodes($arg[$i]);
                    }

                    $returnValue = '{' . $s . '}';
                }
                break;

            case 'object':
                foreach (get_object_vars($arg) AS $i => $v)
                {
                    $v = encodes($v);

                    if (strlen($s) > 0)
                    {
                        $s .= ',';
                    }
                    $s .= encodes($i) . ':' . $v;
                }

                $returnValue = '{' . $s . '}';
                break;

            case 'integer':
            case 'double':
                $returnValue = is_numeric($arg) ? (string) $arg : 'null';
                break;

            case 'string':
                $returnValue = '' . json_encode($arg) . '';
                break;

            case 'boolean':
                $returnValue = $arg?'true':'false';
                break;

            default:
                $returnValue = 'null';
        }

        return $returnValue;
    }


?>
