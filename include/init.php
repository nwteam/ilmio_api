<?php
define('IN_ECS', true);
error_reporting(E_ALL);
date_default_timezone_set('PRC');
header("Content-type: text/html; charset=utf-8");
require('config.php');
require('common.php');
require('global.fun.php');
require('filter.php');


extract($_REQUEST);

if (PHP_VERSION >= '5.1' && !empty($timezone))
{
    date_default_timezone_set($timezone);
}

/* 初始化数据库类 */
require('cls_mysql.php');
$db = new cls_mysql($db_host, $db_user, $db_pass, $db_name);


?>
